-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2021 at 03:51 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyectofinaldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `accidentesreparados`
--

CREATE TABLE `accidentesreparados` (
  `idAccidentesReparados` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `listadoaccidentes_id` int(11) NOT NULL,
  `EstadoReparacion_idEstadoReparacion` int(11) NOT NULL,
  `tiporeparacion_idTipoReparacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categoriasinventarios`
--

CREATE TABLE `categoriasinventarios` (
  `id` int(11) NOT NULL,
  `Desc_Categorias` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `choferes`
--

CREATE TABLE `choferes` (
  `idChoferes` int(11) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL,
  `TipoChofer_idTipoChofer` int(11) NOT NULL,
  `DisponibilidadChofer_idDisponibilidadChofer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `colores`
--

CREATE TABLE `colores` (
  `id` int(11) NOT NULL,
  `Desc_Colores` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `compras`
--

CREATE TABLE `compras` (
  `idCompras` int(11) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL,
  `Tipo de Compra_idTipo de Compra` int(11) NOT NULL,
  `Proveedores_idProveedores` int(11) NOT NULL,
  `Region_idRegion` int(11) NOT NULL,
  `Numero de Pedido` varchar(45) NOT NULL,
  `Total` decimal(12,5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `comprascajachica`
--

CREATE TABLE `comprascajachica` (
  `idComprasCajaChica` int(11) NOT NULL,
  `FechaCompra` datetime NOT NULL,
  `Nota` varchar(45) NOT NULL,
  `Estado_idEstado` int(11) NOT NULL,
  `Partidas_idPartidas` int(11) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `controlcambios`
--

CREATE TABLE `controlcambios` (
  `id` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Descripcion` varchar(150) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `controlviajes`
--

CREATE TABLE `controlviajes` (
  `idControlViajes` int(11) NOT NULL,
  `Viaje_idViaje` int(11) NOT NULL,
  `Facturas` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `id` int(11) NOT NULL,
  `Desc_Departamento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `disponibilidadchofer`
--

CREATE TABLE `disponibilidadchofer` (
  `idDisponibilidadChofer` int(11) NOT NULL,
  `Desc_Disponibilidad` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `Descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estadoreparacion`
--

CREATE TABLE `estadoreparacion` (
  `idEstadoReparacion` int(11) NOT NULL,
  `Descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estadosolicitud`
--

CREATE TABLE `estadosolicitud` (
  `idEstadoSolicitud` int(11) NOT NULL,
  `Descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estadosolicitudvehiculo`
--

CREATE TABLE `estadosolicitudvehiculo` (
  `id` int(11) NOT NULL,
  `Desc_Estado` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(50) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventarios`
--

CREATE TABLE `inventarios` (
  `id` int(11) NOT NULL,
  `Id_RefCategoria` int(11) NOT NULL,
  `DescripcionInv` varchar(50) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `costo` decimal(12,5) NOT NULL,
  `FechaIngreso` datetime NOT NULL,
  `Preferencial_id` int(11) NOT NULL,
  `Zona_idZona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `itemsolicitud`
--

CREATE TABLE `itemsolicitud` (
  `id` int(11) NOT NULL,
  `CantidadItem` int(11) NOT NULL,
  `id_RefSolicitudProveeduria` int(11) NOT NULL,
  `id_RefInvtario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `listadoaccidentes`
--

CREATE TABLE `listadoaccidentes` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `desc_Accidente` varchar(50) NOT NULL,
  `Id_Vehiculo` int(11) NOT NULL,
  `id_TipoAccidente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modelo`
--

CREATE TABLE `modelo` (
  `id` int(11) NOT NULL,
  `Desc_Modelo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `partidas`
--

CREATE TABLE `partidas` (
  `idPartidas` int(11) NOT NULL,
  `Presupuestoporpartida` decimal(12,2) NOT NULL,
  `Presupuesto_idPresupuesto` int(11) NOT NULL,
  `categoriasinventarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `preferencial`
--

CREATE TABLE `preferencial` (
  `id` int(11) NOT NULL,
  `Desc_Preferencial` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `presupuesto`
--

CREATE TABLE `presupuesto` (
  `idPresupuesto` int(11) NOT NULL,
  `Monto` decimal(12,2) NOT NULL,
  `AnnoPresupuesto` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `proveedores`
--

CREATE TABLE `proveedores` (
  `idProveedores` int(11) NOT NULL,
  `Tipo de Proveedor_idTipo de Proveedor` int(11) NOT NULL,
  `NombreProveedor` varchar(45) NOT NULL,
  `Contactos` varchar(45) NOT NULL,
  `Email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `idRegion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reparaciones`
--

CREATE TABLE `reparaciones` (
  `id` int(11) NOT NULL,
  `Desc_Reparacion` varchar(50) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Costo` decimal(12,5) NOT NULL,
  `Id_Vehiculo` int(11) NOT NULL,
  `Media` varchar(300) NOT NULL,
  `accidentesreparados_idAccidentesReparados` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `revisiones`
--

CREATE TABLE `revisiones` (
  `idRevisiones` int(11) NOT NULL,
  `vehiculos_id` int(11) NOT NULL,
  `TipoRevision_idTipoRevision` int(11) NOT NULL,
  `Fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `solicitudproveeduria`
--

CREATE TABLE `solicitudproveeduria` (
  `id` int(11) NOT NULL,
  `FechaSolicitud` datetime NOT NULL,
  `Justificacion` varchar(45) NOT NULL,
  `id_RefTipoSolicitud` int(11) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL,
  `EstadoSolicitud_idEstadoSolicitud` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `solicitudvehiculos`
--

CREATE TABLE `solicitudvehiculos` (
  `id` int(11) NOT NULL,
  `vehiculos_id` int(11) NOT NULL,
  `users_id` mediumint(8) UNSIGNED NOT NULL,
  `FechaSalida` datetime NOT NULL,
  `Razon` varchar(45) NOT NULL,
  `EstadoSolicitudVehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipoaccidente`
--

CREATE TABLE `tipoaccidente` (
  `id` int(11) NOT NULL,
  `Descripcion_Accidente` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipocedula`
--

CREATE TABLE `tipocedula` (
  `idTipoCedula` int(11) NOT NULL,
  `Description` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipochofer`
--

CREATE TABLE `tipochofer` (
  `idTipoChofer` int(11) NOT NULL,
  `Desc_Chofer` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipo de compra`
--

CREATE TABLE `tipo de compra` (
  `idTipo de Compra` int(11) NOT NULL,
  `DescTipoCompra` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipo de proveedor`
--

CREATE TABLE `tipo de proveedor` (
  `idTipo de Proveedor` int(11) NOT NULL,
  `Desc_TipoProveedor` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tiporeparacion`
--

CREATE TABLE `tiporeparacion` (
  `idTipoReparacion` int(11) NOT NULL,
  `Desc_TipoReparacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tiporevision`
--

CREATE TABLE `tiporevision` (
  `idTipoRevision` int(11) NOT NULL,
  `Desc_TipoRevision` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tiposolicitudproveeduria`
--

CREATE TABLE `tiposolicitudproveeduria` (
  `id` int(11) NOT NULL,
  `Desc_TipoSolicitud` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipovehiculo`
--

CREATE TABLE `tipovehiculo` (
  `id` int(11) NOT NULL,
  `Desc_TipoVehiculo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `Cedula` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `tipocedula_idTipoCedula1` int(11) NOT NULL,
  `groups_id` int(50) UNSIGNED NOT NULL,
  `departamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `PlacaVehiculo` varchar(10) NOT NULL,
  `NumeroVIN` varchar(20) NOT NULL,
  `Anno` int(11) NOT NULL,
  `Disponibilidad` tinyint(4) NOT NULL,
  `Kilometraje` varchar(45) NOT NULL,
  `Modelo_Id` int(11) NOT NULL,
  `Color_Id` int(11) NOT NULL,
  `TipoVehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `viaje`
--

CREATE TABLE `viaje` (
  `idViaje` int(11) NOT NULL,
  `solicitudvehiculos_id` int(11) NOT NULL,
  `Choferes_idChoferes` int(11) NOT NULL,
  `PresupuestoAsignado` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `zona`
--

CREATE TABLE `zona` (
  `idZona` int(11) NOT NULL,
  `Desc_Zona` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accidentesreparados`
--
ALTER TABLE `accidentesreparados`
  ADD PRIMARY KEY (`idAccidentesReparados`),
  ADD KEY `fk_AccidentesReparados_listadoaccidentes1_idx` (`listadoaccidentes_id`),
  ADD KEY `fk_AccidentesReparados_EstadoReparacion1_idx` (`EstadoReparacion_idEstadoReparacion`),
  ADD KEY `fk_accidentesreparados_tiporeparacion1_idx` (`tiporeparacion_idTipoReparacion`);

--
-- Indexes for table `categoriasinventarios`
--
ALTER TABLE `categoriasinventarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `choferes`
--
ALTER TABLE `choferes`
  ADD PRIMARY KEY (`idChoferes`),
  ADD KEY `fk_Choferes_users1_idx` (`users_id`),
  ADD KEY `fk_Choferes_TipoChofer1_idx` (`TipoChofer_idTipoChofer`),
  ADD KEY `fk_Choferes_DisponibilidadChofer1_idx` (`DisponibilidadChofer_idDisponibilidadChofer`);

--
-- Indexes for table `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idCompras`),
  ADD KEY `fk_Compras_Tipo de Compra1_idx` (`Tipo de Compra_idTipo de Compra`),
  ADD KEY `fk_Compras_Proveedores1_idx` (`Proveedores_idProveedores`),
  ADD KEY `fk_Compras_Region1_idx` (`Region_idRegion`),
  ADD KEY `fk_Compras_users2_idx` (`users_id`);

--
-- Indexes for table `comprascajachica`
--
ALTER TABLE `comprascajachica`
  ADD PRIMARY KEY (`idComprasCajaChica`),
  ADD KEY `fk_Compras_Estado1_idx` (`Estado_idEstado`),
  ADD KEY `fk_Compras_Partidas1_idx` (`Partidas_idPartidas`),
  ADD KEY `fk_Compras_users1_idx` (`users_id`);

--
-- Indexes for table `controlcambios`
--
ALTER TABLE `controlcambios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_controlcambios_users1_idx` (`users_id`);

--
-- Indexes for table `controlviajes`
--
ALTER TABLE `controlviajes`
  ADD PRIMARY KEY (`idControlViajes`),
  ADD KEY `fk_ControlViajes_Viaje1_idx` (`Viaje_idViaje`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disponibilidadchofer`
--
ALTER TABLE `disponibilidadchofer`
  ADD PRIMARY KEY (`idDisponibilidadChofer`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indexes for table `estadoreparacion`
--
ALTER TABLE `estadoreparacion`
  ADD PRIMARY KEY (`idEstadoReparacion`);

--
-- Indexes for table `estadosolicitud`
--
ALTER TABLE `estadosolicitud`
  ADD PRIMARY KEY (`idEstadoSolicitud`);

--
-- Indexes for table `estadosolicitudvehiculo`
--
ALTER TABLE `estadosolicitudvehiculo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_RefCategoria` (`Id_RefCategoria`),
  ADD KEY `fk_inventarios_Preferencial1_idx` (`Preferencial_id`),
  ADD KEY `fk_inventarios_Zona1_idx` (`Zona_idZona`);

--
-- Indexes for table `itemsolicitud`
--
ALTER TABLE `itemsolicitud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_RefInvtario` (`id_RefInvtario`),
  ADD KEY `id_RefSolicitudProveeduria` (`id_RefSolicitudProveeduria`);

--
-- Indexes for table `listadoaccidentes`
--
ALTER TABLE `listadoaccidentes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_Vehiculo` (`Id_Vehiculo`),
  ADD KEY `id_TipoAccidente` (`id_TipoAccidente`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modelo`
--
ALTER TABLE `modelo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partidas`
--
ALTER TABLE `partidas`
  ADD PRIMARY KEY (`idPartidas`),
  ADD KEY `fk_Partidas_Presupuesto1_idx` (`Presupuesto_idPresupuesto`),
  ADD KEY `fk_Partidas_categoriasinventarios1_idx` (`categoriasinventarios_id`);

--
-- Indexes for table `preferencial`
--
ALTER TABLE `preferencial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`idPresupuesto`);

--
-- Indexes for table `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idProveedores`),
  ADD KEY `fk_Proveedores_Tipo de Proveedor1_idx` (`Tipo de Proveedor_idTipo de Proveedor`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`idRegion`);

--
-- Indexes for table `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_Vehiculo` (`Id_Vehiculo`),
  ADD KEY `fk_reparaciones_accidentesreparados1_idx` (`accidentesreparados_idAccidentesReparados`);

--
-- Indexes for table `revisiones`
--
ALTER TABLE `revisiones`
  ADD PRIMARY KEY (`idRevisiones`),
  ADD KEY `fk_Revisiones_vehiculos1_idx` (`vehiculos_id`),
  ADD KEY `fk_Revisiones_TipoRevision1_idx` (`TipoRevision_idTipoRevision`);

--
-- Indexes for table `solicitudproveeduria`
--
ALTER TABLE `solicitudproveeduria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_RefTipoSolicitud` (`id_RefTipoSolicitud`),
  ADD KEY `fk_solicitudproveeduria_users1_idx` (`users_id`),
  ADD KEY `fk_solicitudproveeduria_EstadoSolicitud1_idx` (`EstadoSolicitud_idEstadoSolicitud`);

--
-- Indexes for table `solicitudvehiculos`
--
ALTER TABLE `solicitudvehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_SolicitudVehiculos_vehiculos1_idx` (`vehiculos_id`),
  ADD KEY `fk_SolicitudVehiculos_users1_idx` (`users_id`),
  ADD KEY `fk_SolicitudVehiculos_EstadoSolicitudVehiculo1_idx` (`EstadoSolicitudVehiculo_id`);

--
-- Indexes for table `tipoaccidente`
--
ALTER TABLE `tipoaccidente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipocedula`
--
ALTER TABLE `tipocedula`
  ADD PRIMARY KEY (`idTipoCedula`);

--
-- Indexes for table `tipochofer`
--
ALTER TABLE `tipochofer`
  ADD PRIMARY KEY (`idTipoChofer`);

--
-- Indexes for table `tipo de compra`
--
ALTER TABLE `tipo de compra`
  ADD PRIMARY KEY (`idTipo de Compra`);

--
-- Indexes for table `tipo de proveedor`
--
ALTER TABLE `tipo de proveedor`
  ADD PRIMARY KEY (`idTipo de Proveedor`);

--
-- Indexes for table `tiporeparacion`
--
ALTER TABLE `tiporeparacion`
  ADD PRIMARY KEY (`idTipoReparacion`);

--
-- Indexes for table `tiporevision`
--
ALTER TABLE `tiporevision`
  ADD PRIMARY KEY (`idTipoRevision`);

--
-- Indexes for table `tiposolicitudproveeduria`
--
ALTER TABLE `tiposolicitudproveeduria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipovehiculo`
--
ALTER TABLE `tipovehiculo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_users_tipocedula1` (`tipocedula_idTipoCedula1`),
  ADD KEY `fk_users_groups1_idx` (`groups_id`),
  ADD KEY `fk_users_departamento1_idx` (`departamento_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Modelo_Id` (`Modelo_Id`),
  ADD KEY `Color_Id` (`Color_Id`),
  ADD KEY `TipoVehiculo_id` (`TipoVehiculo_id`);

--
-- Indexes for table `viaje`
--
ALTER TABLE `viaje`
  ADD PRIMARY KEY (`idViaje`),
  ADD KEY `fk_Viaje_solicitudvehiculos1_idx` (`solicitudvehiculos_id`),
  ADD KEY `fk_Viaje_Choferes1_idx` (`Choferes_idChoferes`);

--
-- Indexes for table `zona`
--
ALTER TABLE `zona`
  ADD PRIMARY KEY (`idZona`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accidentesreparados`
--
ALTER TABLE `accidentesreparados`
  MODIFY `idAccidentesReparados` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoriasinventarios`
--
ALTER TABLE `categoriasinventarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `choferes`
--
ALTER TABLE `choferes`
  MODIFY `idChoferes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `colores`
--
ALTER TABLE `colores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `compras`
--
ALTER TABLE `compras`
  MODIFY `idCompras` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `controlcambios`
--
ALTER TABLE `controlcambios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `controlviajes`
--
ALTER TABLE `controlviajes`
  MODIFY `idControlViajes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `disponibilidadchofer`
--
ALTER TABLE `disponibilidadchofer`
  MODIFY `idDisponibilidadChofer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estadoreparacion`
--
ALTER TABLE `estadoreparacion`
  MODIFY `idEstadoReparacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `estadosolicitud`
--
ALTER TABLE `estadosolicitud`
  MODIFY `idEstadoSolicitud` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `estadosolicitudvehiculo`
--
ALTER TABLE `estadosolicitudvehiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `itemsolicitud`
--
ALTER TABLE `itemsolicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listadoaccidentes`
--
ALTER TABLE `listadoaccidentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `modelo`
--
ALTER TABLE `modelo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partidas`
--
ALTER TABLE `partidas`
  MODIFY `idPartidas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preferencial`
--
ALTER TABLE `preferencial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `idPresupuesto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idProveedores` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `idRegion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reparaciones`
--
ALTER TABLE `reparaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revisiones`
--
ALTER TABLE `revisiones`
  MODIFY `idRevisiones` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `solicitudproveeduria`
--
ALTER TABLE `solicitudproveeduria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `solicitudvehiculos`
--
ALTER TABLE `solicitudvehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipoaccidente`
--
ALTER TABLE `tipoaccidente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tipocedula`
--
ALTER TABLE `tipocedula`
  MODIFY `idTipoCedula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipochofer`
--
ALTER TABLE `tipochofer`
  MODIFY `idTipoChofer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo de compra`
--
ALTER TABLE `tipo de compra`
  MODIFY `idTipo de Compra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo de proveedor`
--
ALTER TABLE `tipo de proveedor`
  MODIFY `idTipo de Proveedor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tiporeparacion`
--
ALTER TABLE `tiporeparacion`
  MODIFY `idTipoReparacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tiporevision`
--
ALTER TABLE `tiporevision`
  MODIFY `idTipoRevision` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tiposolicitudproveeduria`
--
ALTER TABLE `tiposolicitudproveeduria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipovehiculo`
--
ALTER TABLE `tipovehiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `viaje`
--
ALTER TABLE `viaje`
  MODIFY `idViaje` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zona`
--
ALTER TABLE `zona`
  MODIFY `idZona` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accidentesreparados`
--
ALTER TABLE `accidentesreparados`
  ADD CONSTRAINT `fk_AccidentesReparados_EstadoReparacion1` FOREIGN KEY (`EstadoReparacion_idEstadoReparacion`) REFERENCES `estadoreparacion` (`idEstadoReparacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_AccidentesReparados_listadoaccidentes1` FOREIGN KEY (`listadoaccidentes_id`) REFERENCES `listadoaccidentes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_accidentesreparados_tiporeparacion1` FOREIGN KEY (`tiporeparacion_idTipoReparacion`) REFERENCES `tiporeparacion` (`idTipoReparacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `choferes`
--
ALTER TABLE `choferes`
  ADD CONSTRAINT `fk_Choferes_DisponibilidadChofer1` FOREIGN KEY (`DisponibilidadChofer_idDisponibilidadChofer`) REFERENCES `disponibilidadchofer` (`idDisponibilidadChofer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Choferes_TipoChofer1` FOREIGN KEY (`TipoChofer_idTipoChofer`) REFERENCES `tipochofer` (`idTipoChofer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Choferes_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_Compras_Proveedores1` FOREIGN KEY (`Proveedores_idProveedores`) REFERENCES `proveedores` (`idProveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Compras_Region1` FOREIGN KEY (`Region_idRegion`) REFERENCES `region` (`idRegion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Compras_Tipo de Compra1` FOREIGN KEY (`Tipo de Compra_idTipo de Compra`) REFERENCES `tipo de compra` (`idTipo de Compra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Compras_users2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comprascajachica`
--
ALTER TABLE `comprascajachica`
  ADD CONSTRAINT `fk_Compras_Estado1` FOREIGN KEY (`Estado_idEstado`) REFERENCES `estado` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Compras_Partidas1` FOREIGN KEY (`Partidas_idPartidas`) REFERENCES `partidas` (`idPartidas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Compras_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `controlcambios`
--
ALTER TABLE `controlcambios`
  ADD CONSTRAINT `fk_controlcambios_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `controlviajes`
--
ALTER TABLE `controlviajes`
  ADD CONSTRAINT `fk_ControlViajes_Viaje1` FOREIGN KEY (`Viaje_idViaje`) REFERENCES `viaje` (`idViaje`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventarios`
--
ALTER TABLE `inventarios`
  ADD CONSTRAINT `fk_inventarios_Preferencial1` FOREIGN KEY (`Preferencial_id`) REFERENCES `preferencial` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventarios_Zona1` FOREIGN KEY (`Zona_idZona`) REFERENCES `zona` (`idZona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `inventarios_ibfk_1` FOREIGN KEY (`Id_RefCategoria`) REFERENCES `categoriasinventarios` (`id`);

--
-- Constraints for table `itemsolicitud`
--
ALTER TABLE `itemsolicitud`
  ADD CONSTRAINT `itemsolicitud_ibfk_1` FOREIGN KEY (`id_RefInvtario`) REFERENCES `inventarios` (`id`),
  ADD CONSTRAINT `itemsolicitud_ibfk_2` FOREIGN KEY (`id_RefSolicitudProveeduria`) REFERENCES `solicitudproveeduria` (`id`);

--
-- Constraints for table `listadoaccidentes`
--
ALTER TABLE `listadoaccidentes`
  ADD CONSTRAINT `listadoaccidentes_ibfk_1` FOREIGN KEY (`Id_Vehiculo`) REFERENCES `vehiculos` (`id`),
  ADD CONSTRAINT `listadoaccidentes_ibfk_2` FOREIGN KEY (`id_TipoAccidente`) REFERENCES `tipoaccidente` (`id`);

--
-- Constraints for table `partidas`
--
ALTER TABLE `partidas`
  ADD CONSTRAINT `fk_Partidas_Presupuesto1` FOREIGN KEY (`Presupuesto_idPresupuesto`) REFERENCES `presupuesto` (`idPresupuesto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Partidas_categoriasinventarios1` FOREIGN KEY (`categoriasinventarios_id`) REFERENCES `categoriasinventarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `proveedores`
--
ALTER TABLE `proveedores`
  ADD CONSTRAINT `fk_Proveedores_Tipo de Proveedor1` FOREIGN KEY (`Tipo de Proveedor_idTipo de Proveedor`) REFERENCES `tipo de proveedor` (`idTipo de Proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD CONSTRAINT `fk_reparaciones_accidentesreparados1` FOREIGN KEY (`accidentesreparados_idAccidentesReparados`) REFERENCES `accidentesreparados` (`idAccidentesReparados`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `reparaciones_ibfk_1` FOREIGN KEY (`Id_Vehiculo`) REFERENCES `vehiculos` (`id`);

--
-- Constraints for table `revisiones`
--
ALTER TABLE `revisiones`
  ADD CONSTRAINT `fk_Revisiones_TipoRevision1` FOREIGN KEY (`TipoRevision_idTipoRevision`) REFERENCES `tiporevision` (`idTipoRevision`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Revisiones_vehiculos1` FOREIGN KEY (`vehiculos_id`) REFERENCES `vehiculos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `solicitudproveeduria`
--
ALTER TABLE `solicitudproveeduria`
  ADD CONSTRAINT `fk_solicitudproveeduria_EstadoSolicitud1` FOREIGN KEY (`EstadoSolicitud_idEstadoSolicitud`) REFERENCES `estadosolicitud` (`idEstadoSolicitud`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitudproveeduria_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `solicitudproveeduria_ibfk_1` FOREIGN KEY (`id_RefTipoSolicitud`) REFERENCES `tiposolicitudproveeduria` (`id`);

--
-- Constraints for table `solicitudvehiculos`
--
ALTER TABLE `solicitudvehiculos`
  ADD CONSTRAINT `fk_SolicitudVehiculos_EstadoSolicitudVehiculo1` FOREIGN KEY (`EstadoSolicitudVehiculo_id`) REFERENCES `estadosolicitudvehiculo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SolicitudVehiculos_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SolicitudVehiculos_vehiculos1` FOREIGN KEY (`vehiculos_id`) REFERENCES `vehiculos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_departamento1` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups1` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_tipocedula1` FOREIGN KEY (`tipocedula_idTipoCedula1`) REFERENCES `tipocedula` (`idTipoCedula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_ibfk_1` FOREIGN KEY (`Modelo_Id`) REFERENCES `modelo` (`id`),
  ADD CONSTRAINT `vehiculos_ibfk_2` FOREIGN KEY (`Color_Id`) REFERENCES `colores` (`id`),
  ADD CONSTRAINT `vehiculos_ibfk_3` FOREIGN KEY (`TipoVehiculo_id`) REFERENCES `tipovehiculo` (`id`);

--
-- Constraints for table `viaje`
--
ALTER TABLE `viaje`
  ADD CONSTRAINT `fk_Viaje_Choferes1` FOREIGN KEY (`Choferes_idChoferes`) REFERENCES `choferes` (`idChoferes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Viaje_solicitudvehiculos1` FOREIGN KEY (`solicitudvehiculos_id`) REFERENCES `solicitudvehiculos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
