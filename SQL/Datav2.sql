
INSERT INTO `accidentesreparados` (`idAccidentesReparados`, `fecha`, `listadoaccidentes_id`, `EstadoReparacion_idEstadoReparacion`, `tiporeparacion_idTipoReparacion`) VALUES
(1, '2021-10-20 00:00:00', 1, 1, 1),
(2, '2021-10-21 00:00:00', 1, 2, 1);

INSERT INTO `categoriasinventarios` (`id`, `Desc_Categorias`) VALUES
(1, 'Limpieza'),
(2, 'Suministros de Oficina');

INSERT INTO `choferes` (`idChoferes`, `users_id`, `TipoChofer_idTipoChofer`, `DisponibilidadChofer_idDisponibilidadChofer`) VALUES
(3, 3, 1, 1);

INSERT INTO `colores` (`id`, `Desc_Colores`) VALUES
(1, 'red'),
(2, 'Azul');

INSERT INTO `controlviajes` (`idControlViajes`, `Viaje_idViaje`, `Facturas`) VALUES
(3, 5, 'test'),
(4, 5, 'Test origen facturas');

INSERT INTO `departamento` (`id`, `Desc_Departamento`) VALUES
(1, 'TI');

INSERT INTO `disponibilidadchofer` (`idDisponibilidadChofer`, `Desc_Disponibilidad`) VALUES
(1, 'Disponible'),
(2, 'Ausente');

INSERT INTO `estadoreparacion` (`idEstadoReparacion`, `Descripcion`) VALUES
(1, 'Registrado'),
(2, 'En curso');

INSERT INTO `estadosolicitud` (`idEstadoSolicitud`, `Descripcion`) VALUES
(1, 'test'),
(2, 'Aprobado'),
(3, 'Rechazado');

INSERT INTO `estadosolicitudvehiculo` (`id`, `Desc_Estado`) VALUES
(1, 'Solicitado'),
(2, 'Aprobado'),
(3, 'Rechazado');

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Master', 'Usuario Master');

INSERT INTO `inventarios` (`id`, `Id_RefCategoria`, `DescripcionInv`, `Cantidad`, `costo`, `FechaIngreso`, `Preferencial_id`, `Zona_idZona`) VALUES
(1, 1, 'papas', 150, '12.00000', '2021-10-12 00:00:00', 1, 1),
(2, 1, 'Escobas', 50, '150.00000', '2021-10-13 00:00:00', 1, 1),
(3, 1, 'Jabon', 125, '5000.00000', '2021-10-20 00:00:00', 2, 1);

INSERT INTO `listadoaccidentes` (`id`, `fecha`, `desc_Accidente`, `Id_Vehiculo`, `id_TipoAccidente`) VALUES
(1, '2021-10-12 00:00:00', ' test 1', 1, 1),
(2, '0000-00-00 00:00:00', ' Test auto', 1, 1),
(3, '2021-10-11 00:00:00', ' Choque ruta tal', 2, 2);

INSERT INTO `modelo` (`id`, `Desc_Modelo`) VALUES
(1, 'Mitsubishi'),
(2, 'Centra');

INSERT INTO `partidas` (`idPartidas`, `Presupuestoporpartida`, `Presupuesto_idPresupuesto`, `categoriasinventarios_id`) VALUES
(1, '100.00', 1, 1);

INSERT INTO `preferencial` (`id`, `Desc_Preferencial`) VALUES
(1, 'Preferenci8al'),
(2, 'Sin Asignar'),
(3, 'General');

INSERT INTO `presupuesto` (`idPresupuesto`, `Monto`, `AnnoPresupuesto`) VALUES
(1, '100.00', '2021-10-12 17:25:51'),
(2, '450.00', '2021-10-14 00:00:00');

INSERT INTO `reparaciones` (`id`, `Desc_Reparacion`, `Fecha`, `Costo`, `Id_Vehiculo`, `Media`, `accidentesreparados_idAccidentesReparados`) VALUES
(1, ' Pruebna', '2021-10-12 00:00:00', '15000.00000', 1, ' adasdf', 2),
(2, ' Se envia a reparacion de carrozeria ', '2021-10-28 00:00:00', '45000.00000', 1, ' TEST', 1),
(3, ' test', '2021-10-04 00:00:00', '9999999.99999', 1, ' url', 2);

INSERT INTO `revisiones` (`idRevisiones`, `vehiculos_id`, `TipoRevision_idTipoRevision`, `Fecha`) VALUES
(1, 1, 1, '2021-10-29 00:00:00'),
(2, 2, 2, '2022-01-25 00:00:00');

INSERT INTO `solicitudvehiculos` (`id`, `vehiculos_id`, `users_id`, `FechaSalida`, `Razon`, `EstadoSolicitudVehiculo_id`) VALUES
(1, 1, 3, '2021-10-21 22:07:21', 'Pruebas', 1),
(2, 2, 3, '2021-12-15 00:00:00', 'Presentacion prueba Tutor', 1),
(3, 2, 3, '2021-10-11 00:00:00', 'Tal', 1);

INSERT INTO `tipoaccidente` (`id`, `Descripcion_Accidente`) VALUES
(1, 'Vuelco'),
(2, 'Choque');

INSERT INTO `tipocedula` (`idTipoCedula`, `Description`) VALUES
(1, 'Nacional'),
(2, 'Extranjero');

INSERT INTO `tipochofer` (`idTipoChofer`, `Desc_Chofer`) VALUES
(1, 'Permanente'),
(2, 'Rotativo');

INSERT INTO `tiporeparacion` (`idTipoReparacion`, `Desc_TipoReparacion`) VALUES
(1, 'Enderezado'),
(2, 'No Asignado'),
(3, 'Revision Mecanica');

INSERT INTO `tiporevision` (`idTipoRevision`, `Desc_TipoRevision`) VALUES
(1, 'Revision Programada'),
(2, 'RTV');

INSERT INTO `tipovehiculo` (`id`, `Desc_TipoVehiculo`) VALUES
(1, 'Liviano'),
(2, 'Carga');

INSERT INTO `tipo_de_compra` (`idTipo de Compra`, `DescTipoCompra`) VALUES
(1, 'Rebastecer'),
(2, 'Nuevo Item'),
(3, 'Mixto');

INSERT INTO `tipo_de_proveedor` (`idTipo de Proveedor`, `Desc_TipoProveedor`) VALUES
(1, 'Directo'),
(2, 'Indirecto');

INSERT INTO `users` (`id`, `Cedula`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `tipocedula_idTipoCedula1`, `groups_id`, `departamento_id`) VALUES
(3, '116020111', 'fran.95.v@gmail.com', '$2y$10$xZ/QglnkRQa.J9s/Mgb9bu33pkwlpQDJe8h1EkrNUsrQokwR2Ap6y', 'fran.95.v@gmail.com', '1', '1', NULL, NULL, NULL, 0, 1635030140, 1, 'Francisco', '', '87235513', 1, 1, 1),
(4, '106390225', 'chesco696vm@gmail.com', 'prueba', 'chesco696vm', NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Test', 'Francisco', '87235513', 1, 1, 1);

INSERT INTO `vehiculos` (`id`, `PlacaVehiculo`, `NumeroVIN`, `Anno`, `Disponibilidad`, `Kilometraje`, `Modelo_Id`, `Color_Id`, `TipoVehiculo_id`) VALUES
(1, 'dfg456', '794613', 2011, 1, '50', 1, 1, 1),
(2, 'dfg456', '794613', 2011, 1, '80', 1, 2, 1),
(3, 'ABC123', '852945', 2011, 1, '80', 1, 1, 1);

INSERT INTO `viaje` (`idViaje`, `solicitudvehiculos_id`, `Choferes_idChoferes`, `PresupuestoAsignado`) VALUES
(5, 2, 3, '1500.00');

INSERT INTO `zona` (`idZona`, `Desc_Zona`) VALUES
(1, 'Central'),
(2, 'Chorotega');
