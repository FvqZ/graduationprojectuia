 -- INSERT INTO `groups`(`id`, `name`, `description`) VALUES ('1','SuperAdmin','Superdmin'), ('2','Admin','SuperAdmin'), ('3','User','Usuario'), ('4','Revisor','Revisor');

INSERT INTO `users_groups`(`id`, `user_id`, `group_id`) VALUES ('1','1','1');
INSERT INTO `departamento`(`id`, `Desc_Departamento`) VALUES ('1','TI')

INSERT INTO `tipocedula`(`idTipoCedula`, `Description`) VALUES ('1','Físico'), (2, 'Jurídico'), (3, 'Dimex'), (4, 'NITE'), ('5', 'Extranjero');

INSERT INTO `users`(`id`, `Cedula`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `tipocedula_idTipoCedula1`, `groups_id`, `departamento_id`)
VALUES ('1','101110111','fran.95.v@gmail.com','$2y$10$xZ/QglnkRQa.J9s/Mgb9bu33pkwlpQDJe8h1EkrNUsrQokwR2Ap6y','fran.95.v@gmail.com',NULL,NULL,NULL,NULL,NULL,1634638970,NULL,'1','Francisco','Vazquez','88888888',1,1,1);

INSERT INTO `tipovehiculo`(`id`, `Desc_TipoVehiculo`) VALUES ('1','Sedan'), ('2','PickUp'), ('3','Automovil');

INSERT INTO `preferencial`(`id`, `Desc_Preferencial`) VALUES ('1','Prioritario'), ('2','Regular');

INSERT INTO `zona`(`idZona`, `Desc_Zona`) VALUES ('1','Huetar'), ('2','Atlantica'), ('3','Central');
INSERT INTO `categoriasinventarios`(`id`, `Desc_Categorias`) VALUES ('1','Limpieza'), ('2','Miscelanea'), ('3','Disácticos');

INSERT INTO `inventarios`(`id`, `Id_RefCategoria`, `DescripcionInv`, `Cantidad`, `costo`, `FechaIngreso`, `Preferencial_id`, `Zona_idZona`)
VALUES ('1','1','Jabon en polvo','90','1500',now(),'1','3'), ('2','1','Escobas','120','5600',now(),'1','3'), ('3','3','Libros español 9°','10','10600',now(),'1','3')

INSERT INTO `tiposolicitudproveeduria`(`id`, `Desc_TipoSolicitud`) VALUES ('1','Aceptado'), ('2','Pendiente'), ('3','Rechazado');

INSERT INTO `estadosolicitud`(`idEstadoSolicitud`, `Descripcion`) VALUES ('1','Aceptado'), ('2','Rechazado'), ('3','Pendiente');

INSERT INTO `region`(`idRegion`) VALUES ('1');

INSERT INTO `estado`(`idEstado`, `Descripcion`) VALUES ('1','Pendiente'),('2','Reparado'), ('3','En revision');

INSERT INTO `tiporeparacion`(`idTipoReparacion`, `Desc_TipoReparacion`) VALUES ('1','Cambio de llantas'), ('2','Enderezado y pintura'), ('3','RITEVE');
INSERT INTO `colores` (`id`, `Desc_Colores`) VALUES ('1', 'Rojo'), ('2', 'Azul');
INSERT INTO `vehiculos`(`id`, `PlacaVehiculo`, `NumeroVIN`, `Anno`, `Disponibilidad`, `Kilometraje`, `Modelo_Id`, `Color_Id`, `TipoVehiculo_id`) VALUES ('1','DDD111','123','2021','1','19','1','1','1')
INSERT INTO `tipoaccidente` (`id`, `Descripcion_Accidente`) VALUES ('1', 'Choque'), ('2', 'Llanta gastada');
INSERT INTO `listadoaccidentes` (`id`, `fecha`, `desc_Accidente`, `Id_Vehiculo`, `id_TipoAccidente`) VALUES ('1', '2021-10-20 05:01:19.000000', 'Arreglo Riteve', '1', '1'), ('2', '2021-10-20 05:01:19.000000', 'Gases ', '1', '1');

INSERT INTO `estadoreparacion` (`idEstadoReparacion`, `Descripcion`) VALUES ('1', 'En Proceso'), ('2', 'Reparado');

INSERT INTO `accidentesreparados` (`idAccidentesReparados`, `fecha`, `listadoaccidentes_id`, `EstadoReparacion_idEstadoReparacion`, `tiporeparacion_idTipoReparacion`) VALUES ('1', '2021-10-20 05:05:07.000000', '1', '1', '1'), ('2', '2021-10-20 05:05:07.000000', '2', '1', '3');


INSERT INTO `reparaciones` (`id`, `Desc_Reparacion`, `Fecha`, `Costo`, `Id_Vehiculo`, `Media`, `accidentesreparados_idAccidentesReparados`) VALUES ('1', 'Reparacion Gases', '2021-10-20 05:05:46.000000', '213124', '1', 'Test', '1'), ('2', 'Alineado', '2021-10-20 05:05:46.000000', '2121212', '1', '', '1')

INSERT INTO `tipo_de_proveedor`(`id`, `Desc_TipoProveedor`) VALUES (null,'Destacado'),(null,'Esperado'), (null,'Solicitado');
