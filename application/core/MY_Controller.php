<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function construct()
    {
        parent::construct();
        $this->load->library('form_validation');
        $this->load->library('ion_auth');
        $this->load->helper('form');
        $this->load->helper( 'language' );
        $this->lang->load('spanish');

        $this->data['assets'] = base_url() . 'assets/';



        //$this->dateFormats = $dateFormats;
       // $this->data['dateFormats'] = $dateFormats;
        //$this->Admin = $this->sim->in_group('admin') ? TRUE : NULL;
        //$this->data['Admin'] = $this->Admin;

    }

    function page_construct($page, $data = array(), $meta = array()) {
        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        //$meta['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        //$meta['Settings'] = $data['Settings'];
        $data['assets'] = base_url("Estilos/");
        $meta['assets'] =   $data['assets'];
       // $meta['dateFormats'] = $this->dateFormats;
        $meta['module_name'] = $data['module_name'];
        //$meta['events'] = $this->site->getUpcomingEvents();
        $this->load->view('header', $meta);
        $this->load->view($page, $data);
        $this->load->view('footer');
    }

}
