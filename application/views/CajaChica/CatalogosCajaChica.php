
<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>

  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-presupuesto-tab" data-toggle="pill" href="#pills-presupuesto" role="tab" aria-controls="pills-presupuesto" aria-selected="true">Presupuesto</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-partida-tab" data-toggle="pill" href="#pills-partida" role="tab" aria-controls="pills-partida" aria-selected="false">Sub Partidas</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-estadocajachica-tab" data-toggle="pill" href="#pills-estadocajachica" role="tab" aria-controls="pills-estadocajachica" aria-selected="false">Estados</a>
    </li>
  </ul>

  <div class="tab-content" id="pills-tabContent">

    <!--  tabla de Presupuesto-->
      <div class="tab-pane fade show active" id="pills-presupuesto" role="tabpanel" aria-labelledby="pills-presupuesto-tab">
        <div class="card-body">
          <table class="table" id="presupuesto" style="width:100%;">
            <thead>
              <tr>
                <th>ID</th>
                <th>Presupuesto Anual</th>
                <th>Año</th>
                <th> Acciones</th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addpresupuesto"><i class="fa fa-plus"></i>Agregar Presupuesto Anual</button>
        </div>
      </div>
    <!-- /tabla de Presupuesto -->


    <!-- PARTIDA -->
      <div class="tab-pane fade" id="pills-partida" role="tabpanel" aria-labelledby="pills-partida-tab">
        <div class="card-body">
          <table class="table" id="partida" style="width:100%;">
            <thead>
              <tr>
                <th>ID</th>
                <th>Presupuesto por Partida</th>
                <th> Presupuesto Anual</th>
                <th> Partida</th>
                <th> Acciones</th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addpartida"><i class="fa fa-plus"></i>Agregar Partida</button>
        </div>
      </div>
    <!-- /PARTIDA-->

    <!--  tabla de Estados Caja Chica-->
      <div class="tab-pane fade" id="pills-estadocajachica" role="tabpanel" aria-labelledby="pills-estadocajachica-tab">
        <div class="card-body">
          <table class="table" id="estadocajachica" style="width:100%;">
            <thead>
              <tr>
                <th>ID</th>
                <th>Estado</th>
                <th> Acciones</th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addestadocajachica"><i class="fa fa-plus"></i>Agregar Estado</button>
        </div>
      </div>
    <!-- /tabla de Estados Caja Chica -->
  </div>
</div>




<!-- Modelo Presupuesto-->
<div class="modal" tabindex="-1" role="dialog" id="modelpresupuesto">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Presupuesto Anual</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosCajaChica/InsertPresupuesto')?>' method='post'>
      <div class="modal-body">
        <div class="form-group">
        <label>Presupuesto Anual</label><br>
          <input type='number' name='dt_monto' placeholder='Presupuesto' class="form-control";> </input> <br>



        <label>Año</label><br>
          <input type='date' name='dt_anno' class="form-control";> </input> <br>
        </div>
      </div>

      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo presupuesto-->

<!-- Modelo Partida-->
<div class="modal" tabindex="-1" role="dialog" id="modelpartida" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Partida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosCajaChica/InsertPartida')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Partidas</label><br>
          <input type='Number' placeholder='Monto' name='dt_Partida' class="form-control"> </input> <br>
          <label for="PresupuestoAnual" class=" form-control-label">Presupuesto Anual</label><br>

        <?php

        foreach ($PresupuestoAnual as $attr)
        {
        $attrPresupuestoAnualt[$attr->idPresupuesto] = $attr->AnnoPresupuesto;
        }

        echo form_dropdown('PresupuestoAnual',$attrPresupuestoAnualt, '0', 'class="form-control select2"');
        ?><br>
                  <label for="PresupuestoAnual" class=" form-control-label">Categoria</label><br>

        <?php

        foreach ($CategoriaPartida as $attr)
        {
        $attrCategoriaPartida[$attr->id] = $attr->Desc_Categorias;
        }

        echo form_dropdown('CategoriaPartida',$attrCategoriaPartida, '0', 'class="form-control select2"');
        ?><br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo Partida-->

<!-- Modelo Estados Caja Chica-->
<div class="modal" tabindex="-1" role="dialog" id="modelestadocajachica">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Presupuesto Anual</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosCajaChica/InsertEstadoCajaChica')?>' method='post'>
      <div class="modal-body">
        <div class="form-group">
        <label>Estado Caja Chica</label><br>
          <input type='text' placeholder='Estado de la Solicitud' name='dt_estado' class="form-control";> </input> <br>
        </div>
      </div>


      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo Estados Caja Chica-->



<script>

$("#addpartida").on("click",function(){
  $('#modelpartida').modal("show")
})

$("#addpresupuesto").on("click",function(){
  $('#modelpresupuesto').modal("show")
})

$("#addestadocajachica").on("click",function(){
  $('#modelestadocajachica').modal("show")
})


function renderDeletePresupuesto(row)
{

return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosCajaChica/EditPresupuesto")?>/${row.idPresupuesto}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosCajaChica/BorrarPresupuesto")?>?id=${row.idPresupuesto}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}


function renderDeletePartida(row)
{

return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosCajaChica/EditPartidas")?>/${row.idPartidas}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosCajaChica/BorrarPartida")?>?id=${row.idPartidas}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}

function renderDeleteEstado(row)
{
return `<div class="row">
<div class="col-1"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosCajaChica/EditEstado")?>/${row.idEstado}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosCajaChica/BorrarEstado")?>?id=${row.idEstado}"'><i class='fa fa-trash'></i></a> </div>
</div>`;
}



$(document).ready(function(e){


    $('#partida').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url : "<?= site_url("AdminCatalogosCajaChica/CargarPartida")?>",// base_url+'Index.php/AdminCatalogosGaraje/CargarColores',
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idPartidas" },
                { "data": "Presupuestoporpartida" },
                { "data": "Monto" },
                { "data": "Desc_Categorias" },
                { "data": renderDeletePartida }
                          ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }
    }); // End of DataTable


    $('#presupuesto').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminCatalogosCajaChica/CargarPresupuesto")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idPresupuesto" },
                { "data": "Monto" },
                { "data": "AnnoPresupuesto" },
                {"data": renderDeletePresupuesto}
                          ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }
    });

    $('#estadocajachica').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminCatalogosCajaChica/CargarEstadoCajaChica")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idEstado" },
                { "data": "Descripcion" },
                {"data": renderDeleteEstado}
                          ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }
    });



  }); // End Document Ready Function

</script>
