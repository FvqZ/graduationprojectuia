
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableSolicitudCajaChica">
                                <thead>
                                    <tr>
                                         <th>ID</th>
                                        <th>Fecha de Solicitud</th>
                                         <th>Nota</th>
                                         <th>Razon</th>
                                         <th>Partida</th>
                                         <th>Usuario</th>
                                         <th>Total</th>
                                         <th>Estado</th>
                                         <th>Acciones</th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                </div>
            </div>


<script>
            function renderDeletePresupuesto(row)
{

return `<div class="row">
<a class="btn btn-info ${row.Descripcion !== "Registrado" ? 'disabled': ''}" data-toggle="ajax" href="<?= site_url("AdminCatalogosCajaChica/EditListado")?>/${row.idComprasCajaChica}"><i class="fa fa-edit"></i></a>
</div>`;


}


</script>





        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableSolicitudCajaChica').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosCajaChica/CargarSolicitudCajaChica") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idComprasCajaChica" },
                            { "data": "FechaCompra" },
                            { "data": "Nota" },
                            { "data": "Razon" },
                            { "data": "Desc_Categorias"},
                            { "data": "Cedula" },
                            { "data": "TotalFactura"},
                            { "data": "Descripcion" },
                            { "data": renderDeletePresupuesto },
                                    ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

                });

                //table.draw()
            });
            </script>
