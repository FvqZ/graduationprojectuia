
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Partidas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosCajaChica/EditPartidas/'.$id)?>' method='post'>
      <div class="modal-body">

      <div class="form-group">
          <label>Presupuesto</label><br>
          <input type='text' name='dt_Partida' placeholder="Presupuesto" class="form-control" value="<?= $partidas->Presupuestoporpartida ?>" />
      </div>


      <div class="form-group">
          <label>Presupuesto Anual</label><br>
          <?php
          $cantidad_presupuesto[""] = "Seleccione un presupuesto anual";
          foreach ($PresupuestoAnual as $attr)
          {
          $cantidad_presupuesto[$attr->idPresupuesto] = $attr->AnnoPresupuesto;
          }
          echo form_dropdown('edit_TipoProveedor', $cantidad_presupuesto, $partidas->Presupuesto_idPresupuesto, "class='select2 form-control' ");
           ?>
      </div>

      <div class="form-group">
          <label>Partida</label><br>
          <?php
          $tipo_partida[""] = "Seleccione una partida";
          foreach ($CategoriaPartida as $attr)
          {
          $tipo_partida[$attr->id] = $attr->Desc_Categorias;
          }
          echo form_dropdown('edit_TipoPartida', $tipo_partida, $partidas->categoriasinventarios_id, "class='select2 form-control' ");
           ?>
      </div>



      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
