<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Presupuesto</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminCatalogosCajaChica/EditPresupuesto/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Tipo de Cedula</label><br>
        <input type='text' name='dt_monto' placeholder="Monto" class="form-control" value="<?= $presupuesto->Monto ?>"/> <br>
        <label>Año</label><br>
        <input type='text' name='dt_anno' placeholder="Año" class="form-control" value="<?= $presupuesto->AnnoPresupuesto ?>"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>