
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableSolicitudCajaChica">
                                <thead>
                                    <tr>
                                         <th>ID</th>
                                        <th>Fecha de Solicitud</th>
                                         <th>Nota</th>
                                         <th>Razon</th>
                                        <th>Estado</th>
                                         <th>Partida</th>
                                         <th>Total</th>
                                         <th>Usuario</th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addSolicitudCajaChica" id="addSolicitudCajaChica" class="btn btn-primary" data-toggle="modal" data-target="#ModalSolicitudCajaChica">Solicitar por Caja Chica</button>
                </div>
            </div>





        <div class="modal" tabindex="-1" role="dialog" id="ModalSolicitudCajaChica">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Generar una Solicitud de Caja Chica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <form action='<?=site_url('AdminCatalogosCajaChica/InsertSolicitudCajaChica')?>' method='post'>
              <div class="modal-body">


                    <label>Facturas Solicitud</label><br>
                    <textarea name='dt_factura' id='factura' class="form-control" > </textarea> <br>

                    <label>Razon Solicitud</label><br>
                    <textarea name='dt_razon' id='razon' class="form-control" > </textarea> <br>



                    <label for="PartidaSeleccionada" class=" form-control-label">Partida Seleccionada</label><br>

                            <?php

                            foreach ($PartidaSeleccionada as $attr)
                            {
                            $attrPartidaSeleccionada[$attr->id] = $attr->Desc_Categorias." ".date("Y",strtotime($attr->AnnoPresupuesto));
                            }

                            echo form_dropdown('PartidaSeleccionada',$attrPartidaSeleccionada, '0', 'class="form-control select2"');
                            ?><br>

                      <label for="UsuarioSeleccionado" class=" form-control-label">Usuario Solicitante</label><br>

                            <?php

                            foreach ($UsuarioSeleccionado as $attr)
                            {
                            $attrUsuarioSeleccionado[$attr->id] = $attr->Cedula;
                            }

                            echo form_dropdown('UsuarioSeleccionado',$attrUsuarioSeleccionado, '0', 'class="form-control select2"');
                            ?><br>

                    <label>Total Factura</label><br>
                    <input type='number' name='dt_Total' id='dt_Total' placeholder='Costo Total' class="form-control" > </input> <br>


              </div>
              <div class="modal-footer">

                <input type="submit" class="btn btn-primary" value="Guardar"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
                </form>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableSolicitudCajaChica').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosCajaChica/CargarSolicitudCajaChica") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idComprasCajaChica" },
                            { "data": "FechaCompra" },
                            { "data": "Nota" },
                            { "data": "Razon" },
                            { "data": "Descripcion" },
                            { "data": "Desc_Categorias"},
                            { "data": "TotalFactura"},
                            { "data": "Cedula" },
                                    ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

                });

                //table.draw()
            });
            </script>
