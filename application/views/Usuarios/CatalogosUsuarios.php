
<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item">
          <a class="nav-link active" id="pills-tipocedula-tab" data-toggle="pill" href="#pills-tipocedula" role="tab" aria-controls="pills-tipocedula" aria-selected="true">Tipo de Cédula</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" id="pills-tipousuario-tab" data-toggle="pill" href="#pills-tipousuario" role="tab" aria-controls="pills-tipousuario" aria-selected="false">Tipo Usuario</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" id="pills-departamento-tab" data-toggle="pill" href="#pills-departamento" role="tab" aria-controls="pills-departamento" aria-selected="false">Departamento</a>
      </li>
    </ul>


  <div class="tab-content" id="pills-tabContent">



        <!--  tabla de tipocedula-->
    <div class="tab-pane fade show active" id="pills-tipocedula" role="tabpanel" aria-labelledby="pills-tipocedula-tab">
      <div class="card-body">
        <table class="table" id="tipocedula" style="width:100%">
          <thead>
            <tr>
              <th>ID</th>
              <th><?= lang('description') ?></th>
              <th>Acciones</th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addtipocedula"><i class="fa fa-plus"></i> Agregar un Tipo de Cédula</button>
      </div>
    </div>

        <!-- /tabla de tipocedula -->

        <!-- tabla de tipo usuario -->
    <div class="tab-pane fade" id="pills-tipousuario" role="tabpanel" aria-labelledby="pills-tipousuario-tab">
      <div class="card-body">
        <table class="table" id="tipousuario" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID') ?></th>
              <th>Nombre</th>
              <th><?= lang('description') ?></th>
              <th><?= lang('actions') ?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addtipousuario"><i class="fa fa-plus"></i>Agregar Tipo de Usuario</button>
      </div>
    </div>
        <!-- / tabla tipo usuario-->

            <!-- departamento -->
    <div class="tab-pane fade" id="pills-departamento" role="tabpanel" aria-labelledby="pills-departamento-tab">
      <div class="card-body">
        <table class="table" id="departamento" style="width:100%;">
          <thead>
            <tr>
              <th>ID</th>
              <th>Departamento</th>
              <th>Acciones</th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="adddepartamento"><i class="fa fa-plus"></i>Agregar Departamento</button>
      </div>
    </div>
            <!-- /departamento-->

  </div>

</div>






<!-- Modelo tipocedula-->
<div class="modal" tabindex="-1" role="dialog" id="modeltipocedula">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Tipo de Cédula</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminUsuarios/InsertTipoCedula')?>' method='post'>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tipo de Cédula</label><br>
                    <input type='text' name='dt_tipocedula' class="form-control";> </input> <br>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Guardar"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipocedula-->


<!-- Modelo <th>Tipo Usuario</th>-->
<div class="modal" tabindex="-1" role="dialog" id="modeltipousuario">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Tipo Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminUsuarios/InsertTipoUsuario')?>' method='post'>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tipo Usuario</label><br>
                    <input type='text' name='dt_tipousuario' placeholder="Tipo de Usuario" class="form-control"> </input> <br>
                    <label>Descripción del Rol</label><br>
                    <input type='text' name='dt_descrol' placeholder="Descripcion" class="form-control"> </input> <br>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Guardar"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo <th>Tipo Usuario</th>-->


<!-- Modelo tipocedula-->
<div class="modal" tabindex="-1" role="dialog" id="modeldepartamento">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Tipo de Departamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminUsuarios/InsertDepartamento')?>' method='post'>
            <div class="modal-body">
                <div class="form-group">
                    <label>Departamento</label><br>
                    <input type='text' name='dt_departamento' class="form-control";> </input> <br>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Guardar"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipocedula-->


<script>

$("#addtipocedula").on("click",function(){
  $('#modeltipocedula').modal("show")
})

$("#addtipousuario").on("click",function(){
  $('#modeltipousuario').modal("show")
})

$("#adddepartamento").on("click",function(){
  $('#modeldepartamento').modal("show")
})



function renderDeleteTipoCedula(row)
{

return `<div class="row">
  <div class="col-1"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminUsuarios/EditTipoCedula")?>/${row.idTipoCedula}"><i class="fa fa-edit"></i></a></div>
  <div class="col-1"><a class='btn btn-danger' onclick="return confirm('¿Desea borrar este item?');" href='<?= site_url("AdminUsuarios/BorrarTipoCedula")?>?id=${row.idTipoCedula}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}

function renderDeleteTipoUsuarios(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminUsuarios/EditTipoUsuario")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a class='btn btn-danger' onclick="return confirm('¿Desea borrar este item?');" href='<?= site_url("AdminUsuarios/BorrarTipoUsuarios")?>?id=${row.id}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}

function renderDeleteDepartamento(row)
{

return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminUsuarios/EditDepartamento")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a class='btn btn-danger' onclick="return confirm('¿Desea borrar este item?');" href='<?= site_url("AdminUsuarios/BorrarDepartamento")?>?id=${row.id}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}


$(document).ready(function(e){


    $('#tipocedula').DataTable(
      {
      "pageLength" : 10,
      "order": [[0, "asc" ]],
      "ajax":{
                url : "<?= site_url("AdminUsuarios/CargarTipoCedula")?>",// base_url+'Index.php/AdminCatalogosGaraje/CargarColores',
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idTipoCedula" },
                { "data": "Description" },
                { "data": renderDeleteTipoCedula }
              ],

              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }

    }); // End of DataTable


    $('#tipousuario').DataTable(
      {
      "pageLength" : 10,
      "order": [[0, "asc" ]],
      "ajax":{
                url :  "<?= site_url("AdminUsuarios/CargarTipoUsuarios")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "description" },
                {"data": renderDeleteTipoUsuarios}
                          ]
    });

    $('#departamento').DataTable(
      {
      "pageLength" : 10,
      "order": [[0, "asc" ]],
      "ajax":{
                url :  "<?= site_url("AdminUsuarios/CargarDepartamento")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Departamento" },
                { "data": renderDeleteDepartamento  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable



  }); // End Document Ready Function

</script>
