<div class="card">
  <div class="card-header">
        <h2><?= $module_name; ?></h2>
  </div>
  <div class="card-body">

    <table class="table" id="roles">
        <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Descripción</th>
              <th>Acciones</th>
             </tr>
        </thead>
        <tbody></tbody>
    </table>



  </div>
  <div class="card-footer">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#roleModal">Registre un Rol</button>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="roleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <form action= '<?=site_url('AdminUsuarios/InsertRole')?>' method='post'>
        <div class="modal-header">
          <h5 class="modal-title">Registre un Rol</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="name" class=" form-control-label">Nombre</label>
            <input name="name" class="form-control" placeholder="Nombre" />
          </div>
          <div class="form-group">
            <label for="description" class=" form-control-label">Descripción</label>
            <input name="description" class="form-control" placeholder="Descripción" />
          </div>
        </div>
        <div class="modal-footer">
          <input class="btn btn-success" type="submit" value="Agregar">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </form>
  </div>
</div>



<script type="text/javascript">

function ActionsRoles(row)
{
  return `<div class="row">
  <div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url('AdminUsuarios/editRole')?>/${row.id}"><i class="fa fa-edit"></i></a></div>
  <div class="col-2"><a class="btn btn-danger" href="<?= site_url('AdminUsuarios/DeleteRole') ?>/${row.id}"><i class="fa fa-trash"></i></a></div>

  </div>`;
        return id == 1 ? `<span class="text-success">Activo</span>` : `<span class="text-warning">Desactivado</span>`;
}
    $(document).ready(function(e){

    var table =  $('#roles').DataTable(
        {
        "pageLength" : 10,
        "dom": "Bfrtip",
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                    url :  "<?= site_url("AdminUsuarios/getRoles") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                    dataSrc:"data",
                    type : 'GET'
                },
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "description" },
                    { "data": ActionsRoles },
                            ]

        });

        //table.draw()
    });

</script>
