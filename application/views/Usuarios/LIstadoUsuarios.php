
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableusuarios">
                                <thead>
                                    <tr>
                                         <th>ID</th>
                                         <th>Tipo de Identificación</th>
                                         <th>Identificación</th>
                                         <th>Correo</th>
                                         <th>Estado</th>
                                         <th>Nombre</th>
                                         <th>Teléfono</th>
                                         <th>Tipo Usuario</th>
                                         <th>Departamento</th>
                                         <th>Acciones</th>

                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="usuarios" id="usuarios" class="btn btn-primary" data-toggle="modal" data-target="#usuariosModal">Registre un Usuario</button>
                </div>
            </div>



            <div class="modal" tabindex="-1" role="dialog" id="usuariosModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
               <form action= '<?=site_url('AdminUsuarios/InsertUsuarios')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title">Registre un Usuario</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="TipoCedulaSelect" class=" form-control-label">Tipo de Identificación</label>
                      <?php
                        foreach ($TipoCedulaSelect as $attr)
                        {
                          $attrTipoCedulaSelect[$attr->idTipoCedula] = $attr->Description;
                        }
                        echo form_dropdown('TipoCedulaSelect',$attrTipoCedulaSelect, '0', 'class="form-control select2"');
                      ?>
                  </div>
                  <div class="form-group">
                    <label for="TipoUsuarioSelect" class=" form-control-label">Tipo de Usuario</label>
                    <?php
                    foreach ($TipoUsuarioSelect as $attr)
                    {
                      $attrTipoUsuarioSelect[$attr->id] = $attr->name;
                    }
                    echo form_dropdown('TipoUsuarioSelect',$attrTipoUsuarioSelect, '0', 'class="form-control select2"');
                    ?>
                  </div>
                  <div class="form-group">
                    <label for="DepartamentoSelect" class=" form-control-label">Departamento</label>
                    <?php
                      foreach ($DepartamentoSelect as $attr)
                      {
                        $attrDepartamentoSelect[$attr->id] = $attr->Desc_Departamento;
                      }
                    echo form_dropdown('DepartamentoSelect',$attrDepartamentoSelect, '0', 'class="form-control select2"');
                    ?>
                  </div>

                  <div class="form-group">
                    <label>Identificación del Usuario</label>
                    <input name='dt_cedula' id='cedula' class="form-control" />
                  </div>

                  <div class="form-group">
                    <label>Nombre</label>
                    <input name='dt_nombre' id='nombre' class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Apellidos</label>
                    <input name='dt_apellidos' id='apellidos' class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Teléfono</label>
                    <input name='dt_phone' id='telefono' class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Correo Electronico</label>
                    <input name='dt_correo' id='correo' class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Contraseña</label>
                    <input name='password' id='contraseña' class="form-control"  />
                  </div>
        </div>
        <div class="modal-footer">
          <input class="btn btn-success" type="submit" value="Agregar">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>







        <script type="text/javascript">

        function isActive(id)
        {
                return id == 1 ? `<span class="text-success">Activo</span>` : `<span class="text-warning">Desactivado</span>`;
        }
        function actions(row)
        {
            return `
            <div class="row">
                <div class="col-4"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url('AdminUsuarios/editUser') ?>/${row.id}"><i class="fa fa-edit"></i></a></div>
                <div class="col-4"><a class="btn btn-danger" onclick="return confirm('¿Desea borrar este usuario?');" href="<?= site_url('AdminUsuarios/borrarUser') ?>/${row.id}"><i class="fa fa-trash"></i></a></div>

            </div>`;
        }
            $(document).ready(function(e){

            var table =  $('#datatableusuarios').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminUsuarios/CargarUsuarios") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "Description" },
                            { "data": "Cedula" },
                            { "data": "email" },
                            { "data": "active", "render":isActive },
                            { "data": "user" },
                            { "data": "phone" },
                            { "data": "name" },
                            { "data": "Desc_Departamento" },
                            { "data": actions}
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>
