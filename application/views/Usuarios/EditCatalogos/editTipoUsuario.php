<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Tipo Usuario</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminUsuarios/EditTipoUsuario/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Tipo de Usuario</label><br>
        <input type='text' name='dt_tipousuario' placeholder="Tipo de Cedula" class="form-control" value="<?= $tipousuario->name ?>"/> <br>
        <label>Descripcion</label><br>
        <input type='text' name='dt_descrol' placeholder="Descripcion" class="form-control" value="<?= $tipousuario->description ?>"/> <br>
        
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>