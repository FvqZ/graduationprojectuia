<div class="modal-dialog" role="document">
    <div class="modal-content">
     <form action= '<?=site_url('AdminUsuarios/editUser/'.$id)?>' method='post'>
      <div class="modal-header">
        <h5 class="modal-title">Editar usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="TipoCedulaSelect" class=" form-control-label">Tipo de Identificación</label>
              <?php
                foreach ($TipoCedulaSelect as $attr)
                {
                  $attrTipoCedulaSelect[$attr->idTipoCedula] = $attr->Description;
                }
                echo form_dropdown('TipoCedulaSelect',$attrTipoCedulaSelect, $usuario->tipocedula_idTipoCedula1, 'class="form-control select2"');
              ?>
          </div>
          <div class="form-group">
            <label for="TipoUsuarioSelect" class=" form-control-label">Tipo de Usuario</label>
            <?php
            foreach ($TipoUsuarioSelect as $attr)
            {
              $attrTipoUsuarioSelect[$attr->id] = $attr->name;
            }
            echo form_dropdown('TipoUsuarioSelect',$attrTipoUsuarioSelect, $usuario->groups_id, 'class="form-control select2"');
            ?>
          </div>
          <div class="form-group">
            <label for="DepartamentoSelect" class=" form-control-label">Departamento</label>
            <?php
              foreach ($DepartamentoSelect as $attr)
              {
                $attrDepartamentoSelect[$attr->id] = $attr->Desc_Departamento;
              }
            echo form_dropdown('DepartamentoSelect',$attrDepartamentoSelect, $usuario->departamento_id, 'class="form-control select2"');
            ?>
          </div>

          <div class="form-group">
            <label>Identificación del Usuario</label>
            <input name='dt_cedula' id='cedula' class="form-control" value="<?= $usuario->Cedula ?>" />
          </div>

          <div class="form-group">
            <label>Nombre</label>
            <input name='dt_nombre' id='nombre' class="form-control" value="<?= $usuario->first_name ?>" />
          </div>
          <div class="form-group">
            <label>Apellidos</label>
            <input name='dt_apellidos' id='apellidos' class="form-control" value="<?= $usuario->last_name ?>" />
          </div>
          <div class="form-group">
            <label>Teléfono</label>
            <input name='dt_phone' id='telefono' class="form-control" value="<?= $usuario->phone ?>" />
          </div>
          <div class="form-group">
            <label>Correo Electronico</label>
            <input name='dt_correo' id='correo' class="form-control" value="<?= $usuario->email ?>"/>
          </div>
          <div class="form-group">
            <label>Contraseña</label>
            <input name='password' id='contraseña' class="form-control"  />
          </div>
    </div>
      <div class="modal-footer">
        <input class="btn btn-success" type="submit" value="Agregar">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </form>
</div>
