<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Departamento</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminUsuarios/EditDepartamento/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Departamento</label><br>
        <input type='text' name='dt_departamento' placeholder="Departamento" class="form-control" value="<?= $departamento->Desc_Departamento ?>"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>