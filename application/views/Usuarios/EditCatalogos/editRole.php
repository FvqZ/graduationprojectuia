<div class="modal-dialog" role="document">
  <div class="modal-content">
     <form action= '<?=site_url('AdminUsuarios/EditRole/'.$id)?>' method='post'>
      <div class="modal-header">
        <h5 class="modal-title">Editar un Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="name" class=" form-control-label">Nombre</label>
          <input name="dt_name" class="form-control" placeholder="Nombre" value="<?= $rol->name?>" />
        </div>
        <div class="form-group">
          <label for="description" class=" form-control-label">Descripción</label>
          <input name="dt_description" class="form-control" placeholder="Descripción" value="<?= $rol->description?>" />
        </div>
      </div>
      <div class="modal-footer">
        <input class="btn btn-success" type="submit" value="Agregar">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </form>
</div>
