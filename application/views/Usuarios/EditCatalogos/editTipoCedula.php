<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Tipo Cedula</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminUsuarios/EditTipoCedula/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Tipo de Cedula</label><br>
        <input type='text' name='dt_tipocedula' placeholder="Tipo de Cedula" class="form-control" value="<?= $tipocedula->Description ?>"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>