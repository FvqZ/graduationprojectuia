
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= $module_name ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
	<script type="text/javascript">
	function fld(oObj) {
			if (oObj != null) {
					var aDate = oObj.split('-');
					var bDate = aDate[2].split(' ');
					year = aDate[0], month = aDate[1], day = bDate[0], time = bDate[1];
					return month + "/" + day + "/" + year + " " + time;
			} else {
					return '';
			}
	}

	convertNumbers = (number) => {
		number = parseFloat(number).toFixed(2);
	return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};


	</script>
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?= $assets ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/select2/select2.min.css">
<link rel="stylesheet" href="<?= $assets?>css/select2-bootstrap.min.css">
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="<?= $assets ?>css/util.css"> -->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>css/main.css">
<!--===============================================================================================-->
<link href="<?= $assets ?>css/animsition.min.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/theme.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/animate.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/slick.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/perfect-scrollbar.css" rel="stylesheet" media="all">

<link rel="stylesheet" type="text/css" href="<?= $assets ?>js/datatables.min.css"/>
<link rel="stylesheet" href="<?= $assets ?>css/jqueryui.css">

<!--estilos para el header -->
<!-- Font Awesome -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>

</head>
<?php $user_group = $this->ion_auth->show_group(); ?>


<body class="animsition">

	<div class="page-wrapper">

		<!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="<?= site_url('MainMenu') ?>">
													<i class="fa fa-home"></i> &nbsp;
													<?= lang('home') ?>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">

												<li class="has-sub">
													<a class="js-arrow mm_admincatalogosgaraje" href="#">
													<i class="fas fa-car"></i><?= lang('garage') ?></a>
														<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

															<?php if ($user_group->id == 1 ): ?>
																<li id="admincatalogosgaraje_index_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje")?>"><?= lang('catalogo_support') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id ==1 || $user_group->id ==2 || $user_group->id ==3 || $user_group->id == 4): ?>
																<li id="admincatalogosgaraje_solicitarvehiculo_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/SolicitarVehiculo")?>"><?= lang('request_vehicle') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id ==1 || $user_group->id ==2 || $user_group->id ==3): ?>
																<li id="admincatalogosgaraje_listadosolicitudes_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/ListadoSolicitudes")?>"><?= lang('request_list') ?></a>
																</li>

																<li id="admincatalogosgaraje_listadoviajes_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/ListadoViajes")?>"><?= lang('trips_list') ?></a>
																</li>

																<li id="admincatalogosgaraje_reportarfacturas_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/ReportarFacturas")?>"><?= lang('add_invoices') ?></a>
																</li>

																<li id="admincatalogosgaraje_listadofacturas_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/ListadoFacturas")?>"><?= lang('list_invoices') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2 ): ?>
																<li id="admincatalogosgaraje_registraraccidente_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarAccidente")?>"><?= lang('add_accident')?></a>
																</li>

																<li id="admincatalogosgaraje_registrarreparaccion_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarReparaccion")?>"><?= lang('add_repair') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2 ): ?>
																<li id="admincatalogosgaraje_agregarchoferes_mobile" >
																	<a href="<?= site_url("AdminCatalogosGaraje/AgregarChoferes")?>"><?= lang('driver_list') ?></a>
																</li>

																<li id="admincatalogosgaraje_registrarmantenimiento_mobile">
																	<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarMantenimiento")?>"><?= lang('add_support') ?></a>
																</li>
															<?php endif; ?>
														</ul>
												</li>

												<li class="has-sub">
													<a class="js-arrow mm_adminsuministros" href="#">
													<i class="fas fa-box"></i>Suministros</a>
														<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

															<?php if ($user_group->id == 1): ?>
																<li id="adminsuministros_index_mobile">
																	<a  href=<?= site_url("AdminSuministros")?>><?= lang('catalogo_support') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2 || $user_group->id == 3 || $user_group->id == 4): ?>
																<li id="adminsuministros_cargarsolicitud_mobile">
																	<a  href=<?= site_url("AdminSuministros/CargarSolicitud")?>><?= lang('request_supplies') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2): ?>
																<li id="adminsuministros_listadopedidos_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminSuministros/ListadoPedidos")?>><?= lang('order_list') ?></a>
																</li>

																<li id="adminsuministros_listadoinventario_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminSuministros/ListadoInventario")?>><?= lang('available_inventory') ?></a>
																</li>

																<li id="adminsuministros_proveedores_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminSuministros/proveedores")?>><?= lang('provider') ?></a>
																</li>

																<li id="adminsuministros_compras_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminSuministros/compras")?>><?= lang('sales') ?></a>
																</li>
															<?php endif; ?>
														</ul>
												</li>

												<li class="has-sub">
													<a class="js-arrow mm_admincatalogoscajachica" href="#">
													<i class="fas fa-bank"></i><?= lang('petty_cash') ?></a>
														<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

															<?php if ($user_group->id == 1): ?>
																<li id="admincatalogoscajachica_index_mobile">
																	<a href="<?= site_url("AdminCatalogosCajaChica")?>"><?= lang('catalogo_support') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2 || $user_group->id == 3 || $user_group->id == 4): ?>
																<li id="admincatalogoscajachica_solicitudcajachica_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminCatalogosCajaChica/SolicitudCajaChica")?>><?= lang('new_request') ?></a>
																</li>

																<li id="admincatalogoscajachica_listadosolicitudescajachica_mobile" >
																	<a class="dropdown-item" href=<?= site_url("AdminCatalogosCajaChica/ListadoSolicitudesCajaChica")?>><?= lang('request_list') ?></a>
																</li>
															<?php endif; ?>
														</ul>
												</li>

												<li class="has-sub">
													<a class="js-arrow mm_adminusuarios" href="#">
													<i class="fas fa-user-circle"></i><?= lang('users') ?></a>
														<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

															<?php if ($user_group->id == 1 ): ?>
																<li id="adminusuarios_index_mobile">
																	<a  href="<?= site_url("AdminUsuarios")?>"> <?= lang('catalogo_support') ?></a>
																</li>
															<?php endif; ?>

															<?php if ($user_group->id == 1 || $user_group->id == 2): ?>
																<li id="adminusuarios_listadousuarios_mobile">
																	<a  href="<?= site_url("AdminUsuarios/ListadoUsuarios")?>"> <?= lang('users_list') ?></a>
																</li>
															<?php endif; ?>
														</ul>
												</li>

												<?php if ($user_group->id == 1 ): ?>
													<li class="has-sub">
														<a class="js-arrow mm_adminreportes" href="#">
														<i class="fas fa-file"></i><?= lang('reports') ?></a>
															<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
																<li id="adminreportes_index_mobile">
																	<a href="<?= site_url("AdminReportes")?>"> <?= lang('inventory_reports') ?></a>
																</li>
															</ul>
													</li>
												<?php endif; ?>

												<hr>
													<li>
														<a href="<?= site_url('Auth/logout')?>"><i class="fas fa-sign-out"></i><?= lang('logout') ?></a>
													</li>
											</ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->





		<aside class="menu-sidebar d-none d-lg-block">
				<div class="logo">
						<a href="<?= site_url('MainMenu')  ?>">
							<i class="fa fa-home"></i> &nbsp; <?= lang('home') ?>
						</a>
				</div>
				<div class="menu-sidebar__content js-scrollbar1">
					<nav class="navbar-sidebar">
						<ul class="list-unstyled navbar__list">
							<li class="has-sub">
								<a class="js-arrow mm_admincatalogosgaraje" href="#">
								<i class="fas fa-car"></i><?= lang('garage') ?></a>
									<ul class="list-unstyled navbar__sub-list js-sub-list">

										<?php if ($user_group->id == 1 ): ?>
											<li id="admincatalogosgaraje_index">
												<a  href="<?= site_url("AdminCatalogosGaraje")?>"><?= lang('catalogo_support') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id ==1 || $user_group->id ==2 || $user_group->id ==3 || $user_group->id == 4): ?>
											<li id="admincatalogosgaraje_solicitarvehiculo">
												<a  href="<?= site_url("AdminCatalogosGaraje/SolicitarVehiculo")?>"><?= lang('request_vehicle') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id ==1 || $user_group->id ==2 || $user_group->id ==3): ?>
											<li id="admincatalogosgaraje_listadosolicitudes">
												<a  href="<?= site_url("AdminCatalogosGaraje/ListadoSolicitudes")?>"><?= lang('request_list') ?></a>
											</li>

											<li id="admincatalogosgaraje_listadoviajes">
												<a  href="<?= site_url("AdminCatalogosGaraje/ListadoViajes")?>"><?= lang('trips_list') ?></a>
											</li>

											<li id="admincatalogosgaraje_reportarfacturas">
												<a  href="<?= site_url("AdminCatalogosGaraje/ReportarFacturas")?>"><?= lang('add_invoices') ?></a>
											</li>

											<li id="admincatalogosgaraje_listadofacturas">
												<a  href="<?= site_url("AdminCatalogosGaraje/ListadoFacturas")?>"><?= lang('list_invoices') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2 ): ?>
											<li id="admincatalogosgaraje_registraraccidente">
												<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarAccidente")?>"><?= lang('add_accident')?></a>
											</li>

											<li id="admincatalogosgaraje_registrarreparaccion">
												<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarReparaccion")?>"><?= lang('add_repair') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2 ): ?>
											<li id="admincatalogosgaraje_agregarchoferes" >
												<a href="<?= site_url("AdminCatalogosGaraje/AgregarChoferes")?>"><?= lang('driver_list') ?></a>
											</li>

											<li id="admincatalogosgaraje_registrarmantenimiento">
												<a  href="<?= site_url("AdminCatalogosGaraje/RegistrarMantenimiento")?>"><?= lang('add_support') ?></a>
											</li>
										<?php endif; ?>
									</ul>
							</li>

							<li class="has-sub">
								<a class="js-arrow mm_adminsuministros" href="#">
								<i class="fas fa-box"></i>Suministros</a>
									<ul class="list-unstyled navbar__sub-list js-sub-list">

										<?php if ($user_group->id == 1): ?>
											<li id="adminsuministros_index">
												<a  href=<?= site_url("AdminSuministros")?>><?= lang('catalogo_support') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2 || $user_group->id == 3 || $user_group->id == 4): ?>
											<li id="adminsuministros_cargarsolicitud">
												<a  href=<?= site_url("AdminSuministros/CargarSolicitud")?>><?= lang('request_supplies') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2): ?>
											<li id="adminsuministros_listadopedidos" >
												<a class="dropdown-item" href=<?= site_url("AdminSuministros/ListadoPedidos")?>><?= lang('order_list') ?></a>
											</li>

											<li id="adminsuministros_listadoinventario" >
												<a class="dropdown-item" href=<?= site_url("AdminSuministros/ListadoInventario")?>><?= lang('available_inventory') ?></a>
											</li>

											<li id="adminsuministros_proveedores" >
												<a class="dropdown-item" href=<?= site_url("AdminSuministros/proveedores")?>><?= lang('provider') ?></a>
											</li>

											<li id="adminsuministros_compras" >
												<a class="dropdown-item" href=<?= site_url("AdminSuministros/compras")?>><?= lang('sales') ?></a>
											</li>
										<?php endif; ?>
									</ul>
							</li>

							<li class="has-sub">
								<a class="js-arrow mm_admincatalogoscajachica" href="#">
								<i class="fas fa-bank"></i><?= lang('petty_cash') ?></a>
									<ul class="list-unstyled navbar__sub-list js-sub-list">

										<?php if ($user_group->id == 1): ?>
											<li id="admincatalogoscajachica_index">
												<a href="<?= site_url("AdminCatalogosCajaChica")?>"><?= lang('catalogo_support') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2 || $user_group->id == 3 || $user_group->id == 4): ?>
											<li id="admincatalogoscajachica_solicitudcajachica" >
												<a class="dropdown-item" href=<?= site_url("AdminCatalogosCajaChica/SolicitudCajaChica")?>><?= lang('new_request') ?></a>
											</li>

											<li id="admincatalogoscajachica_listadosolicitudescajachica" >
												<a class="dropdown-item" href=<?= site_url("AdminCatalogosCajaChica/ListadoSolicitudesCajaChica")?>><?= lang('request_list') ?></a>
											</li>
										<?php endif; ?>
									</ul>
							</li>

							<li class="has-sub">
								<a class="js-arrow mm_adminusuarios" href="#">
								<i class="fas fa-user-circle"></i><?= lang('users') ?></a>
									<ul class="list-unstyled navbar__sub-list js-sub-list">

										<?php if ($user_group->id == 1 ): ?>
											<li id="adminusuarios_index">
												<a  href="<?= site_url("AdminUsuarios")?>"> <?= lang('catalogo_support') ?></a>
											</li>
										<?php endif; ?>

										<?php if ($user_group->id == 1 || $user_group->id == 2): ?>
											<li id="adminusuarios_listadousuarios">
												<a  href="<?= site_url("AdminUsuarios/ListadoUsuarios")?>"> <?= lang('users_list') ?></a>
											</li>
										<?php endif; ?>
									</ul>
							</li>

							<?php if ($user_group->id == 1 ): ?>
								<li class="has-sub">
									<a class="js-arrow mm_adminreportes" href="#">
									<i class="fas fa-file"></i><?= lang('reports') ?></a>
										<ul class="list-unstyled navbar__sub-list js-sub-list">
											<li id="adminreportes_index">
												<a href="<?= site_url("AdminReportes")?>"> <?= lang('inventory_reports') ?></a>
											</li>
										</ul>
								</li>
							<?php endif; ?>

							<hr>
								<li>
									<a href="<?= site_url('Auth/logout')?>"><i class="fas fa-sign-out"></i><?= lang('logout') ?></a>
								</li>
						</ul>
					</nav>
				</div>
		</aside>



		<script src="<?= $assets ?>vendor/jquery/jquery-3.2.1.min.js"></script>


<div class="page-container">
	<div class="main-content">
		<div class="section__content section__content--p30">
			<div class="container-fluid">
        <?php if ($error): ?>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong><?=  $this->session->flashdata("error") ?></strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
		<?php elseif ($message): ?>
          <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong><?=  $this->session->flashdata("message") ?></strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <?php endif; ?>

					<!--
master = tiene acceso a todo
proveduria = tine acceso a todo menos a mantenimiento de catalogo y control de usuarios
admingaraje = tienen acceso a solicitar al tab de garajes y solicitar suministros y solicitud de caja chica
usuario standar = solicitar vehiculo, solicitar suministros, solicitar caja chica

			-->
