<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= $assets?>vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?= $assets ?>css/animsition.min.css" rel="stylesheet" media="all">



    <title>Login </title>
  </head>
  <body style="background-image:url('<?= $assets ?>images/bg.jpeg'); height: 100%; background-repeat: no-repeat; background-position: unset; background-size: cover;">



  <div class="content"  >
    <div class="container">
      <div class="row" style="margin-top:25%;">
        <div class="col-md-6 card d-flex justify-content-between">
          <div class="row justify-content-center card-body">
            <div class="col-md-12">
              <div class="form-block card-body">
                  <div class="mb-4 justify-content-center">
                    <?php if ($this->session->flashdata("error")): ?>
                      <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong><?=  $this->session->flashdata("error") ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <?php endif; ?>
                  <h3 class="card-title" style="text-align:center;"><?= lang('login_heading') ?> </h3>
                  <p class="mb- card-title" style="text-align:center;"><?= lang('mideplan_welcome') ?></p>
                </div>
                <form action="#" method="post">
                  <div class="form-group first">
                    <label for="username"><?= lang('login_identity_label')?></label>
                    <?php echo form_input($identity);?>
                  <!--  <input type="text" class="form-control" id="username"> -->

                  </div>
                  <div class="form-group last mb-4">
                    <label for="password"><?= lang('login_password_label') ?></label>
                    <?php echo form_input($password);?>


                  </div>

                  <input type="submit" value="<?= lang('login_heading') ?>"  class="btn btn-pill text-white btn-block btn-primary">

                  </div>
                </form>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>


    <script src="<?= $assets ?>vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?= $assets ?>vendor/animsition/animsition.min.js"></script>
    <script src="<?= $assets ?>vendor/bootstrap/js/popper.js"></script>
    <script src="<?= $assets ?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$assets ?>js/main.js"></script>

  </body>
</html>
