
            <div class="card">
                    <div class="card-header">
                        <h2>Listado de Viajes</h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableviajes">
                                <thead>
                                    <tr>
                                         <th><?= lang('ID') ?></th>
                                        <th><?= lang('trip_request')?></th>
                                         <th><?= lang('assigned_driver')?></th>
                                        <th><?= lang('budget_assigned')?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">

                </div>
            </div>

            


        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableviajes').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarListadoViajes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idViaje" },
                            { "data": "Razon" },
                            { "data": "Cedula" },
                            { "data": "PresupuestoAsignado" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>


<div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatablefacturas">
                                <thead>
                                    <tr>
                                        <th><?= lang('ID')?></th>
                                        <th><?= lang('trip') ?></th>
                                        <th><?= lang('invoices') ?></th>
                                        <th><?= lang('total')?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>

            <div class="line-chart js-line-chart">


<div class="card">
<div class="justify-content-center">
<div class="card-header">

                        <h2>Grafico de Gastos por Viaje</h2>
                    </div>

      <canvas id="line-chart" width="800" height="450">


      </canvas>
      <script>
      let chartData = [], label = "";
      $(document).ready(function(){


          var xhttp;
            if (window.XMLHttpRequest) {

              xhttp = new XMLHttpRequest();
              } else {

              xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                chartData = JSON.parse(this.responseText);

                new Chart(document.getElementById("line-chart"), {
            type: 'bar',
            data: {
              labels: chartData.labels,
              datasets: [{
                  data: chartData.data,
                  label: "Niveles de Inventarios en productos",
                  backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                  fill: true
                }
              ]
            },

            options: {
              title: {
                display: true,
                text: 'Comportamiento de nivel de inventario'
              }
            }
          });
              }
            };
            xhttp.open("GET","<?= site_url("AdminCatalogosGaraje/GetGastosViajes")?>" , true);
            xhttp.send("categoria="+$(this).val());


      })

                  </script>
                   <div class="card-footer">
                </div>
      </div>
      </div>
      </div>




        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatablefacturas').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarFacturasViajes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idControlViajes" },
                            { "data": "Razon" },
                            { "data": "Facturas" },
                            { "data": "TotalFactura" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>

