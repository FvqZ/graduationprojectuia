
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatablefacturas">
                                <thead>
                                    <tr>
                                        <th><?= lang('ID')?></th>
                                        <th><?= lang('trip') ?></th>
                                        <th><?= lang('invoices') ?></th>
                                        <th><?= lang('total')?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addviajes" id="addfacturas" class="btn btn-primary" data-toggle="modal" data-target="#addfacturasModal"><?= lang('add_trip_invoice') ?></button>
                </div>
            </div>



            <div class="modal" tabindex="-1" role="dialog" id="addfacturasModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action= '<?=site_url('AdminCatalogosGaraje/InsertFacturasViaje')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title"><?= lang('add_trip_invoice') ?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">



                                            <label for="ListadoViajes" class=" form-control-label"><?= lang('vehicle_request') ?></label><br>
                                              <?php

                                                  foreach ($ListadoViajes as $attr)
                                                  {
                                                  $attrListadoViajes[$attr->idViaje] = $attr->Razon;
                                                  }

                                                  echo form_dropdown('ListadoViajes',$attrListadoViajes, '0', 'class="form-control select2"');
                                                  ?><br>

                                               <label><?= lang('invoices') ?></label><br>
                                               <input type='text' name='dt_facturas' id='Facturas' placeholder="Factura" class="form-control"> </input> <br>
                                               <label><?= lang('total')?></label><br>
                                              <input type='number' name='dt_total' id='Total' placeholder="Monto total de factura" class="form-control" min="0" oninput="this.value = Math.abs(this.value)"/> <br>



                  </form>
                </div>
                <div class="modal-footer">
                  <input class="btn btn-success" type="submit" value="Agregar">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatablefacturas').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarFacturasViajes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idControlViajes" },
                            { "data": "Razon" },
                            { "data": "Facturas" },
                            { "data": "TotalFactura" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>
