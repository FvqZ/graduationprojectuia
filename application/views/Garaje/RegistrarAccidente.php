<div class="card">
                    <div class="card-header">
                        <h2> Vehículos </h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">

                            <table class="table" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th><?=lang('ID') ?></th>
                                                    <th><?= lang('vehicle_plate') ?></th>
                                                    <th><?= lang('VIN')?></th>
                                                    <th><?= lang('year') ?></th>
                                                    <th><?= lang('km') ?></th>
                                                    <th><?= lang('model') ?></th>
                                                    <th><?= lang('color') ?></th>
                                                    <th><?= lang('vehicle_type') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                            </table>

                    </div>

            </div>
            <div class="card-footer">

            </div>

    </div>


            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableAccidentes">
                                <thead>
                                    <tr>
                                        <th><?= lang('ID') ?></th>
                                        <th><?= lang('date') ?></th>
                                        <th><?= lang('accident') ?></th>
                                        <th><?= lang('vehicle') ?></th>
                                        <th><?= lang('accident_type') ?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addAccidente" id="addAccidente" class="btn btn-primary" data-toggle="modal" data-target="#addAccidenteModal"><?= lang('add_accident') ?></button>
                </div>
            </div>



            <div class="modal" tabindex="-1" role="dialog" id="addAccidenteModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action= '<?=site_url('AdminCatalogosGaraje/InsertAccidenteDesc')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title"><?= lang('add_accident') ?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">

                                          <label for="TipoAccidente" class=" form-control-label"><?= lang('accident_type') ?></label><br>
                                              <?php

                                                  foreach ($TipoAccidente["data"] as $attr)
                                                  {
                                                  $attrTipoAccidente[$attr->id] = $attr->Descripcion_Accidente;
                                                  }

                                                  echo form_dropdown('TipoAccidente',$attrTipoAccidente, '0', 'class="form-control select2"');
                                                  ?><br>

                                           <label for="VehiculoSeleccionado" class=" form-control-label"><?= lang('vehicle') ?></label>

                                              <?php

                                                  foreach ($VehiculoSeleccionado["data"] as $attr)
                                                  {
                                                  $attrVehiculoSeleccionado[$attr->id] = $attr->PlacaVehiculo;
                                                  }

                                                  echo form_dropdown('VehiculoSeleccionado',$attrVehiculoSeleccionado, '0', 'class="form-control select2"');
                                                  ?><br>
                                              <label><?= lang('accident_description') ?></label>
                                               <textarea name='dt_DescAccidente' id='DescAccidente' placeholder='Descripción del Accidente' class="form-control" > </textarea> <br>
                                               <label><?= lang('accident_date') ?></label>
                                               <input type='date' name='dt_fecha' id='Fecha' class="form-control" max="<?= date('Y-m-d') ?>" value="<?= date('Y-m-d') ?>" /> <br>



                  </form>
                </div>
                <div class="modal-footer">
                  <input class="btn btn-success" type="submit" value="Agregar">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>





        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatable').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarVehiculo") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "PlacaVehiculo" },
                            { "data": "NumeroVIN" },
                            { "data": "Anno" },
                            { "data": "Kilometraje" },
                            { "data": "Desc_Modelo" },
                            { "data": "Desc_Colores" },
                            { "data": "Desc_TipoVehiculo" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>

        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableAccidentes').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarAccidente") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "fecha", "render":fld },
                            { "data": "desc_Accidente" },
                            { "data": "PlacaVehiculo" },
                            { "data": "Descripcion_Accidente" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }
                });

                //table.draw()
            });
            </script>
