
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableviajes">
                                <thead>
                                    <tr>
                                         <th><?= lang('ID') ?></th>
                                        <th><?= lang('trip_request')?></th>
                                         <th><?= lang('assigned_driver')?></th>
                                        <th><?= lang('budget_assigned')?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addviajes" id="addviajes" class="btn btn-primary" data-toggle="modal" data-target="#addviajesModal"><?= lang('create_new_trip')?></button>
                </div>
            </div>



            <div class="modal" tabindex="-1" role="dialog" id="addviajesModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action= '<?=site_url('AdminCatalogosGaraje/InsertRegistroViaje')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title"><?= lang('create_new_trip')?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">

                                          <label for="SolicitudVehiculoSeleccionada" class=" form-control-label"><?= lang('vehicle_request')?></label>
                                              <?php

                                                  foreach ($SolicitudVehiculoSeleccionada as $attr)
                                                  {
                                                  $attrSolicitudVehiculoSeleccionada[$attr->id] = $attr->Razon;
                                                  }

                                                  echo form_dropdown('SolicitudVehiculoSeleccionada',$attrSolicitudVehiculoSeleccionada, '0', 'class="form-control select2"');
                                                  ?><br>

                                           <label for="ChoferAsignado" class=" form-control-label"><?= lang('driver') ?></label>

                                              <?php

                                                  foreach ($ChoferAsignado as $attr)
                                                  {
                                                  $attrChoferAsignado[$attr->idChoferes] = $attr->Cedula;
                                                  }

                                                  echo form_dropdown('ChoferAsignado',$attrChoferAsignado, '0', 'class="form-control select2"');
                                                  ?><br>
                                               <label><?= lang('budget_assigned') ?></label>
                                               <input type='number' name='dt_presupuesto' id='Presupuesto' placeholder="Presupuesto" class="form-control" min="0" oninput="this.value = Math.abs(this.value)" /> <br>



                  </form>
                </div>
                <div class="modal-footer">
                  <input class="btn btn-success" type="submit" value="Agregar">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableviajes').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarListadoViajes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idViaje" },
                            { "data": "Razon" },
                            { "data": "Cedula" },
                            { "data": "PresupuestoAsignado" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>
