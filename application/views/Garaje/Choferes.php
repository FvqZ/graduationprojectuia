
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableChoferes">
                                <thead>
                                    <tr>
                                        <th><?= lang('ID') ?></th>
                                        <th><?= lang('user') ?></th>
                                        <th><?= lang('driver_type') ?></th>
                                        <th><?= lang('driver_availability')?></th>
                                        <th>Acciones</th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addChoferes" id="addChoferes" class="btn btn-primary" data-toggle="modal" data-target="#addChoferesModal"><?= lang('add_driver')?></button>
                </div>
            </div>



            <div class="modal" tabindex="-1" role="dialog" id="addChoferesModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action= '<?=site_url('AdminCatalogosGaraje/InsertChoferes')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title"><?= lang('add_driver') ?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">

                    <label for="FuncionarioSeleccionado" class=" form-control-label"><?= lang('staff') ?></label><br>

                            <?php

                            foreach ($FuncionarioSeleccionado as $attr)
                            {
                            $attrFuncionarioSeleccionado[$attr->id] = $attr->Cedula;
                            }

                            echo form_dropdown('FuncionarioSeleccionado',$attrFuncionarioSeleccionado, '0', 'class="form-control select2"');
                            //falta el select
                            ?>

                    <label for="TipoChofer" class=" form-control-label"><?= lang('driver_type')?></label><br>

                            <?php

                                foreach ($TipoChofer as $attr)
                                {
                                $attrTipoChofer[$attr->idTipoChofer] = $attr->Desc_Chofer;
                                }

                                echo form_dropdown('TipoChofer',$attrTipoChofer, '0', 'class="form-control select2"');
                                ?>

                    <label for="DisponibilidadChoferes2" class=" form-control-label"><?= lang('driver_availability')?></label><br>

                            <?php

                                foreach ($DisponibilidadChoferes2 as $attr)
                                {
                                $attrDisponibilidadChoferes2[$attr->idDisponibilidadChofer] = $attr->Desc_Disponibilidad;
                                }

                                echo form_dropdown('DisponibilidadChoferes2',$attrDisponibilidadChoferes2, '0', 'class="form-control select2"');
                                ?>



                  </form>
                </div>
                <div class="modal-footer">
                  <input class="btn btn-success" type="submit" value="Agregar">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>







        <script type="text/javascript">

function renderAcciones(row)
{
return `<div class="row">
<div class="col-2">
  <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditChofer")?>/${row.idChoferes}"><i class="fa fa-edit"></i></a>
</div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableChoferes').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarChoferes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idChoferes" },
                            { "data": "Cedula" },
                            { "data": "Desc_Chofer" },
                            { "data": "Desc_Disponibilidad" },
                            { "data": renderAcciones },
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }                          

                });

                //table.draw()
            });
            </script>
