<div class="card">
                    <div class="card-header">
                        <h2> Listado de Vehículos </h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">

                            <table class="table" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('ID') ?></th>
                                                    <th><?= lang('vehicle_plate') ?></th>
                                                    <th><?= lang('VIN') ?></th>
                                                    <th><?= lang('year') ?></th>
                                                    <th><?= lang('km') ?></th>
                                                    <th><?= lang('model') ?></th>
                                                    <th><?= lang('color') ?></th>
                                                    <th><?= lang('vehicle_type') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                            </table>

                    </div>

            </div>
            <div class="card-footer">

            </div>

    </div>


            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableSolicitudVehiculo">
                                <thead>
                                    <tr>
                                         <th><?= lang('ID') ?></th>
                                        <th><?= lang('requested_vehicle') ?></th>
                                         <th><?= lang('staff_request') ?></th>
                                        <th><?= lang('date_out') ?></th>
                                         <th><?= lang('reason') ?></th>
                                         <th><?= lang('request_status') ?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" name="addSolicitudVehiculo" id="addASolicitudVehiculo" class="btn btn-primary" data-toggle="modal" data-target="#addSolicitudVehiculo"><?= lang('request_vehicle') ?></button>
                </div>
            </div>





        <div class="modal" tabindex="-1" role="dialog" id="addSolicitudVehiculo">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title"><?= lang('generate_vehicle_request') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <form action='<?=site_url('AdminCatalogosGaraje/InsertSolicitudVehiculo')?>' method='post'>
              <div class="modal-body">
                <label for="VehiculoSeleccionado" class=" form-control-label"><?= lang('vehicle') ?></label>

                    <?php

                    foreach ($VehiculoSeleccionado["data"] as $attr)
                    {
                    $attrVehiculoSeleccionado[$attr->id] = $attr->PlacaVehiculo;
                    }

                    echo form_dropdown('VehiculoSeleccionado',$attrVehiculoSeleccionado, '0', 'class="form-control select2"');
                    ?><br>

                <label for="FuncionarioSeleccionado" class=" form-control-label"><?= lang('staff') ?></label>

                    <?php

                    foreach ($FuncionarioSeleccionado as $attr)
                    {
                    $attrFuncionarioSeleccionado[$attr->id] = $attr->Cedula;
                    }

                    echo form_dropdown('FuncionarioSeleccionado',$attrFuncionarioSeleccionado, '0', 'class="form-control select2"');
                    //falta el select
                    ?><br>
                <label><?= lang('date_out')?> </label>
                <?php
                $date = new DateTime(); // Date object using current date and time
                $dt= $date->format('Y-m-d\TH:i:s');
                 ?>
                <input type='datetime-local' name='dt_anno' id='FechaSalida' class="form-control" min="<?= $dt ?>" value="<?= $dt ?>" /> <br>
                <label><?= lang('reason') ?> </label>
                <input type='text' name='dt_Razon' id='Razon' placeholder='Razon' class="form-control" /> <br>
              </div>
              <div class="modal-footer">

                <input type="submit" class="btn btn-primary" value="Guardar"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
              </div>
                </form>
            </div>
          </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatable').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarVehiculo") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "PlacaVehiculo" },
                            { "data": "NumeroVIN" },
                            { "data": "Anno" },
                            { "data": "Kilometraje" },
                            { "data": "Desc_Modelo" },
                            { "data": "Desc_Colores" },
                            { "data": "Desc_TipoVehiculo" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }
                });

                //table.draw()
            });
            </script>

        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table_request =  $('#datatableSolicitudVehiculo').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarSolicitudes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "PlacaVehiculo" },
                            { "data": "Cedula" },
                            { "data": "FechaSalida" },
                            { "data": "Razon" },
                            { "data": "Desc_Estado" },
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });

            </script>
