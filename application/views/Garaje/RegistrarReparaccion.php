

<div class="card">
                    <div class="card-header">
                        <h2>Listado de Accidentes</h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">

                            <table class="table" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('ID')?></th>
                                                    <th><?= lang('record_date')?> </th>
                                                    <th><?= lang('accident') ?></th>
                                                    <th><?= lang('repair_status') ?></th>
                                                    <th><?= lang('repair_type') ?></th>
                                                    <th><?= lang('actions')?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                            </table>

                    </div>

            </div>
            <div class="card-footer">
            </div>

    </div>



    <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">

                            <table class="table" id="datatablereparaciones">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('ID') ?></th>
                                                    <th><?= lang('repair_description') ?></th>
                                                    <th><?= lang('date')?></th>
                                                    <th><?= lang('cost') ?></th>
                                                    <th><?= lang('vehicle')?></th>
                                                    <th><?= lang('media') ?></th>
                                                    <th><?= lang('accident') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                            </table>

                    </div>

            </div>
            <div class="card-footer">
                <button type="button" name="addVehiculo" id="addVehiculo" class="btn btn-primary" data-toggle="modal" data-target="#addReparacionNueva"><?= lang('add_repair') ?></button>
            </div>

    </div>






            <div class="modal" tabindex="-1" role="dialog" id="addReparacionNueva">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action= '<?=site_url('AdminCatalogosGaraje/InsertCostoReparacion')?>' method='post'>
                <div class="modal-header">
                  <h5 class="modal-title"><?= lang('add_repair') ?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">

                    <label><?= lang('repair_description') ?></label>
                    <textarea name='dt_DescReparacion' id='DescReparacion' placeholder='Descripción de la Reparación' class="form-control" > </textarea> <br>
                    <label><?= lang('repair_date')?></label>
                    <input type='date' name='dt_fecha' id='Fecha' class="form-control" min="<?= date('Y-m-d') ?>" value="<?= date('Y-m-d') ?>" /> <br>
                    <label><?= lang('cost') ?></label>
                    <input type='number' name='dt_CostoReparacion' id='CostoReparacion' placeholder='Costo Total' class="form-control" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
                    <label for="VehiculoSeleccionado" class=" form-control-label"><?= lang('vehicle') ?></label>

                        <?php

                        foreach ($VehiculoSeleccionado["data"] as $attr)
                        {
                        $attrVehiculoSeleccionado[$attr->id] = $attr->PlacaVehiculo;
                        }

                        echo form_dropdown('VehiculoSeleccionado',$attrVehiculoSeleccionado, '0', 'class="form-control select2"');
                        ?><br>

                    <label>Factura</label>
                    <textarea name='dt_Factura' id='Factura' place holder='Facturas' class="form-control" > </textarea> <br>

                    <label for="AccidenteReparado2" class=" form-control-label"><?= lang('accident') ?></label>

                        <?php

                        foreach ($AccidenteReparado2 as $attr)
                        {
                        $attrAccidenteReparado2[$attr->idAccidentesReparados] = $attr->desc_Accidente;
                        }

                        echo form_dropdown('AccidenteReparado2',$attrAccidenteReparado2, '0', 'class="form-control select2"');
                        ?>



                  </form>
                </div>
                <div class="modal-footer">
                  <input class="btn btn-success" type="submit" value="Agregar">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>

        <div class="line-chart js-line-chart">


<div class="card">
<div class="justify-content-center">
<div class="card-header">

                        <h2>Grafico de Gastos por Vehículo</h2>
                    </div>

      <canvas id="line-chart" width="800" height="450">


      </canvas>
      <script>
      let chartData = [], label = "";
      $(document).ready(function(){


          var xhttp;
            if (window.XMLHttpRequest) {

              xhttp = new XMLHttpRequest();
              } else {

              xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                chartData = JSON.parse(this.responseText);

                new Chart(document.getElementById("line-chart"), {
            type: 'bar',
            data: {
              labels: chartData.labels,
              datasets: [{
                  data: chartData.data,
                  label: "Niveles de Inventarios en productos",
                  backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                  fill: true
                }
              ]
            },

            options: {
              title: {
                display: true,
                text: 'Comportamiento de nivel de inventario'
              }
            }
          });
              }
            };
            xhttp.open("GET","<?= site_url("AdminCatalogosGaraje/GetGastosVehiculo")?>" , true);
            xhttp.send("categoria="+$(this).val());


      })

                  </script>
                   <div class="card-footer">
                </div>
      </div>
      </div>
      </div>




        <script type="text/javascript">

        function renderActions(row)
        {
          return `<div class="row">
          <div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/changeStatusAccidente")?>/${row.idAccidentesReparados}"><i class="fa fa-edit"></i></a></div>
          </div>`;

        }
            $(document).ready(function(e){

            var table =  $('#datatable').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarListadoReparacion") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "idAccidentesReparados" },
                            { "data": "fecha", "render":fld },
                            { "data": "desc_Accidente" },
                            { "data": "Descripcion" },
                            { "data": "Desc_TipoReparacion" },
                            {'data': renderActions }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>

        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatablereparaciones').DataTable(
                {
                "pageLength" : 10,
                "dom": "Bfrtip",
                
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarReparacion") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "Desc_Reparacion" },
                            { "data": "Fecha", "render":fld },
                            { "data": "Costo", "render":convertNumbers },
                            { "data": "PlacaVehiculo" },
                            { "data": "Media" },
                            { "data": "desc_Accidente" }
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }


                });

                //table.draw()
            });
            </script>
