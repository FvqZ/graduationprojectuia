
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Chofer</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminCatalogosGaraje/EditChofer/'.$id)?>' method='post'>
    <div class="modal-body">
        
        <label for="DisponibilidadChoferes2" class=" form-control-label"><?= lang('driver_availability')?></label><br>

                    <?php

                    foreach ($DisponibilidadChoferes2 as $attr)
                    {
                    $attrDisponibilidadChoferes2[$attr->idDisponibilidadChofer] = $attr->Desc_Disponibilidad;
                    }
                    
                    echo form_dropdown('DisponibilidadChoferes2',$attrDisponibilidadChoferes2, $ChoferDisponible->DisponibilidadChofer_idDisponibilidadChofer, 'class="form-control select2"');

                    ?>

                    <br>


    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
