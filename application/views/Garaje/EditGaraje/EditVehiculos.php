
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Vehiculo</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminCatalogosGaraje/EditVehiculo/'.$id)?>' method='post'>
    <div class="modal-body">

        <label>Numero de Placa</label><br>
        <input type='text' name='dt_PlacaVehiculo' id='PlacaVehiculo' class="form-control" value="<?= $vehiculo->PlacaVehiculo ?>" /> <br>
        <label>Disponibilidad</label><br>
        <input type='text' name='dt_disponibilidad' id='Disponibilidad' class="form-control" value="<?= $vehiculo->Disponibilidad ?>" /> <br>
        <label>Kilometraje</label><br>
        <input type='text' name='dt_kilometraje' id='Kilometraje' class="form-control" value="<?= $vehiculo->Kilometraje ?>" /> <br>
        <label for="Color" class=" form-control-label">Color</label><br>
                  <?php
                   // $Color = $Color["data"];

                  foreach ($Color as $attr)
                  {
                    $attrColor[$attr->id] = $attr->Desc_Colores;
                  }

                  echo form_dropdown('Color',$attrColor, $vehiculo->Color_Id, 'class="form-control select2"');
                  ?>
                  <br>
        <label for="Poliza" class=" form-control-label">Poliza</label><br>
            <?php
               // $Poliza = $Poliza["data"];
                foreach ($Poliza as $attr)
                {
                    $attrPoliza[$attr->idPolizas] = $attr->DescPoliza;
                }
            echo form_dropdown('Poliza',$attrPoliza, $vehiculo->Polizas_idPolizas, 'class="form-control select2"');
            ?>
            <br>

    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
