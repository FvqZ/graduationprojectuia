
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/changeStatusAccidente/'.$id)?>' method='post'>
      <div class="modal-body">

      <div class="form-group">
          <label>Nuevo Estado</label><br>
          <?php
          $estado_asignado[""] = "Seleccione un estado";
  
          foreach ($status['data'] as $attr)
          {
          $estado_asignado[$attr->idEstadoReparacion] = $attr->Descripcion;
          }
          echo form_dropdown('dt_status', $estado_asignado, $accidente->EstadoReparacion_idEstadoReparacion, "class='select2 form-control' ");
           ?>
      </div>

      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
