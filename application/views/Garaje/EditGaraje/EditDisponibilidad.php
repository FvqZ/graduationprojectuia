<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Disponibilidad</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminCatalogosGaraje/EditDisponibilidad/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Estado</label><br>
        <input type='text' name='dt_DisponibilidadChofer' placeholder="Disponibilidad" class="form-control" value="<?= $disponibilidad->Desc_Disponibilidad ?>"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
