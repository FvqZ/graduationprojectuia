
            <div class="card">
                    <div class="card-header">
                        <h2><?= $module_name ?></h2>
                    </div>
                <div class="card-body card-block">
                    <div class="form-group">
                            <table class="table" id="datatableSolicitudVehiculo">
                                <thead>
                                    <tr>
                                         <th><?= lang('ID') ?></th>
                                         <th><?= lang('request_vehicle') ?></th>
                                         <th><?= lang('staff_request') ?></th>
                                        <th><?= lang('date_out') ?></th>
                                         <th><?= lang('reason') ?></th>
                                         <th><?= lang('request_status')?></th>
                                         <th><?= lang('actions') ?></th>
                                     </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card-footer">
                </div>
            </div>

            <script>
            function renderDeletePresupuesto(row)
            {

            return `<div class="row">
            <a class="btn btn-info ${row.Desc_Estado !== "Solicitado" ? 'disabled': ''}" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditListado")?>/${row.id}"><i class="fa fa-edit"></i></a>
            </div>`;
            }


</script>




        <script type="text/javascript">
            $(document).ready(function(e){
                var base_url = "<?php echo base_url();?>";
            var table =  $('#datatableSolicitudVehiculo').DataTable(
                {
                "dom": "Bfrtip",
                
                "pageLength": 10,
                "order": [[0, "asc" ]],
                "buttons": ['copy', 'excel', 'pdf'],
                "ajax":{
                            url :  "<?= site_url("AdminCatalogosGaraje/CargarSolicitudes") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                            dataSrc:"data",
                            type : 'GET'
                        },
                        "columns": [
                            { "data": "id" },
                            { "data": "PlacaVehiculo" },
                            { "data": "Cedula" },
                            { "data": "FechaSalida" },
                            { "data": "Razon" },
                            { "data": "Desc_Estado" },
                            { "data": renderDeletePresupuesto },
                          ],
                          "language":{
                            "infoEmpty": "No hay información disponible",
                            "search": "Buscar",
                            "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                            "processing":     "Procesando...",
                            "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                          }

                });

                //table.draw()
            });
            </script>
