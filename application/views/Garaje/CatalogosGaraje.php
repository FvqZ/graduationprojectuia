
<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>

  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-color-tab" data-toggle="pill" href="#pills-color" role="tab" aria-controls="pills-color" aria-selected="true"><?= lang('color') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-tipovehiculo-tab" data-toggle="pill" href="#pills-tipovehiculo" role="tab" aria-controls="pills-tipovehiculo" aria-selected="false"><?= lang('vehicle_type') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-modelo-tab" data-toggle="pill" href="#pills-modelo" role="tab" aria-controls="pills-modelo" aria-selected="false"><?= lang('model') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-reparacion-tab" data-toggle="pill" href="#pills-reparacion" role="tab" aria-controls="pills-reparacion" aria-selected="false"><?= lang('repair_type')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-vehiculo-tab" data-toggle="pill" href="#pills-vehiculo" role="tab" aria-controls="pills-vehiculo" aria-selected="false"><?= lang('vehicle') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-accidente-tab" data-toggle="pill" href="#pills-accidente" role="tab" aria-controls="pills-accidente" aria-selected="false"><?= lang('accident')?>s</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-estado-tab" data-toggle="pill" href="#pills-estado" role="tab" aria-controls="pills-estado" aria-selected="false"><?= lang('repair_status')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-estadosolvehiculo-tab" data-toggle="pill" href="#pills-estadosolvehiculo" role="tab" aria-controls="pills-estadosolvehiculo" aria-selected="false"><?= lang('status_request_vehicle') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-tiporevision-tab" data-toggle="pill" href="#pills-tiporevision" role="tab" aria-controls="pills-tiporevision" aria-selected="false"><?= lang('revision_type')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-tipochofer-tab" data-toggle="pill" href="#pills-tipochofer" role="tab" aria-controls="pills-tipochofer" aria-selected="false"><?= lang('driver_type') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-disponibilidadchofer-tab" data-toggle="pill" href="#pills-disponibilidadchofer" role="tab" aria-controls="pills-disponibilidadchofer" aria-selected="false"><?= lang('driver_availability') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-poliza-tab" data-toggle="pill" href="#pills-poliza" role="tab" aria-controls="pills-poliza" aria-selected="false"><?= lang('policy_secures') ?></a>
    </li>
  </ul>

  <div class="tab-content" id="pills-tabContent">

    <!--  tabla de colores-->
      <div class="tab-pane fade show active" id="pills-color" role="tabpanel" aria-labelledby="pills-color-tab">
        <div class="card-body">
          <table class="table" id="colores">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('color')?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addColor"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('color')?></button>
        </div>
      </div>
    <!-- /tabla de colores -->

    <!-- tabla de tipo vehiculo -->
      <div class="tab-pane fade" id="pills-tipovehiculo" role="tabpanel" aria-labelledby="pills-tipovehiculo-tab">
        <div class="card-body">
          <table class="table" id="tipoVehiculo" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('vehicle_type') ?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addTipoVehiculo"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('vehicle_type') ?></button>
        </div>
      </div>
    <!-- / tabla tipo vehiculo-->

    <!-- modelo -->
      <div class="tab-pane fade" id="pills-modelo" role="tabpanel" aria-labelledby="pills-modelo-tab">
        <div class="card-body">
          <table class="table" id="Modelo" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('model') ?>s</th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addModelo"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('model')?></button>
        </div>
      </div>
    <!-- /modelo-->

    <!-- tipo Reparacion-->
      <div class="tab-pane fade" id="pills-reparacion" role="tabpanel" aria-labelledby="pills-reparacion-tab">
        <div class="card-body">
          <table class="table" id="tipoReparacion" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('repair_type') ?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addReparacion"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('repair_type') ?></button>
        </div>
      </div>
    <!-- /tipo Reparacion-->

    <!-- vehiculo-->
      <div class="tab-pane fade" id="pills-vehiculo" role="tabpanel" aria-labelledby="pills-vehiculo-tab">
        <div class="card-body">
          <table class="table" id="vehiculo" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('vehicle_plate')?></th>
                <th><?= lang('VIN')?></th>
                <th><?= lang('year') ?></th>
                <th><?= lang('availability')?></th>
                <th><?= lang('km')?></th>
                <th><?=  lang('model')?></th>
                <th><?= lang('color')?></th>
                <th><?= lang('vehicle_type')?></th>
                <th><?= lang('policy')?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addVehiculo"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('vehicle')?></button>
        </div>
      </div>
    <!-- /vehiculo -->

    <!--  tabla de Accidentes-->
      <div class="tab-pane fade" id="pills-accidente" role="tabpanel" aria-labelledby="pills-accidente-tab">
        <div class="card-body">
          <table class="table" id="accidente" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('accident')?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addAccidente"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('accident')?> </button>
        </div>
      </div>
    <!-- /tabla de Accidentes -->

    <!--  tabla de <?= lang('repair_status')?>-->
      <div class="tab-pane fade" id="pills-estado" role="tabpanel" aria-labelledby="pills-estado-tab">
        <div class="card-body">
          <table class="table" id="estado" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('repair_status')?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="add<?= lang('status')?>"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('status')?></button>
        </div>
      </div>
    <!-- /tabla de <?= lang('status')?> -->

    <!--  tabla de <?= lang('status')?> Sol Vehiculo-->
      <div class="tab-pane fade" id="pills-estadosolvehiculo" role="tabpanel" aria-labelledby="pills-estadosolvehiculo-tab">
        <div class="card-body">
          <table class="table" id="estadosolvehiculo" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('status')?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addestadosolvehiculo"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('status')?> de Solicitud</button>
        </div>
      </div>
    <!-- /tabla de <?= lang('status')?> Sol Vehiculo -->

    <!--  tabla de Tipo de Revision-->
      <div class="tab-pane fade" id="pills-tiporevision" role="tabpanel" aria-labelledby="pills-tiporevision-tab">
        <div class="card-body">
          <table class="table" id="tiporevision" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('revision_type') ?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addtiporevision"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('revision_type') ?></button>
        </div>
      </div>
    <!-- /tabla de Tipo de Revision -->

    <!--  tabla de Tipo de chofer-->
      <div class="tab-pane fade" id="pills-tipochofer" role="tabpanel" aria-labelledby="pills-tipochofer-tab">
        <div class="card-body">
          <table class="table" id="tipochofer" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('driver_type') ?></th>
                <th> <?= lang('actions')?></th>
              </tr>
            </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addtipochofer"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('driver_type') ?></button>
      </div>
    </div>
  <!-- /tabla de Tipo de chofer -->


  <!--  tabla de disponibilidad de chofer-->
    <div class="tab-pane fade" id="pills-disponibilidadchofer" role="tabpanel" aria-labelledby="pills-disponibilidadchofer-tab">
      <div class="card-body">
        <table class="table" id="disponibilidadchofer" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('availability')?></th>
              <th> <?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="adddisponibilidadchofer"><i class="fa fa-plus"></i><?= lang('add')?> Tipo de Disponibilidad</button>
      </div>
    </div>
  <!-- /tabla de disponibilidad de chofer -->

  <!--  tabla de Polizas-->
    <div class="tab-pane fade" id="pills-poliza" role="tabpanel" aria-labelledby="pills-poliza-tab">
      <div class="card-body">
        <table class="table" id="poliza" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('policy_type')?></th>
              <th> <?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
      <button class="btn btn-primary" id="addpoliza"><i class="fa fa-plus"></i><?= lang('add')?> <?= lang('policy') ?></button>
      </div>
    </div>
  <!-- /tabla de Polizas -->
  </div>
    </div>


<!----------------- Modelos --------------->


<!-- Modelo colores-->
<div class="modal" tabindex="-1" role="dialog" id="modelColor">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('color') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertColores')?>' method='post'>
      <div class="modal-body">
        <div class="form-group">
        <label><?= lang('color')?></label><br>
          <input type='text' name='dt_color' placeholder="Color" class="form-control" maxlength="45"/> <br>
        </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo colores-->


<!-- Modelo tipo vehiculo-->
<div class="modal" tabindex="-1" role="dialog" id="modelTipoVehiculo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('vehicle_type') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoVehiculo')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label><?= lang('vehicle_type')?></label><br>
          <input type='text' name='dt_tipovehiculo' placeholder="Tipo de Vehículo" class="form-control" maxlength="45"/> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo vehiculo-->

<!-- Modelo Accidente-->
<div class="modal" tabindex="-1" role="dialog" id="modelAccidente" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('accident') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertAccidente')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label><?= lang('accident')?></label><br>
          <input type='text' name='dt_Accidente' placeholder="Tipo de Accidente" class="form-control" maxlength="45"/> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo Accidente-->



<!-- Modelo Modelo-->
<div class="modal" tabindex="-1" role="dialog" id="modelModelo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('model')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertModelo')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label><?= lang('model') ?></label><br>
          <input type='text' name='dt_modelo' placeholder="Modelo" class="form-control" maxlength="45"/> <br>
      </div>


      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo Modelo-->


<!-- Modelo tipo reparacion-->
<div class="modal" tabindex="-1" role="dialog" id="modelReparacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> Reparaciones</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoReparacion')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('repair_type')?></label><br>
            <input type='text' name='dt_Reparacion' placeholder="Tipo de Reparacion"  id='category' class="form-control" maxlength="45" /> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo reparacion-->



<!-- Modelo vehiculo-->
<div class="modal" tabindex="-1" role="dialog" id="modelVehiculo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('vehicle') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertVehiculo')?>' method='post'>
      <div class="modal-body">
        <label><?= lang('vehicle_plate')?> </label><br>
        <input type='text' name='dt_PlacaVehiculo' id='PlacaVehiculo' placeholder="Placa de Vehículo" class="form-control" maxlength="10" /> <br>
        <label><?= lang('VIN')?></label><br>
        <input type='number' name='dt_VIN' id='NumeroVIN' placeholder="Número VIN" class="form-control" maxlength="100" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
        <label><?= lang('year')?> </label><br>
        <?php
        $max_year = date('Y') + 1;

        for ($i=$max_year - 30; $i <= $max_year; $i++)
        {
            $data[$i] = $i;
        }

        echo form_dropdown('dt_anno', $data, date('Y'), 'class="select2 form-control"');

         ?>
    <!--     <input type='number' maxlength="4" name='dt_anno' id='Anno' placeholder="Año" class="form-control" min="1945" max="<?= date('Y') + 1 ?>" oninput="this.value = Math.abs(this.value)" step="1" /> --><br>
        <label><?= lang('availability')?> </label><br>

        <select class="form-control select2" name="dt_disponibilidad">
          <option value="1">Disponible</option>
          <option value="2">Sin Disponibilidad</option>
        </select>

        <br>
        <label><?= lang('km')?> </label><br>
        <input type='number' name='dt_kilometraje' id='Kilometraje' placeholder="Kilometraje" class="form-control" maxlength="45" min="0" oninput="this.value = Math.abs(this.value)"/> <br>
        <label for="Modelo" class=" form-control-label"><?= lang('model')?></label><br>
      <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                  <?php
                  $Modelo = $Modelo["data"];

                  foreach ($Modelo as $attr)
                  {
                    $attrModelo[$attr->id] = $attr->Desc_Modelo;
                  }

                  echo form_dropdown('Modelo',$attrModelo, '1', 'class="form-control select2"');
                  ?>
                  <br>
        <label for="Color" class=" form-control-label"><?= lang('color')?></label><br>
      <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                  <?php

                  foreach ($Color as $attr)
                  {
                    $attrColor[$attr->id] = $attr->Desc_Colores;
                  }

                  echo form_dropdown('Color',$attrColor, '1', 'class="form-control select2"');
                  ?>
                  <br>
         <label for="TipoVehiculo" class=" form-control-label"><?= lang('vehicle_type') ?></label><br>
      <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                  <?php

                  foreach ($TipoVehiculo as $attr)
                  {
                    $attrTipoVehiculo[$attr->id] = $attr->Desc_TipoVehiculo;
                  }

                  echo form_dropdown('TipoVehiculo',$attrTipoVehiculo, '1', 'class="form-control select2"');
                  ?>
         <label for="Poliza" class=" form-control-label"><?= lang('policy')?></label><br>
      <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                  <?php

                  foreach ($Poliza as $attr)
                  {
                    $attrPoliza[$attr->idPolizas] = $attr->DescPoliza;
                  }

                  echo form_dropdown('Poliza',$attrPoliza, '1', 'class="form-control select2"');
                  ?>


      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo vehiculo-->

<!-- Modelo estado-->
<div class="modal" tabindex="-1" role="dialog" id="model<?= lang('status')?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('repair_status')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoEstadoVehiculo')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('status')?></label><br>
            <input type='text' name='dt_EstadoReparacion' id='category' placeholder="Estado de Reparación" class="form-control"> </input> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo estado-->

<!-- Modelo estado Sol Vehiculo-->
<div class="modal" tabindex="-1" role="dialog" id="modelestadosolvehiculo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('status')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoEstadoSolVehiculo')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('status')?></label><br>
            <input type='text' name='dt_<?= lang('status')?>' id='category' placeholder="Estado de Solicitud" class="form-control"> </input> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo estado Sol Vehiculo-->

<!-- Modelo estado Tipo Revision-->
<div class="modal" tabindex="-1" role="dialog" id="modeltiporevision">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('revision_type')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoRevision')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('revision')?></label><br>
            <input type='text' name='dt_Revision' id='category' placeholder="Tipo de Revision" class="form-control" maxlength="45" /> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo estado Tipo Revision-->

<!-- Modelo estado Tipo Chofer-->
<div class="modal" tabindex="-1" role="dialog" id="modeltipochofer">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('driver_type')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertTipoChofer')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('driver_type')?></label><br>
            <input type='text' name='dt_TipoChofer' id='category' placeholder="Tipo de Chofer" class="form-control" maxlength="45"/> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo estado Tipo Chofer-->

<!-- Modelo estado disponibilidad Chofer-->
<div class="modal" tabindex="-1" role="dialog" id="modeldisponibilidadchofer">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('availability_type') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertDisponibilidadChofer')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('driver_availability') ?></label><br>
            <input type='text' name='dt_DisponibilidadChofer' id='category' placeholder="Disponibilidad de Chofer" class="form-control" maxlength="45" /> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo estado disponibilidad Chofer-->

<!-- ModeloPoliza-->
<div class="modal" tabindex="-1" role="dialog" id="modelpoliza">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= lang('add')?> <?= lang('policy')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminCatalogosGaraje/InsertPoliza')?>' method='post'>
      <div class="modal-body">
          <label><?= lang('policy')?></label><br>
            <input type='text' name='dt_Poliza' id='category' placeholder="Poliza" class="form-control" maxlength="45"/> <br>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / ModeloPolizar-->

<script>

$("#addColor").on("click",function(){
  $('#modelColor').modal("show")
})

$("#addTipoVehiculo").on("click",function(){
  $('#modelTipoVehiculo').modal("show")
})

$("#addModelo").on("click",function(){
  $('#modelModelo').modal("show")
})
$("#addReparacion").on("click",function(){
  $('#modelReparacion').modal("show")
})

$("#addVehiculo").on("click",function(){
  $('#modelVehiculo').modal("show")
})

$("#addAccidente").on("click",function(){
  $('#modelAccidente').modal("show")
})

$("#add<?= lang('status')?>").on("click",function(){
  $('#model<?= lang('status')?>').modal("show")
})

$("#addestadosolvehiculo").on("click",function(){
  $('#modelestadosolvehiculo').modal("show")
})

$("#addtiporevision").on("click",function(){
  $('#modeltiporevision').modal("show")
})

$("#addtipochofer").on("click",function(){
  $('#modeltipochofer').modal("show")
})

$("#adddisponibilidadchofer").on("click",function(){
  $('#modeldisponibilidadchofer').modal("show")
})

$("#addpoliza").on("click",function(){
  $('#modelpoliza').modal("show")
})

function isActive(id)
        {
                return id == 1 ? `<span class="text-success">Disponible</span>` : `<span class="text-warning">Sin Disponibilidad</span>`;
        }

function renderDeleteTipoCedula(row)
{

return `<div class="row">
  <div class="col-1"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminUsuarios/EditTipoCedula")?>/${row.idTipoCedula}"><i class="fa fa-edit"></i></a></div>
  <div class="col-1"><a class='btn btn-danger' href='<?= site_url("AdminUsuarios/BorrarTipoCedula")?>?id=${row.idTipoCedula}"'><i class='fa fa-trash'></i></a> </div>
</div>`;

}


function renderDeleteColor(row)
{

return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditColor")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarColor")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;

}


function renderDeleteTipoVehiculo(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditTipoVehiculo")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarTipoVehiculo")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteModelo(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditModelo")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarModelo")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteReparacion(row)
{

return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditTipoReparacion")?>/${row.idTipoReparacion}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarReparacion")?>?id=${row.idTipoReparacion}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteVehiculo(row)
{
return `<div class="row">
<div class="col-4"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditVehiculo")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-4"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarVehiculo")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteAccidente(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditAccidente")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarAccidente")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDelete<?= lang('status')?>(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditEstado")?>/${row.id<?= lang('status')?>Reparacion}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarEstadoVehiculo")?>?id=${row.id<?= lang('status')?>Reparacion}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDelete<?= lang('status')?>SolVehiculo(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditEstadoSol")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarEstadoSolVehiculo")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteTipoRevision(row)
{
return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditTipoRevision")?>/${row.idTipoRevision}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarTipoRevision")?>?id=${row.idTipoRevision}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteTipoChofer(row)
{

return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditTipoChofer")?>/${row.idTipoChofer}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarTipoChofer")?>?id=${row.idTipoChofer}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteDisponibilidadChofer(row)
{

return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditDisponibilidad")?>/${row.idDisponibilidadChofer}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarDisponibilidadChofer")?>?id=${row.idDisponibilidadChofer}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeletePoliza(row)
{

return `<div class="row">
<div class="col-1"> <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminCatalogosGaraje/EditPoliza")?>/${row.idPolizas}"><i class="fa fa-edit"></i></a></div>
<div class="col-1"> <a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminCatalogosGaraje/BorrarPoliza")?>?id=${row.idPolizas}"'><i class='fa fa-trash'></i></a></div>

</div>`;
}

$(document).ready(function(e){


    $('#colores').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url : "<?= site_url("AdminCatalogosGaraje/CargarColores")?>",// base_url+'Index.php/AdminCatalogosGaraje/CargarColores',
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Colores" },
                { "data": renderDeleteColor }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable


    $('#tipoVehiculo').DataTable(
      {
      "pageLength" : 10,
      
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminCatalogosGaraje/CargarTipoVehiculo")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_TipoVehiculo" },
                {"data": renderDeleteTipoVehiculo}
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

    $('#Modelo').DataTable(
      {
      "pageLength" : 10,
      
      "dom": "Bfrtip",
      
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminCatalogosGaraje/CargarModelo")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Modelo" },
                { "data": renderDeleteModelo  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable


    $('#tipoReparacion').DataTable(
      {
      "pageLength" : 10,
      
      "dom": "Bfrtip",
      
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?=site_url("AdminCatalogosGaraje/CargarTipoReparacion")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idTipoReparacion" },
                { "data": "Desc_TipoReparacion" },
                { "data": renderDeleteReparacion }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });


    $('#vehiculo').DataTable(
        {
        "pageLength" : 10,
        "dom": "Bfrtip",
      
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarVehiculo") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "id" },
                  { "data": "PlacaVehiculo" },
                  { "data": "NumeroVIN" },
                  { "data": "Anno" },
                  { "data": "Disponibilidad" , "render":isActive },
                  { "data": "Kilometraje" },
                  { "data": "Desc_Modelo" },
                  { "data": "Desc_Colores" },
                  { "data": "Desc_TipoVehiculo" },
                  { "data": "DescPoliza" },

                  { "data": renderDeleteVehiculo }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

      $('#accidente').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?=site_url("AdminCatalogosGaraje/CargarTipoAccidente")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Descripcion_Accidente" },
                { "data": renderDeleteAccidente }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

    $('#estado').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarTipoEstadoVehiculo") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "id<?= lang('status')?>Reparacion" },
                  { "data": "Descripcion" },
                  { "data": renderDeleteEstado }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

      $('#estadosolvehiculo').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarEstadoSolVehiculo") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "id" },
                  { "data": "Desc_Estado" },
                  { "data": renderDeleteEstadoSolVehiculo }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

      $('#tiporevision').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarTipoRevision") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "idTipoRevision" },
                  { "data": "Desc_TipoRevision" },
                  { "data": renderDeleteTipoRevision }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

      $('#tipochofer').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarTipoChofer") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "idTipoChofer" },
                  { "data": "Desc_Chofer" },
                  { "data": renderDeleteTipoChofer }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

      $('#disponibilidadchofer').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarDisponibilidadChofer") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "idDisponibilidadChofer" },
                  { "data": "Desc_Disponibilidad" },
                  { "data": renderDeleteDisponibilidadChofer }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });


      $('#poliza').DataTable(
        {
        "pageLength" : 10,
         "dom": "Bfrtip",
        
        "order": [[0, "asc" ]],
        "buttons": ['copy', 'excel', 'pdf'],
        "ajax":{
                  url :  "<?= site_url("AdminCatalogosGaraje/CargarPoliza") ?>",//base_url+'Index.php/AdminCatalogosGaraje/CargarVehiculo',
                  dataSrc:"data",
                  type : 'GET'
                },
                "columns": [
                  { "data": "idPolizas" },
                  { "data": "DescPoliza" },
                  { "data": renderDeletePoliza }
                ],
                "language":{
                  "infoEmpty": "No hay información disponible",
                  "search": "Buscar",
                  "paginate": {
                          "first":      "Primero",
                          "last":       "Último",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  "processing":     "Procesando...",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                }

      });

  }); // End Document Ready Function

</script>
