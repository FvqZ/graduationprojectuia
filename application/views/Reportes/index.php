<?php
$v = '?v=1';

if ($this->input->post('fecha_inicio'))
{
  $v.='&fecha_inicio='.$this->input->post('fecha_inicio');
}

if ($this->input->post('fecha_final'))
{
  $v.= '&fecha_final='.$this->input->post('fecha_final');
}

if ($this->input->post('producto'))
{
 $v.= '&producto='.$this->input->post('producto');
}

if($this->input->post('user'))
{
  $v.="&user=".$this->input->post('user');
}


 ?>


<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>
  <div class="card-body">

    <div class="form-group">
      <form action="<?= site_url('AdminReportes') ?>" method="post">
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label for="fecha_inicio" class="form-label">Fecha Inicio</label>
              <input type="date" name="fecha_inicio" class="form-control" />
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="fecha_final" class="form-label">Fecha Final</label>
              <input type="date" name="fecha_final" class="form-control" />
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="producto" class="form-label">Producto</label>
              <?php
              $productos[""] = "--Seleccione un producto--";
              foreach ($producto_data as $row)
              {
                $productos[$row->id] = $row->DescripcionInv;
              }
              echo form_dropdown('producto', $productos, '', "class='select2 form-control'");
               ?>
            </div>
          </div>
          <!--Comentado pq inventario no tiene usuarios
          <div class="col-4">
            <div class="form-group">
              <label for="fecha_final" class="form-label">Usuario</label>
              <input type="date" name="fecha_final" value="<?=date('Y-m-d') ?>" class="form-control" />
            </div>
          </div>-->

        </div>
        <div class="row">
          <div class="col-12">
            <input type="submit" name="submit" class="btn btn-info" value="Enviar">
          </div>
        </div>
      </form>
    </div>

    <div class="form-group">
      <table class="table" id="reporte_inventario">
          <thead>
              <tr>
                <th>ID</th>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Cantidad actual</th>
                <th>Costo</th>
                <th>Fecha de Ingreso</th>
                <th>Preferencial</th>
                <th>Zona</th>
                <th>Cantidad mínima</th>
               </tr>
          </thead>
          <tbody>
          </tbody>
      </table>

    </div>


  </div>
  <div class="card-footer">

  </div>
</div>

<script type="text/javascript">


$(document).ready(function(){

  $('#reporte_inventario').DataTable(
    {
    "pageLength" : 10,
    "dom": "Bfrtip",
    "order": [[0, "asc" ]],
    "buttons": ['copy', 'excel', 'pdf'],
    "ajax":{
              url :"<?= site_url("AdminReportes/getInventario".$v) ?>",//  base_url+'Index.php/AdminSuministros/CargarInventarios',
              dataSrc:"data",
              type : 'GET'
            },
            "columns": [

              { "data": "id" },
              { "data": "Desc_Categorias" },
              { "data": "DescripcionInv" },
              { "data": "Cantidad", "render":convertNumbers },
              { "data": "costo", "render":convertNumbers },
              { "data": "FechaIngreso", "render": fld },
              {"data": "Desc_Preferencial"},
              {"data": "Desc_Zona"},
              {"data":"cantidadminima"}
              ]
  });


})

</script>
