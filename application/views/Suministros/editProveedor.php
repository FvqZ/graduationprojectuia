
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/EditProveedor/'.$id)?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Tipo de Proveedor</label><br>
          <?php
          $tipo_prov[""] = "Seleccione un tipo de proveedor";
          foreach ($TipoProveedor["data"] as $row)
          {
            $tipo_prov[$row->id] = $row->Desc_TipoProveedor;
          }
          echo form_dropdown('edit_TipoProveedor', $tipo_prov, $proveedor->idTipoProveedor, "class='select2 form-control' ");
           ?>
      </div>

      <div class="form-group">
          <label>Nombre</label><br>
          <input type='text' name='edit_nombre' placeholder="Nombre" class="form-control" value="<?= $proveedor->NombreProveedor ?>" />
      </div>

      <div class="form-group">
          <label>Contactos</label><br>
          <input type='text' name='edit_contactos' placeholder="Contactos" class="form-control" value="<?= $proveedor->Contactos ?>"/>
      </div>

      <div class="form-group">
          <label>Correo Electrónico</label><br>
          <input type='email' name='edit_email' placeholder="Correo Electrónico" class="form-control" value="<?= $proveedor->Email ?>" />
      </div>


      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
