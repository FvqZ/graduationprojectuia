
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar a Inventario</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/EditInventario/'.$id)?>' method='post'>
    <div class="modal-body">

    <label for="Categoria" class=" form-control-label">Categoría</label><br>
    <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                <?php
                $Categoria = $Categoria["data"];

                foreach ($Categoria as $attr)
                {
                  $attrCategoria[$attr->id] = $attr->Desc_Categorias;
                }

                echo form_dropdown('Categoria',$attrCategoria, $inventario->Id_RefCategoria, 'class="form-control select2"');
                ?>
                <br>

      <label>Item </label><br>
      <input type='text' name='dt_item' id='ItemInv' class="form-control" value="<?= $inventario->DescripcionInv ?>" maxlength="150"/> <br>
      <label>Cantidad </label><br>
      <input type='number' name='dt_cantidad' id='Cantidad' class="form-control" value="<?= $inventario->Cantidad ?>" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
      <label>Costo </label><br>
      <input type='number' name='dt_unitario' id='Precio' class="form-control" value="<?= $inventario->costo ?>" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
      <label>Cantidad Mínima </label><br>
      <input type='number' name='dt_minimo' id='CantidadMinima' class="form-control" value="<?= $inventario->cantidadminima ?>" min="0" oninput="this.value = Math.abs(this.value)"/> <br>
       <label>Fecha de Ingreso </label><br>

      <input type='date' name='dt_fecha' id='Anno' class="form-control" value="<?= date('Y-m-d', strtotime($inventario->FechaIngreso)) ?>" /> <br>

      <label for="Preferencial" class=" form-control-label">Preferencial</label><br>
    <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                <?php
                $Preferencial = $Preferencial["data"];

                foreach ($Preferencial as $attr)
                {
                  $attrPreferencial[$attr->id] = $attr->Desc_Preferencial;
                }

                echo form_dropdown('Preferencial',$attrPreferencial, $inventario->Preferencial_id, 'class="form-control select2"');
                ?>
                <br>
      <label for="Zona" class=" form-control-label">Zona</label><br>
    <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                <?php
                $Zona = $Zona["data"];

                foreach ($Zona as $attr)
                {
                  $attrZona[$attr->idZona] = $attr->Desc_Zona;
                }

                echo form_dropdown('Zona',$attrZona, $inventario->Zona_idZona, 'class="form-control select2"');
                ?>
                <br>
                <label for="Edificio" class=" form-control-label">Edificio</label><br>
            <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                        <?php
                        $Edificio = $Edificio["data"];

                        foreach ($Edificio as $attr)
                        {
                          $attrEdificio[$attr->idRegion] = $attr->Desc_Region;
                        }

                        echo form_dropdown('Edificio',$attrEdificio, $inventario->region_idRegion, 'class="form-control select2"');
                        ?>
                        <br>

    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
