
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar a Cantidad de <?= $inventario->DescripcionInv ?></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/sumInventario/'.$id)?>' method='post'>
    <div class="modal-body">
      <div class="form-group">
        <label>Cantidad </label><br>
        <input type='number' name='dt_cantidad' id='Cantidad' class="form-control" value="" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
      </div>
    </div>
    <div class="modal-footer">
      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
