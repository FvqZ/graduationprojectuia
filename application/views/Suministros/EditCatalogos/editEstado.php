
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Estado</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/EditEstadoSuministro/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Descripción de Estado Solicitud</label><br>
        <input type='text' name='dt_EstadoSolicitud' placeholder="Estado" class="form-control" value="<?= $estado->Descripcion ?>" maxlength="45" /><br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
