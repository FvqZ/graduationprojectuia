<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Edificio</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/EditEdificio/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Edificio</label><br>
        <input type='text' name='dt_Edificio' placeholder="Edificio" class="form-control" value="<?= $edificio->Desc_Region ?>" maxlength="45"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
