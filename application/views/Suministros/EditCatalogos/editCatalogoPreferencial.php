<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Agregar Estado Preferencial</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/EditCatalogoPreferencial/'.$id)?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Estado Preferencial</label><br>
        <input type='text' name='dt_Preferencial' placeholder="Preferencial" class="form-control" value="<?= $estado->Desc_Preferencial ?>" maxlength="45"/> <br>
    </div>
    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
