<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>


  <!-- tabla de tipo Inventario -->
  <div class="card-body">
    <table class="table" id="Inventario" style="width:100%;">
      <thead>
        <tr>
          <th>ID</th>
          <th><?= lang('category')?></th>
          <th><?= lang('description')?></th>
          <th>Cantidad</th>
          <th>Costo</th>
          <th>Fecha de Ingreso</th>
          <th>Item Preferencial</th>
          <th>Nivel Mínimo</th>
          <th>Zona</th>
          <th>Edificio</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="card-footer">
    <button class="btn btn-primary" id="addInventario"><i class="fa fa-plus"></i>&nbsp;Agregar a Nuevo Item a Inventario</button>
  </div>
</div>





      <!-- Modelo Inventario-->
      <div class="modal" tabindex="-1" role="dialog" id="modelInventario">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Agregar a Inventario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <form action= '<?=site_url('AdminSuministros/InsertInv')?>' method='post'>
            <div class="modal-body">


            <label for="Categoria" class=" form-control-label"><?= lang('category')?></label><br>
            <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                        <?php
                        $Categoria = $Categoria["data"];

                        foreach ($Categoria as $attr)
                        {
                          $attrCategoria[$attr->id] = $attr->Desc_Categorias;
                        }

                        echo form_dropdown('Categoria',$attrCategoria, '1', 'class="form-control select2"');
                        ?>
                        <br>

              <label>Item </label><br>
              <input type='text' name='dt_item' id='ItemInv' class="form-control"> </input> <br>
              <label>Cantidad </label><br>
              <input type='number' min="0" oninput="this.value = Math.abs(this.value)" name='dt_cantidad' id='Cantidad' class="form-control" /> <br>
              <label>Costo </label><br>
              <input type='number' name='dt_unitario' id='Precio' class="form-control" min="0" oninput="this.value = Math.abs(this.value)"  /> <br>
              <label>Nivel Mínimo </label><br>
              <input type='number' name='dt_minimo' id='Precio' class="form-control" min="0" oninput="this.value = Math.abs(this.value)" /> <br>
              <label>Fecha de Ingreso </label><br>
              <input type='date' name='dt_fecha' id='Anno' class="form-control" value="<?= date('Y-m-d') ?>" />
              <span class="hidden text-danger" id="validate_fecha">La fecha no debe ser mayor al día de hoy</span>

              <br>
              <label for="Preferencial" class=" form-control-label">Preferencial</label><br>
            <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                        <?php
                        $Preferencial = $Preferencial["data"];

                        foreach ($Preferencial as $attr)
                        {
                          $attrPreferencial[$attr->id] = $attr->Desc_Preferencial;
                        }

                        echo form_dropdown('Preferencial',$attrPreferencial, '1', 'class="form-control select2"');
                        ?>
                        <br>
              <label for="Zona" class=" form-control-label">Zona</label><br>
            <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                        <?php
                        $Zona = $Zona["data"];

                        foreach ($Zona as $attr)
                        {
                          $attrZona[$attr->idZona] = $attr->Desc_Zona;
                        }

                        echo form_dropdown('Zona',$attrZona, '1', 'class="form-control select2"');
                        ?>
                        <br>

                        <label for="Edificio" class=" form-control-label">Edificio</label><br>
            <!--  <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                        <?php
                        $Edificio = $Edificio["data"];

                        foreach ($Edificio as $attr)
                        {
                          $attrEdificio[$attr->idRegion] = $attr->Desc_Region;
                        }

                        echo form_dropdown('Edificio',$attrEdificio, '1', 'class="form-control select2"');
                        ?>
                        <br>


            </div>
            <div class="modal-footer">

              <input type="submit" class="btn btn-primary" value="Guardar"/>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
              </form>
          </div>
        </div>
      </div>
      <!-- / Modelo Inventario-->


      <div class="line-chart js-line-chart">
      <!--<label for="Categoria" class=" form-control-label">Item</label><br>

                    <?php
            $attrCategoria[0] = "Seleccione un producto";
              foreach ($Categoria as $attr)
              {
                $attrCategoria[$attr->id] = $attr->Desc_Categorias;
              }

              echo form_dropdown('Item',$attrCategoria, '1', 'class="form-control select2" id="categoriaChart"');
              ?>-->

<div class="card">
<div class="justify-content-center">
<div class="card-header">

                        <h2>Niveles de Inventario</h2>
                    </div>

      <canvas id="line-chart" width="800" height="450">


      </canvas>
      <script>
      let chartData = [], label = "";
      $(document).ready(function(){


          var xhttp;
            if (window.XMLHttpRequest) {

              xhttp = new XMLHttpRequest();
              } else {

              xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                chartData = JSON.parse(this.responseText);

                new Chart(document.getElementById("line-chart"), {
            type: 'bar',
            data: {
              labels: chartData.labels,
              datasets: [{
                  data: chartData.data,
                  label: "Niveles de Inventarios en productos",
                  backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                  fill: true
                }
              ]
            },

            options: {
              title: {
                display: true,
                text: 'Comportamiento de nivel de inventario'
              }
            }
          });
              }
            };
            xhttp.open("GET","<?= site_url("AdminSuministros/GetInventarioPorMes")?>" , true);
            xhttp.send("categoria="+$(this).val());


      })

                  </script>
                        <div class="card-footer">
                </div>
      </div>
      </div>
      </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
        $(document).ready(function(e){
            var base_url = "<?php echo base_url();?>";
            $('#datatable').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :"<?= site_url("AdminSuministros/CargarInventarios") ?>",//  base_url+'Index.php/AdminSuministros/CargarInventarios',
                        dataSrc:"",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "Desc_Categorias" },
                        { "data": "DescripcionInv" },
                        { "data": "Cantidad" },
                        { "data": "costo" },
                        { "data": "FechaIngreso" }

                                  ]
            });
          });
        </script>

  <!-- / tabla tipo Inventario-->


<script>


$("#addInventario").on("click",function(){
  $('#modelInventario').modal("show")
})


function renderDeleteInventario(row)
{
return `<div class="row">
<div class="col-2">
  <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/sumInventario")?>/${row.id}"><i class="fa fa-plus"></i></a>
</div>
<div class="col-2">
  <a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditInventario")?>/${row.id}"><i class="fa fa-edit"></i></a>
</div>
<div class="col-2">
  <a class='btn btn-danger' href='<?= site_url("AdminSuministros/BorrarInventario")?>?id=${row.id}'><i class='fa fa-trash'></i></a>
</div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}
$(document).ready(function(e){


    $('#Inventario').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/CargarInventarios")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Categorias" },
                { "data": "DescripcionInv" },
                { "data": "Cantidad", "render":convertNumbers },
                { "data": "costo", "render":convertNumbers },
                { "data": "FechaIngreso", "render":fld },
                { "data": "Desc_Preferencial" },
                { "data": "cantidadminima" },
                { "data": "Desc_Zona" },
                { "data": "Desc_Region" },
                {"data": renderDeleteInventario}
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

  }); // End Document Ready Function

</script>
