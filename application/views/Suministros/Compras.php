
<div class="card">
<div class="card-header">
  <h2><?= $module_name ?></h2>
                    </div>

  <div class="card-body">
    <table class="table" id="Compras">
        <thead>
          <tr>
            <th>ID</th>
            <th>Usuario</th>
            <th>Tipo de compra</th>
            <th>Proveedor</th>
            <th>Número de pedido</th>
            <th>Total</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

  </div>
  <div class="card-footer">
     <button class="btn btn-primary" id="addCompras" data-toggle="modal" data-target="#modelCompras"><i class="fa fa-plus"></i>&nbsp;Agregar Compras</button>
            </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modelCompras">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertCompra')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Usuario</label><br>
          <?php
          $usuario[""] = "Seleccione un usuario";
          foreach ($usuarios as $row)
          {
            $usuario[$row->id] = $row->first_name.' '.$row->last_name;
          }

          echo form_dropdown('user', $usuario, $this->session->userdata('user_id'), "class='select2 form-control' ");
           ?>
      </div>

      <div class="form-group">
          <label>Tipo de compra</label><br>
          <?php

          $tipo_compra_dd[""] = "Seleccione un tipo de compra";
          foreach ($tipo_compra['data'] as $row)
          {
            $tipo_compra_dd[$row->id] = $row->DescTipoCompra;
          }
          echo form_dropdown('TipoCompra', $tipo_compra_dd, "", "class='select2 form-control' ");
           ?>

      </div>

      <div class="form-group">
          <label>Proveedor</label><br>
          <?php

          $proveedor[""] = "Seleccione un proveedor";
          foreach ($proveedores["data"] as $row)
          {
            $proveedor[$row->id] = $row->nombre;
          }
          echo form_dropdown('Proveedor', $proveedor, "", "class='select2 form-control' ");
           ?>

      </div>

      <div class="form-group">
          <label>Número de Pedido</label><br>
          <input type='number' name='numero_pedido' placeholder="Número de Pedido" class="form-control" />
      </div>

      <div class="form-group">
          <label>Total</label><br>
          <input type='number' name='total' placeholder="Total" class="form-control" />
      </div>


      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>

<div class="card">
<div class="justify-content-center">
<div class="card-header">

                        <h2>Gastos por Proveedor</h2>
                    </div>
<canvas id="line-chart" width="800" height="450">


</canvas>
<script>
let chartData = [], label = "";
$(document).ready(function(){


    var xhttp;
      if (window.XMLHttpRequest) {

        xhttp = new XMLHttpRequest();
        } else {

        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          chartData = JSON.parse(this.responseText);

          new Chart(document.getElementById("line-chart"), {
      type: 'bar',
      data: {
        labels: chartData.labels,
        datasets: [{
            data: chartData.data,
            label: "Niveles de Inventarios en productos",
            backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
      ],
            fill: true
          }
        ]
      },

      options: {
        title: {
          display: true,
          text: 'Comportamiento de nivel de inventario'
        }
      }
    });
        }
      };
      xhttp.open("GET","<?= site_url("AdminSuministros/GetGastosVehiculo")?>" , true);
      xhttp.send("categoria="+$(this).val());


})

            </script>
                   <div class="card-footer">
                </div>
      </div>
      </div>
      </div>

<script type="text/javascript">

function renderActionsCompras(row)
{
return `<div class="row">
<a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditCompra")?>/${row.id}"><i class="fa fa-edit"></i></a>
<a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteCompra")?>?id=${row.id}'><i class='fa fa-trash'></i></a>
</div>`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

$(document).ready(function () {
  $('#Compras').DataTable(
    {
    "pageLength" : 10,
    "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
    "ajax":{
              url :  "<?= site_url("AdminSuministros/getCompras")?>",
              dataSrc:"data",
              type : 'GET'
            },
            "columns": [
              { "data": "id" },
              { "data": "username" },
              { "data": "tipocompra" },
              { "data": "proveedor" },
              { "data": "numeropedido" },
              { "data": "total", "render": convertNumbers },
              { "data": renderActionsCompras  }
            ],
            "language":{
              "infoEmpty": "No hay información disponible",
              "search": "Buscar",
              "paginate": {
                      "first":      "Primero",
                      "last":       "Último",
                      "next":       "Siguiente",
                      "previous":   "Anterior"
                  },
              "processing":     "Procesando...",
              "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            }
  });
})



</script>
