<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

    <link rel="stylesheet" href="<?= base_url("Estilos/css/BotonesMenu.css") ?>">
    <title> <?=$module_name?> </title>
  </head>

  <body>


    <form action= '<?=site_url('AdminSuministros/InsertCategorias')?>' method='post'>
      <div class="main-content">
        <div class="card">
          <div class="card-header">
                      <strong>Categorias</strong>
          </div>
            <div class="card-body card-block">
              <div class="form-group">
                <label><?= lang('category') ?></label><br>
                <input type='text' name='dt_categorias' id='category'> </input> <br>
                <input type="submit" value="Enviar">

              </div>
            <div>
       </div>
      </div>
    </form>


    <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tipo Categoria</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
      </table>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
        $(document).ready(function(e){
            var base_url = "<?php echo base_url();?>";
            $('#datatable').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :  base_url+'Index.php/AdminSuministros/CargarCategorias',
                        dataSrc:"",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "Desc_Categorias" }
                                  ]
            }); // End of DataTable
          }); // End Document Ready Function
        </script>

  </body>

</html>
