
<div class="card">
  <div class="card-header">
    <strong>Inventario Disponible</strong>
  </div>
  <div class="card-body">
    <table class="table" id="datatable">
      <thead>
          <tr>
            <th><?= lang('ID')?></th>
            <th><?= lang('category_type')?></th>
            <th><?= lang('item') ?></th>
            <th><?= lang('qty')?></th>
            <th><?= lang('unitary_price')?></th>
            <th><?= lang('last_date_ingress')?></th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="card-footer">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modaladdSolicitud"><?= lang('request_supplies')?></button>

  </div>
</div>


<div class="card">
  <div class="card-header">
    <strong><?= lang('requests')?></strong>
  </div>
  <div class="card-body">
    <table class="table" id="datatableSolicitudes">
      <thead>
          <tr>
            <th><?= lang('ID')?></th>
            <th><?= lang('date_request')?></th>
            <th><?= lang('justification')?></th>
            <th><?= lang('request_type')?></th>
            <th><?= lang('qty_requested')?></th>
            <th><?= lang('item')?></th>
            <th><?= lang('user') ?></th>
            <th><?= lang('status') ?></th>

          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>


<div class="modal fade" id="modaladdSolicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action= '<?=site_url('AdminSuministros/addSolicitud')?>' method='post'>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Item a Solicitar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">

          <label for="Item" class=" form-control-label"><?= lang('item')?></label><br>

                  <?php

                    foreach ($Item["data"] as $attr)
                    {
                      $attrItem[$attr->id] = $attr->DescripcionInv;
                    }

                    echo form_dropdown('Categoria',$attrItem, '1', 'class="form-control select2"');
                    ?>
        </div>
        <div class="form-group">
          <label for="input"><?= lang('qty')?></label>
          <input type='number' name='dt_cantidad' id='Cantidad' class="form-control" min="0" oninput="this.value = Math.abs(this.value)" />
        </div>

        <div class="form-group">
          <label for="Estado"><?= lang('prov_request_type')?></label>
          <?php

            foreach ($tipoProveeduria['data'] as $attr)
            {
              $attrTipo[$attr->id] = $attr->Desc_TipoSolicitud;
            }
            echo form_dropdown('TipoProveduria',$attrTipo, '1', 'class="form-control select2"');
            ?>
        </div>




<div class="form-group">
  <label for="justificacion"><?= lang('justification')?></label>
  <?php
  $data = array(
     'name'        => 'justificacion',
     'id'          => 'justificacion',
     'class' => 'form-control',

   );
  echo form_textarea($data) ?>
</div>
         <!--  <input type='text' name='dt_item' id='DescripcionInv'> </input> <br>  -->



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close')?></button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!--
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
-->  <script type="text/javascript">
        $(document).ready(function(e){
          $('#datatable').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :  "<?= site_url('AdminSuministros/CargarInventarios') ?>",
                        dataSrc:"data",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "Desc_Categorias" },
                        { "data": "DescripcionInv" },
                        { "data": "Cantidad" },
                        { "data": "costo", "render":convertNumbers },
                        { "data": "FechaIngreso", "render":fld }

                      ],
                      "language":{
                        "infoEmpty": "No hay información disponible",
                        "search": "Buscar",
                        "paginate": {
                                "first":      "Primero",
                                "last":       "Último",
                                "next":       "Siguiente",
                                "previous":   "Anterior"
                            },
                        "processing":     "Procesando...",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                      }
            });
          });
        </script>

 <script type="text/javascript">
        $(document).ready(function(e){
          $('#datatableSolicitudes').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :  "<?= site_url('AdminSuministros/CargarSolicitudes') ?>",
                        dataSrc:"data",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "FechaSolicitud" },
                        { "data": "Justificacion" },
                        { "data": "Desc_TipoSolicitud" },
                        { "data": "CantidadPedido"},
                        { "data": "DescripcionInv"},
                        { "data": "Cedula"},
                        { "data": "Descripcion"}
                      ],
                      "language":{
                        "infoEmpty": "No hay información disponible",
                        "search": "Buscar",
                        "paginate": {
                                "first":      "Primero",
                                "last":       "Último",
                                "next":       "Siguiente",
                                "previous":   "Anterior"
                            },
                        "processing":     "Procesando...",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                      }
            });
          });
        </script>
