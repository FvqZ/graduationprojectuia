<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

    <link rel="stylesheet" href="<?= base_url("Estilos/css/BotonesMenu.css") ?>">
    <title> <?=$module_name?> </title>
  </head>

  <body>


    <form action= '<?=site_url('AdminSuministros/InsertInventarios')?>' method='post'>

      <div class="main-content">
        <div class="card">
          <div class="card-header">
                      <strong>Inventario</strong>
          </div>
            <div class="card-body card-block">
              <div class="form-group">

                <label for="Categoria" class=" form-control-label">Categoria</label><br>
              <!-- test <input type='text' name='dt_idcategorias' id='Id_RefCategoria'> </input> <br> -->
                          <?php

                          foreach ($Categoria as $attr)
                          {
                            $attrCategoria[$attr->id] = $attr->Desc_Categorias;
                          }

                          echo form_dropdown('Categoria',$attrCategoria, '0', 'class="form-control select2"');
                          ?>

                <label>Item</label><br>
                <input type='text' name='dt_item' id='DescripcionInv'> </input> <br>
                <label>Cantidad</label><br>
                <input type='number' min="0" min="0" oninput="this.value = Math.abs(this.value)" name='dt_cantidad' id='Cantidad' /> <br>
                <label>Precio Unitario</label><br>
                <input type='text' name='dt_unitario' id='costo'> </input> <br>
                <label>Última Fecha de Ingreso</label><br>
                <input type='date' name='dt_fecha' id='FechaIngreso'> </input> <br>
                <input type="submit" value="Enviar">

              </div>
            <div>
       </div>
      </div>

    </form>


    <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tipo Categoria</th>
                            <th>Item</th>
                            <th>Cantidad</th>
                            <th>Precio Unitario</th>
                            <th>Ultima Fecha de Ingreso</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
      </table>

      <div class="line-chart js-line-chart">
      <br><br>

      <canvas id="line-chart" width="800" height="450">


      </canvas>
      <script>
      let chartData = [], label = "";
      $(document).ready(function(){


          var xhttp;
            if (window.XMLHttpRequest) {

              xhttp = new XMLHttpRequest();
              } else {

              xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                chartData = JSON.parse(this.responseText);
                new Chart(document.getElementById("line-chart"), {
            type: 'bar',
            data: {
              labels: chartData.labels,
              datasets: [{
                  data: chartData.data,
                  label: "Niveles de Inventarios en productos",
                  borderColor: "#3e95cd",
                  fill: false
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Comportamiento de nivel de inventario'
              }
            }
          });
              }
            };
            xhttp.open("GET","<?= site_url("AdminSuministros/GetInventarioPorMes")?>" , true);
            xhttp.send("categoria="+$(this).val());


      })

                  </script>
      </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
        $(document).ready(function(e){
            var base_url = "<?php echo base_url();?>";
            $('#datatable').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :"<?= site_url("AdminSuministros/CargarInventarios") ?>",//  base_url+'Index.php/AdminSuministros/CargarInventarios',
                        dataSrc:"",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "Desc_Categorias" },
                        { "data": "DescripcionInv" },
                        { "data": "Cantidad" },
                        { "data": "costo" },
                        { "data": "FechaIngreso" }

                      ],
                      "language":{
                        "infoEmpty": "No hay información disponible",
                        "search": "Buscar",
                        "paginate": {
                                "first":      "Primero",
                                "last":       "Último",
                                "next":       "Siguiente",
                                "previous":   "Anterior"
                            },
                        "processing":     "Procesando...",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                      }
            });
          });
        </script>

  </body>

</html>
