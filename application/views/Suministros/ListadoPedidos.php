

<div class="card">
  <div class="card-header">
    <strong>Solicitudes</strong>
  </div>
  <div class="card-body">
    <table class="table" id="datatableSolicitudes">
      <thead>
          <tr>
            <th>ID</th>
            <th>Fecha Solicitud</th>
            <th>Justificacion</th>
            <th>Tipo de Solicitud</th>
            <th>Cantidad de Pedido</th>
            <th>Usuario</th>
            <th>Estado</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>

<script>
function renderDeletePresupuesto(row)
  {
    return `<div class="row">
    <a class="btn btn-info ${row.Descripcion !== "Registrado" ? 'disabled': ''}" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditListado")?>/${row.id}"><i class="fa fa-edit"></i></a>
    </div>`;
  }


</script>



 <script type="text/javascript">
        $(document).ready(function(e){
          $('#datatableSolicitudes').DataTable(
              {
              "pageLength" : 10,
              "dom": "Bfrtip",
              "order": [[0, "asc" ]],
              "buttons": ['copy', 'excel', 'pdf'],
              "ajax":{
                        url :  "<?= site_url('AdminSuministros/CargarSolicitudes') ?>",
                        dataSrc:"data",
                        type : 'GET'
                      },
                      "columns": [
                        { "data": "id" },
                        { "data": "FechaSolicitud" },
                        { "data": "Justificacion" },
                        { "data": "Desc_TipoSolicitud" },
                        { "data": "CantidadPedido" },
                        { "data": "Cedula"},
                        { "data": "Descripcion"},
                        { "data": renderDeletePresupuesto}

                      ],
                      "language":{
                        "infoEmpty": "No hay información disponible",
                        "search": "Buscar",
                        "paginate": {
                                "first":      "Primero",
                                "last":       "Último",
                                "next":       "Siguiente",
                                "previous":   "Anterior"
                            },
                        "processing":     "Procesando...",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                      }
            });
          });
        </script>
