<h2><?= $module_name ?></h2>
<div class="card">
  <div class="card-body">
    <table class="table" id="Proveedor">
        <thead>
          <tr>
            <th>ID</th>
            <th>Proveedor</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>Email</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

  </div>
  <div class="card-footer">
           <button class="btn btn-primary" id="addProveedor" data-toggle="modal" data-target="#modelProveedor"><i class="fa fa-plus"></i>&nbsp;Agregar Proveedor</button>
            </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modelProveedor">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertProveedor')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Tipo de Proveedor</label><br>
          <?php
          $tipo_prov[""] = "Seleccione un tipo de proveedor";
          foreach ($TipoProveedor["data"] as $row)
          {
            $tipo_prov[$row->id] = $row->Desc_TipoProveedor;
          }
          echo form_dropdown('TipoProveedor', $tipo_prov, "", "class='select2 form-control' ");
           ?>
      </div>

      <div class="form-group">
          <label>Nombre</label><br>
          <input type='text' name='nombre' placeholder="Nombre" class="form-control" maxlength="150" />
      </div>

      <div class="form-group">
          <label>Contactos</label><br>
          <input type='text' name='contactos' placeholder="Contactos" class="form-control" maxlength="150" />
      </div>

      <div class="form-group">
          <label>Correo Electrónico</label><br>
          <input type='email' name='email' placeholder="Correo Electrónico" class="form-control" maxlength="150" />
      </div>


      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>


<script type="text/javascript">

function renderDeleteProveedor(row)
{
return `<div class="row">
<a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditProveedor")?>/${row.id}"><i class="fa fa-edit"></i></a>
<a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteProveedor")?>?id=${row.id}'><i class='fa fa-trash'></i></a>
</div>`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

$(document).ready(function () {
  $('#Proveedor').DataTable(
    {
    "pageLength" : 10,
    "dom": "Bfrtip",
    "order": [[0, "asc" ]],
    "buttons": ['copy', 'excel', 'pdf'],
    "ajax":{
              url :  "<?= site_url("AdminSuministros/getProveedores")?>",
              dataSrc:"data",
              type : 'GET'
            },
            "columns": [
              { "data": "id" },
              { "data": "tipo" },
              { "data": "nombre" },
              { "data": "contacto" },
              { "data": "email" },
              { "data": renderDeleteProveedor  }
            ],
            "language":{
              "infoEmpty": "No hay información disponible",
              "search": "Buscar",
              "paginate": {
                      "first":      "Primero",
                      "last":       "Último",
                      "next":       "Siguiente",
                      "previous":   "Anterior"
                  },
              "processing":     "Procesando...",
              "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            }
  });
})



</script>
