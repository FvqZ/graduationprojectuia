<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Editar Compra</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <form action= '<?=site_url('AdminSuministros/EditCompra/'.$id) ?>' method='post'>
    <div class="modal-body">
    <div class="form-group">
        <label>Usuario</label><br>
        <?php
        $usuario[""] = "Seleccione un usuario";
        foreach ($usuarios as $row)
        {
          $usuario[$row->id] = $row->first_name.' '.$row->last_name;
        }

        echo form_dropdown('edit_user', $usuario, $compra->users_id, "class='select2 form-control' ");
         ?>
    </div>

    <div class="form-group">
        <label>Tipo de compra</label><br>
        <?php

        $tipo_compra_dd[""] = "Seleccione un tipo de compra";
        foreach ($tipo_compra['data'] as $row)
        {
          $tipo_compra_dd[$row->id] = $row->DescTipoCompra;
        }
        echo form_dropdown('edit_TipoCompra', $tipo_compra_dd, $compra->idTipoCompra, "class='select2 form-control' ");
         ?>

    </div>

    <div class="form-group">
        <label>Proveedor</label><br>
        <?php

        $proveedor[""] = "Seleccione un proveedor";
        foreach ($proveedores["data"] as $row)
        {
          $proveedor[$row->id] = $row->nombre;
        }
        echo form_dropdown('edit_Proveedor', $proveedor, $compra->Proveedores_idProveedores, "class='select2 form-control' ");
         ?>

    </div>

    <div class="form-group">
        <label>Número de Pedido</label><br>
        <input type='number' name='edit_numero_pedido' placeholder="Número de Pedido" class="form-control" value="<?= $compra->NumeroPedido ?>" />
    </div>

    <div class="form-group">
        <label>Total</label><br>
        <input type='number' name='edit_total' placeholder="Total" class="form-control" value="<?= $compra->Total ?>" />
    </div>


    </div>
    <div class="modal-footer">

      <input type="submit" class="btn btn-primary" value="Guardar"/>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </form>
  </div>
</div>
