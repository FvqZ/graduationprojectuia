
<div class="card">
  <div class="card-header">
    <h2><?= $module_name ?></h2>
  </div>

  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

    <li class="nav-item">
      <a class="nav-link active" id="pills-Categoria-tab" data-toggle="pill" href="#pills-Categoria" role="tab" aria-controls="pills-Categoria" aria-selected="true"><?= lang('category')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-Estado-tab" data-toggle="pill" href="#pills-Estado" role="tab" aria-controls="pills-Estado" aria-selected="false"><?= lang('status')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-Preferencial-tab" data-toggle="pill" href="#pills-Preferencial" role="tab" aria-controls="pills-Preferencial" aria-selected="false"><?= lang('preferential_catalog')?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-TipoCompra-tab" data-toggle="pill" href="#pills-TipoCompra" role="tab" aria-controls="pills-TipoCompra" aria-selected="false"><?= lang('purchase_type') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-TipoProveedor-tab" data-toggle="pill" href="#pills-TipoProveedor" role="tab" aria-controls="pills-TipoProveedor" aria-selected="false"><?= lang('provider_type') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-Zonas-tab" data-toggle="pill" href="#pills-Zonas" role="tab" aria-controls="pills-Zonas" aria-selected="false"><?= lang('zones') ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-Edificio-tab" data-toggle="pill" href="#pills-Edificio" role="tab" aria-controls="pills-Edificio" aria-selected="false"><?= lang('warehouse') ?></a>
    </li>

  </ul>

  <div class="tab-content" id="pills-tabContent">

    <!--  tabla de Categoria-->
      <div class="tab-pane fade show active" id="pills-Categoria" role="tabpanel" aria-labelledby="pills-Categoria-tab">
        <div class="card-body">
          <table class="table" id="Categoria">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?=lang('category') ?>/Tipo de Activo</th>
                 <th><?= lang('actions')?></th>
               </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
           <button class="btn btn-primary" id="addCategoria"><i class="fa fa-plus"></i>&nbsp;Agregar Categoria</button>
         </div>
        </div>
    <!-- /tabla de Categoria -->

    <!-- Estado -->
      <div class="tab-pane fade" id="pills-Estado" role="tabpanel" aria-labelledby="pills-Estado-tab">
        <div class="card-body">
          <table class="table" id="Estado" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('status')?></th>
                <th><?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
          </div>
          <div class="card-footer">
            <button class="btn btn-primary" id="addEstado"><i class="fa fa-plus"></i> &nbsp;Agregar Estado</button>
          </div>
        </div>
    <!-- /Estado-->

    <!-- Preferencial -->
      <div class="tab-pane fade" id="pills-Preferencial" role="tabpanel" aria-labelledby="pills-Preferencial-tab">
        <div class="card-body">
          <table class="table" id="Preferencial" style="width:100%;">
            <thead>
              <tr>
                <th><?= lang('ID')?></th>
                <th><?= lang('status')?></th>
                <th><?= lang('actions')?></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" id="addPreferencial"><i class="fa fa-plus"></i> &nbsp;Agregar Estado Preferencial</button>
        </div>
      </div>
  <!-- /Preferencial-->

  <!-- Tipo de compra -->
    <div class="tab-pane fade show" id="pills-TipoCompra" role="tabpanel" aria-labelledby="pills-TipoCompra-tab">
      <div class="card-body">
        <table class="table" id="TipoCompra" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('description')?></th>
              <th><?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addTipoCompra" data-toggle="modal" data-target="#modelTipoCompra"><i class="fa fa-plus"></i> &nbsp; Agregar Tipo de Compra</button>
      </div>
    </div>
  <!-- // tipo de compra -->

  <!-- Tipo de Proveedor -->
    <div class="tab-pane fade show" id="pills-TipoProveedor" role="tabpanel" aria-labelledby="pills-TipoProveedor-tab">
      <div class="card-body">
        <table class="table" id="TipoProveedor" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('description')?></th>
              <th><?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addTipoProveedor" data-toggle="modal" data-target="#modelTipoProveedor"><i class="fa fa-plus"></i> &nbsp;Agregar Tipo de Proveedor</button>
      </div>
    </div>
  <!-- // tipo de Proveedor -->

  <!-- Tipo de Zonas -->
    <div class="tab-pane fade show" id="pills-Zonas" role="tabpanel" aria-labelledby="pills-Zonas-tab">
      <div class="card-body">
        <table class="table" id="Zonas" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('description')?></th>
              <th><?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addZonas" data-toggle="modal" data-target="#modelZonas"><i class="fa fa-plus" ></i>&nbsp;Agregar Zonas</button>
      </div>
    </div>
  <!-- // tipo de Zonas -->

  <!-- Edificio -->
    <div class="tab-pane fade" id="pills-Edificio" role="tabpanel" aria-labelledby="pills-Edificio-tab">
      <div class="card-body">
        <table class="table" id="Edificio" style="width:100%;">
          <thead>
            <tr>
              <th><?= lang('ID')?></th>
              <th><?= lang('description') ?></th>
              <th><?= lang('actions')?></th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary" id="addEdificio"><i class="fa fa-plus"></i> &nbsp;Agregar Bodega</button>
      </div>
    </div>
  <!-- Edificio -->
  </div>
    </div>

  <!-- Modelo Categoria-->
  <div class="modal" tabindex="-1" role="dialog" id="modelCategoria">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Agregar Categoria</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form action= '<?=site_url('AdminSuministros/InsertCategorias')?>' method='post'>
        <div class="modal-body">
          <div class="form-group">
          <label><?= lang('category') ?></label><br>
            <input type='text' name='dt_Categoria' class="form-control" maxlength="45" /> <br>
          </div>
        </div>
        <div class="modal-footer">

          <input type="submit" class="btn btn-primary" value="Guardar"/>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
          </form>
      </div>
    </div>
  </div>
  <!-- / Modelo Categoria-->

<!-- Modelo tipo Estado-->
<div class="modal" tabindex="-1" role="dialog" id="modelEstado">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertEstadoSuministro')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Descripción Estado de Solicitud</label><br>
          <input type='text' name='dt_EstadoSolicitud' placeholder="Estado" class="form-control" maxlength="45" /> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo Estado-->

<!-- Modelo tipo Preferencial-->
<div class="modal" tabindex="-1" role="dialog" id="modelPreferencial">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Estado Preferencial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertPreferencial')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Estado Preferencial</label><br>
          <input type='text' name='dt_Preferencial' placeholder="Preferencial" class="form-control" maxlength="45"> </input> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo Preferencial-->

<!-- Modelo tipo TipoCompra-->
<div class="modal" tabindex="-1" role="dialog" id="modelTipoCompra">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Tipo de Compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertTipoCompra')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Tipo de Compra</label><br>
          <input type='text' name='dt_TipoCompra' placeholder="Tipo de Compra" class="form-control" maxlength="45" /> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo TipoCompra-->

<!-- Modelo tipo TipoProveedor-->
<div class="modal" tabindex="-1" role="dialog" id="modelTipoProveedor">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Estado Preferencial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertTipoProveedor')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Tipo de Proveedor</label><br>
          <input type='text' name='dt_TipoProveedor' placeholder="Tipo de Proveedor" class="form-control" maxlength="45" /> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo TipoProveedor-->

<!-- Modelo tipo Zonas-->
<div class="modal" tabindex="-1" role="dialog" id="modelZonas">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Zona</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertZona')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Zona</label><br>
          <input type='text' name='dt_Zona' placeholder="Zona" class="form-control" maxlength="45" /> <br>
      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo Zonas-->

<!-- Modelo tipo Edificio-->
<div class="modal" tabindex="-1" role="dialog" id="modelEdificio">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar Bodega</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action= '<?=site_url('AdminSuministros/InsertEdificio')?>' method='post'>
      <div class="modal-body">
      <div class="form-group">
          <label>Edificio</label><br>
          <input type='text' name='dt_Edificio' placeholder="Edificio" class="form-control" maxlength="45" /> <br>

      </div>
      </div>
      <div class="modal-footer">

        <input type="submit" class="btn btn-primary" value="Guardar"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- / Modelo tipo Estado-->





<script>

$("#addCategoria").on("click",function(){
  $('#modelCategoria').modal("show")
})

$("#addInventario").on("click",function(){
  $('#modelInventario').modal("show")
})

$("#addEstado").on("click",function(){
  $('#modelEstado').modal("show")
})

$("#addPreferencial").on("click",function(){
  $('#modelPreferencial').modal("show")
})

$("#addEdificio").on("click",function(){
  $('#modelEdificio').modal("show")
})




function renderDeleteCategoria(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditCategoria")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/BorrarCategorias")?>?id=${row.id}'><i class='fa fa-trash'></i></a></div>
</div>

`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

function renderDeleteInventario(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditInventario")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/BorrarInventario")?>?id=${row.id}'><i class='fa fa-trash'></i></a></div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

function renderDeleteEstado(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditEstadoSuministro")?>/${row.idEstadoSolicitud}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/BorrarEstadoSolicitud")?>?id=${row.idEstadoSolicitud}'><i class='fa fa-trash'></i></a></div>
</div>`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

function renderDeletePreferencial(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditCatalogoPreferencial")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/BorrarPreferencial")?>?id=${row.id}'><i class='fa fa-trash'></i></a></div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite
}

function renderDeleteTipoCompra(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditTipoCompra")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteTipoCompra")?>?id=${row.id}'><i class='fa fa-trash'></i></a></div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite

}


function renderDeleteEdificio(row)
{
return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditEdificio")?>/${row.idRegion}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteEdificio")?>?id=${row.idRegion}'><i class='fa fa-trash'></i></a></div>
</div>
`; //esta functionrendderiza el botond e acciones, se pueden agregar mas opciones dependiendo de lo que se necesite

}


function renderDeleteTipoProveedor(row)
{


return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditTipoProveedor")?>/${row.id}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteTipoProveedor")?>?id=${row.id}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

function renderDeleteTipoZonas(row)
{

return `<div class="row">
<div class="col-2"><a class="btn btn-info" data-toggle="ajax" href="<?= site_url("AdminSuministros/EditZona")?>/${row.idZona}"><i class="fa fa-edit"></i></a></div>
<div class="col-2"><a onclick="return confirm('¿Desea borrar este item?');" class='btn btn-danger' href='<?= site_url("AdminSuministros/deleteZonas")?>?id=${row.idZona}"'><i class='fa fa-trash'></i></a></div>
</div>`;
}

$(document).ready(function(e){


    $('#Categoria').DataTable(
      {
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
      "paging":true,
      "pageLength" : 10,
      "dom": 'Bfrtip',
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel'],
      "ajax":{
                url : "<?= site_url("AdminSuministros/CargarCategoriaSuministros")?>",// base_url+'Index.php/AdminCatalogosGaraje/CargarColores',
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Categorias" },
                { "data": renderDeleteCategoria }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable



    $('#Estado').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/CargarEstadoSuministros")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idEstadoSolicitud" },
                { "data": "Descripcion" },
                { "data": renderDeleteEstado  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable

    $('#Preferencial').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/CargarPreferencial")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_Preferencial" },
                { "data": renderDeletePreferencial  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    }); // End of DataTable


    $('#TipoCompra').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/getTipoCompras")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "DescTipoCompra" },
                { "data": renderDeleteTipoCompra  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

    $('#TipoProveedor').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/getTipoProveedor")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "id" },
                { "data": "Desc_TipoProveedor" },
                { "data": renderDeleteTipoProveedor  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

    $('#Zonas').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/getZonas")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idZona" },
                { "data": "Desc_Zona" },
                { "data": renderDeleteTipoZonas  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });

    $('#Edificio').DataTable(
      {
      "pageLength" : 10,
      "dom": "Bfrtip",
      "order": [[0, "asc" ]],
      "buttons": ['copy', 'excel', 'pdf'],
      "ajax":{
                url :  "<?= site_url("AdminSuministros/getEdificio")?>",
                dataSrc:"data",
                type : 'GET'
              },
              "columns": [
                { "data": "idRegion" },
                { "data": "Desc_Region" },
                { "data": renderDeleteEdificio  }
              ],
              "language":{
                "infoEmpty": "No hay información disponible",
                "search": "Buscar",
                "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                "processing":     "Procesando...",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              }
    });


  }); // End Document Ready Function

</script>
