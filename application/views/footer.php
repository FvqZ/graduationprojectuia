      </div>
    </div>
  </div>
</div>
</body>
<style>
footer {
    position: fixed;
    height: 100px;
    bottom: 0;
    width: 100%;
}
</style>
<div class="modal" data-easein="flipYIn" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true"></div>

<?php
  $m = strtolower($this->router->fetch_class());
  $v = strtolower($this->router->fetch_method());
  $tab = $this->session->flashdata("tab");
  unset($_SESSION['error'], $_SESSION['message']);
?>


<script type="text/javascript">

    $(document).ready(function(){
      <?php if($m == 'home'): ?>
        $('.mm_<?= $m; ?>').parent('li').addClass('current');
      <?php  else:  ?>
        $('.mm_<?= $m; ?>').parent('li').addClass('current');
        $('.mm_<?= $m; ?>').click();

        $('.mm_<?= $m; ?>').parent('li').addClass('active')
        $('#<?= $m; ?>_<?= $v; ?>').addClass('active');
        $('#<?= $m; ?>_<?= $v; ?>_mobile').addClass('active');
      <?php endif; ?>



      <?php if (!is_null($tab)): ?>
          $('#pills-<?= $tab ?>-tab').click();
      <?php endif; ?>

    });



</script>


  <footer fixed-bottom class="bg-dark text-center text-white">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!--===============================================================================================-->
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/bootstrap/js/popper.js"></script>
<script src="<?= $assets ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/tilt/tilt.jquery.min.js"></script>
<script type="text/javascript" src="<?=$assets ?>js/datatables.min.js"></script>

<script src="<?= $assets ?>vendor/animsition/animsition.min.js"></script>
<script src="<?= $assets ?>js/main.js"></script>


<div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Copyright: Francisco Vazquez
  </div>


  </footer>
