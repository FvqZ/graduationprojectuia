<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUsuarios extends MY_Controller {

	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
		$this->load->library('form_validation');
	  $this->load->model("Usuarios_Model");  /* This used to load model */
		$this->load->model("Ion_auth_model");
		$this->load->library('Ion_auth'); //invocar libreria de auth
		$this->load->library('form_validation');
		if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
		{
			redirect('auth/login', 'refresh');
		}
	}

	public function index($tab = null)
	{
		$this->data['module_name']='Administración de Módulos en Usuarios';
		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

		$this->data["tab"] = $tab; //info del tab para que se despliegue automaticamente
		$this->page_construct('Usuarios/CatalogosUsuarios', $this->data);

	}

	public function ListadoUsuarios()
	{
		$this->data['module_name']='Listado de Usuarios';
		$this->data["TipoCedulaSelect"] =$this->Usuarios_Model->getTipoCedulaSelect();
		$this->data["TipoUsuarioSelect"] =$this->Usuarios_Model->getTipoUsuarioSelect();
		$this->data["DepartamentoSelect"] =$this->Usuarios_Model->getDepartamentoSelect();
		$this->page_construct('Usuarios/ListadoUsuarios', $this->data);

	}
//Inserts
    public function InsertTipoCedula()
	{
		$this->form_validation->set_rules('dt_tipocedula', 'Tipo de Cédula', 'trim|required');

		if ($this->form_validation->run())
		{
			$data = array(
				'Description' => $this->input->post('dt_tipocedula')
			);
			if($this->Usuarios_Model->InsertTipoCedula($data))
			{
					$this->session->set_flashdata('message', 'Tipo de cédula agregado correctamente');
			} else {
				$this->session->set_flashdata('error', 'Error al agregar el tipo de cédula');
			}

		} else {
			$this->session->set_flashdata('error', validation_errors());
		}
		redirect('AdminUsuarios');
	}

    public function InsertTipoUsuario()
	{
		$this->form_validation->set_rules('dt_tipousuario', 'tipo de usuario', 'trim|required' );
		$this->form_validation->set_rules('dt_descrol', 'descripción del rol', 'trim|required' );
		$this->session->set_flashdata('tab', 'tipousuario');
		if ($this->form_validation->run())
		{
			$data = array(
				'name' => $this->input->post('dt_tipousuario'),
							'description' => $this->input->post('dt_descrol')
			);
			if($this->Usuarios_Model->InsertTipoUsuario($data))
			{
					$this->session->set_flashdata('message', 'Tipo de usuario agregado correctamente');
			} else {
				$this->session->set_flashdata('error', 'Error al agregar el tipo de usuario');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors());
		}

		redirect('AdminUsuarios');
	}

    public function InsertDepartamento()
	{
		$this->form_validation->set_rules('dt_departamento', 'Departamento', 'trim|required');
		$this->session->set_flashdata('tab', 'departamento');
		if ($this->form_validation->run())
		{
			$data = array(
				'Desc_Departamento' => $this->input->post('dt_departamento')
			);
			if($this->Usuarios_Model->InsertDepartamento($data))
			{
				$this->session->set_flashdata('message', 'Departamento agregado correctamente');
			} else {
				$this->session->set_flashdata('error', 'Error al agregar el departamento');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors());
		}

		redirect('AdminUsuarios');
	}

	public function InsertUsuarios()
	{

		$this->form_validation->set_rules('dt_cedula', 'Identificación', 'trim|min_length[9]|max_length[20]|required');
		$this->form_validation->set_rules('dt_correo', 'Correo Electrónico', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|min_length[8]|required');
		$this->form_validation->set_rules('dt_nombre', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('dt_apellidos', 'Apellidos', 'trim|required');
		$this->form_validation->set_rules('dt_phone', 'Teléfono', 'trim|max_length[15]|required');
		$this->form_validation->set_rules('TipoCedulaSelect', 'Tipo de Identificación', 'trim|required');
		$this->form_validation->set_rules('TipoUsuarioSelect', 'Tipo de Usuario', 'trim|required');
		$this->form_validation->set_rules('DepartamentoSelect', 'Departamento', 'trim|required');

		if ($this->form_validation->run())
		{
			$data = array(
				'Cedula' => $this->input->post('dt_cedula'),
				'username' => $this->input->post('dt_correo'),
				'email' => $this->input->post('dt_correo'),
				'active' =>1,
				'password' =>$this->Ion_auth_model->hash_password($this->input->post('password')),
				'first_name' => $this->input->post('dt_nombre'),
				'last_name' => $this->input->post('dt_apellidos'),
				'phone' => $this->input->post('dt_phone'),
				'tipocedula_idTipoCedula1' => $this->input->post('TipoCedulaSelect'),
				'groups_id' => $this->input->post('TipoUsuarioSelect'),
				'departamento_id' => $this->input->post('DepartamentoSelect')
			);
			$InsertUsuarios=$this->Usuarios_Model->InsertUsuarios($data);
			$this->session->set_flashdata('message', 'Usuario agregado correctamente');
			redirect('AdminUsuarios/ListadoUsuarios');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('AdminUsuarios/ListadoUsuarios');
		}


	}


    /*********************************funciones de borrado******************************************/
public function BorrarTipoCedula($id = null)
{
	if ($this->input->get('id')) { $id = $this->input->get("id");	}

	if ($this->Usuarios_Model->BorrarTipoCedula($id))
	{
		$this->session->set_flashdata('message', 'Tipo de cédula correctamente');
	}else {
		$this->session->set_flashdata('error', 'Error al borrar tipo de cédula');
	}
		redirect("AdminUsuarios");
}

public function BorrarTipoUsuarios($id = null)
{
	if ($this->input->get('id')) { $id = $this->input->get("id"); }
		$this->session->set_flashdata('tab', 'tipousuario');
		if ($this->Usuarios_Model->BorrarTipoUsuarios($id))
		{
			$this->session->set_flashdata('message', 'Tipo de usuario eliminado correctamente');
		}
		else
		{
			$this->session->set_flashdata('error', 'Error al eliminar tipo de usuario');
		}
		redirect("AdminUsuarios");

}

public function BorrarDepartamento($id = null)
{
	if ($this->input->get("id")) { $id = $this->input->get("id"); }
			$this->session->set_flashdata('tab', 'departamento');
			if ($this->Usuarios_Model->BorrarDepartamento($id))
			{
				$this->session->set_flashdata('message', 'Departamento eliminado correctamente');
			} else {
				$this->session->set_flashdata('error', 'Error al eliminar departamento');
			}
		redirect("AdminUsuarios");
}

public function borrarUser($id=NULL)
{
	if ($this->input->get('id')) { $id = $this->input->get('id');	}

	if ($this->Usuarios_Model->borrarUsuario($id))
	{
		$this->session->set_flashdata('message', 'Usuario eliminado correctamente');
		redirect('AdminUsuarios/ListadoUsuarios');
	}
	$this->session->set_flashdata('error', 'Error al eliminar el usuario');
	redirect('AdminUsuarios/ListadoUsuarios');
}

//Selects//

public function CargarTipoCedula()
{
    $TipoCedula = $this->Usuarios_Model->getTipoCedula();

        echo json_encode($TipoCedula);
}

public function CargarTipoUsuarios()
{
    $TipoUsuarios = $this->Usuarios_Model->getTipoUsuarios();

        echo json_encode($TipoUsuarios);
}

public function CargarDepartamento()
{
    $Departamento = $this->Usuarios_Model->getDepartamento();

        echo json_encode($Departamento);
}

public function CargarUsuarios()
{
    $Usuarios = $this->Usuarios_Model->getUsuarios();

        echo json_encode($Usuarios);
}


// Updates

//TipoCedula

public function EditTipoCedula($id = NULL)
{
	$this->form_validation->set_rules('dt_tipocedula', 'tipocedula', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}
	$this->session->set_flashdata('tab', 'tipocedula');
	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Description' => $this->input->post('dt_tipocedula'),
		);

		if ($this->Usuarios_Model->EditTipoCedula($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de Cedula editado correctamente');

			redirect('AdminUsuarios');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el tipo de cedula');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'tipocedula');
		redirect('AdminUsuarios');
	}else{

		$this->data["tipocedula"] = $this->Usuarios_Model->getTipoCedulabyId($id);
		$this->data["id"] = $id;
			$this->load->view('Usuarios/EditCatalogos/editTipoCedula', $this->data);
		}
	}
}

//Tipo Usuario

public function EditTipoUsuario($id = NULL)
{
	$this->form_validation->set_rules('dt_tipousuario', 'tipousuario', 'trim|required');
	$this->form_validation->set_rules('dt_descrol', 'tipousuario', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'name' => $this->input->post('dt_tipousuario'),
			'description' => $this->input->post('dt_descrol'),

		);


		if ($this->Usuarios_Model->EditTipoUsuario($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de usuario editado correctamente');
			$this->session->set_flashdata('tab', 'tipousuario');
			redirect('AdminUsuarios');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el tipo de usuario');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'tipousuario');
		redirect('AdminUsuarios');
	}else{

		$this->data["tipousuario"] = $this->Usuarios_Model->getTipoUsuariobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Usuarios/EditCatalogos/editTipoUsuario', $this->data);
		}
	}
}

//Departamento

public function EditDepartamento($id = NULL)
{
	$this->form_validation->set_rules('dt_departamento', 'departamento', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Departamento' => $this->input->post('dt_departamento'),

		);


		if ($this->Usuarios_Model->EditDepartamento($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Departamento editado correctamente');
			$this->session->set_flashdata('tab', 'departamento');
			redirect('AdminUsuarios');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el tipo de usuario');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'departamento');
		redirect('AdminUsuarios');
	}else{

		$this->data["departamento"] = $this->Usuarios_Model->getDepartamentobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Usuarios/EditCatalogos/editDepartamentos', $this->data);
		}
	}
}

public function roles()
{
	$this->data['module_name']= 'Roles';
	$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
	$this->page_construct('Usuarios/Roles', $this->data);
}

public function getRoles()
{
	$q = $this->Usuarios_Model->getRoles();
	echo json_encode($q);
}

public function EditRole($id = NULL)
{
	$this->form_validation->set_rules('dt_name', 'Nombre', 'trim|required');
	$this->form_validation->set_rules('dt_description', 'Description', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'name' => $this->input->post('dt_name'),
			'description' => $this->input->post('dt_description')
		);


		if ($this->Usuarios_Model->EditRole($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Rol editado correctamente');
			redirect('AdminUsuarios/roles');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el rol');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminUsuarios/roles');
	}else{

		$this->data["rol"] = $this->Usuarios_Model->getRolebyId($id);
		$this->data["id"] = $id;
		$this->load->view('Usuarios/EditCatalogos/editRole', $this->data);
		}
	}
}


public function DeleteRole($id = null)
{
		if($this->input->get('id')){$id = $this->input->get("id");}

		if ($this->Usuarios_Model->deleteRole($id))
		{
			$this->session->set_flashdata('message', 'Rol eliminado correctamente');
			redirect('AdminUsuarios/roles');
		}

		$this->session->set_flashdata('error', 'Error al eliminar el rol correctamente');
		redirect('AdminUsuarios/roles');
}

public function grupousuarios()
{
	$this->data['module_name']= "Grupo de usuarios";
	$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
	$this->page_construct('Usuarios/grupousuarios', $this->data);
}

// Grupo usuarios
public function getusuarioRoles()
{
	$q = $this->Usuarios_Model->getusuarioRoles();
	echo json_encode($q);
}

public function editUser($id = NULL)
{

	if($this->input->get("id")){ $id = $this->input->get('id');}

	$this->form_validation->set_rules('dt_cedula', 'Identificación', 'trim|min_length[9]|max_length[20]|required');
	$this->form_validation->set_rules('dt_correo', 'Correo Electrónico', 'trim|required|valid_email');
	if ($this->input->post('password') && $this->input->post('password') != "")
	{
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|min_length[8]|required');
	}
	$this->form_validation->set_rules('dt_nombre', 'Nombre', 'trim|required');
	$this->form_validation->set_rules('dt_apellidos', 'Apellidos', 'trim|required');
	$this->form_validation->set_rules('dt_phone', 'Teléfono', 'trim|max_length[15]|required');
	$this->form_validation->set_rules('TipoCedulaSelect', 'Tipo de Identificación', 'trim|required');
	$this->form_validation->set_rules('TipoUsuarioSelect', 'Tipo de Usuario', 'trim|required');
	$this->form_validation->set_rules('DepartamentoSelect', 'Departamento', 'trim|required');


	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Cedula' => $this->input->post('dt_cedula'),
			'username' => $this->input->post('dt_correo'),
			'email' => $this->input->post('dt_correo'),
			'active' =>1,
			'password' =>$this->Ion_auth_model->hash_password($this->input->post('password')),
			'first_name' => $this->input->post('dt_nombre'),
			'last_name' => $this->input->post('dt_apellidos'),
			'phone' => $this->input->post('dt_phone'),
			'tipocedula_idTipoCedula1' => $this->input->post('TipoCedulaSelect'),
			'groups_id' => $this->input->post('TipoUsuarioSelect'),
			'departamento_id' => $this->input->post('DepartamentoSelect')
		);
		if (!$this->input->post('password') || $this->input->post('password') == "")
		{
			unset($dataUpdate['password']);
		}

		if ($this->Usuarios_Model->editUser($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Usuario editado correctamente');
			redirect('AdminUsuarios/ListadoUsuarios');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el usuario');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminUsuarios/ListadoUsuarios');
	}else{

		$this->data["TipoCedulaSelect"] =$this->Usuarios_Model->getTipoCedulaSelect();
		$this->data["TipoUsuarioSelect"] =$this->Usuarios_Model->getTipoUsuarioSelect();
		$this->data["DepartamentoSelect"] =$this->Usuarios_Model->getDepartamentoSelect();

		$this->data["usuario"] = $this->Usuarios_Model->getUsuarioById($id);
		$this->data["id"] = $id;
		$this->load->view('Usuarios/EditCatalogos/editUser', $this->data);
		}
	}
}


}
