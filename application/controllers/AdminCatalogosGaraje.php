<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminCatalogosGaraje extends MY_Controller {

	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
	  $this->load->model("Garaje_Model");  /* This used to load model */
	  $this->load->library('form_validation');
	  $this->load->library('Util');


		$this->load->library('Ion_auth'); //invocar libreria de auth
		$this->load->library('form_validation');
		if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
		{
			redirect('auth/login', 'refresh');
		}
	}

	public function index($tab = null)
	{
		$this->data['module_name']='Administración de Módulos en Garaje';
		$this->data["Modelo"] =$this->Garaje_Model->getModelo();
		$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["Poliza"] =$this->Garaje_Model->getListadoPoliza();
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculoTable2();

		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

		$this->data["tab"] = $tab; //info del tab para que se despliegue automaticamente
		$this->page_construct('Garaje/CatalogosGaraje', $this->data);

	}

	public function AgregarVehiculo()
	{
		$this->data['module_name']='Agregar Vehículo';
		$this->data["Modelo"] =$this->Garaje_Model->getModelo();
		$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->page_construct('Garaje/AgregarVehiculo', $this->data);

	}

	public function RegistrarAccidente()
	{
		$this->data['module_name']='Registrar Accidentes';
		$this->data["Modelo"] =$this->Garaje_Model->getModelo();
		$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["TipoAccidente"] =$this->Garaje_Model->getAccidente();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->page_construct('Garaje/RegistrarAccidente', $this->data);

	}

	public function SolicitarVehiculo()
	{
		$this->data['module_name']='Solicitar un Vehículo';
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["FuncionarioSeleccionado"] =$this->Garaje_Model->getFuncionarioSeleccionado2();
		$this->page_construct('Garaje/SolicitarVehiculo', $this->data);

	}

	public function ListadoSolicitudes()
	{

		$this->load->library('email');


		$this->data['module_name']='Solicitar un Vehículo';
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["FuncionarioSeleccionado"] =$this->Garaje_Model->getFuncionarioSeleccionado2();
		$this->data["EstadoSolicitud"] =$this->Garaje_Model->getEstadoSolicitudLista();
		$this->page_construct('Garaje/ListadoSolicitudes', $this->data);

	}

	/////////Reparaciones//////////////////

	public function RegistrarReparaccion()
	{
		$this->data['module_name']='Registrar Reparación';
		$this->data["Modelo"] =$this->Garaje_Model->getModelo();
		$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["TipoAccidente"] =$this->Garaje_Model->getAccidente();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["AccidenteReparado"] =$this->Garaje_Model->getCostoReparacion();
		$this->data["AccidenteReparado2"] =$this->Garaje_Model->getCostoReparacion2();

		$this->page_construct('Garaje/RegistrarReparaccion', $this->data);

	}

	public function RegistrarMantenimiento()
	{
		$this->data['module_name']='Registrar Mantenimiento';
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["TipoRevision2"] =$this->Garaje_Model->getTipoRevision2();
		$this->page_construct('Garaje/RegistrarMantenimiento', $this->data);

	}

	public function ListadoViajes()
	{
		$this->data['module_name']='Listado de Viajes';
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["SolicitudVehiculoSeleccionada"] =$this->Garaje_Model->getSolicitudVehiculoSeleccionada();
		$this->data["ChoferAsignado"] =$this->Garaje_Model->getChoferAsignado();
		$this->page_construct('Garaje/ListadoViajes', $this->data);

	}

	public function ListadoFacturas()
	{
		$this->data['module_name']='Listado de Facturas';
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["SolicitudVehiculoSeleccionada"] =$this->Garaje_Model->getSolicitudVehiculoSeleccionada();
		$this->data["ChoferAsignado"] =$this->Garaje_Model->getChoferAsignado();
		$this->page_construct('Garaje/ListadoFacturas', $this->data);

	}

	public function ReportarFacturas()
	{
		$this->data['module_name']='Listado de Viajes';
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo2();
		$this->data["VehiculoSeleccionado"] =$this->Garaje_Model->getTipoVehiculo();
		$this->data["SolicitudVehiculoSeleccionada"] =$this->Garaje_Model->getSolicitudVehiculoSeleccionada();
		$this->data["ListadoViajes"] =$this->Garaje_Model->getListadoViajes();
		$this->data["ChoferAsignado"] =$this->Garaje_Model->getChoferAsignado();
		$this->page_construct('Garaje/ReportarFacturas', $this->data);

	}

	public function AgregarChoferes()
	{
		$this->data['module_name']='Registrar Choferes';
		$this->data["DisponibilidadChoferes2"] =$this->Garaje_Model->getDisponibilidadChoferes2();
		$this->data["TipoChofer"] =$this->Garaje_Model->getTipoChofer2();
		$this->data["FuncionarioSeleccionado"] =$this->Garaje_Model->getFuncionarioSeleccionado2();
		$this->page_construct('Garaje/Choferes', $this->data);

	}




    public function Agregar()
	{
		$this->data['module_name']='Garaje/Administracion de Módulos en Garaje';
		$this->page_construct('Garaje/AgregarGaraje', $this->data);

	}

    public function AgregarColor()
	{

		$this->data['module_name']='Garaje/Agregar Color';
		$this->page_construct('Garaje/AgregarColor', $this->data);

	}


    public function AgregarTipoVehiculo()
	{
		$this->data['module_name']='Garaje/Agregar Tipo';
	//	$this->page_construct('Garaje/CatalogosGaraje', $this->data);
	redirect('AdminCatalogosGaraje');

	}

	public function AgregarTipoReparacion()
	{
		$this->data['module_name']='Garaje/Agregar Tipo Reparacion';
		$this->page_construct('Garaje/AgregarTipoReparacion', $this->data);

	}



	public function AgregarTipoEstadoVehiculo()
	{
		$this->data['module_name']='Garaje/Agregar Estado';

	redirect('AdminCatalogosGaraje');

	}

	public function AgregarModelo()
	{
		$this->data['module_name']='Garaje/Agregar Modelos';
		$this->page_construct('Garaje/AgregarModelo', $this->data);

	}

	public function CargarColores()
	{
		$Colores = $this->Garaje_Model->getColores();

			echo json_encode($Colores);
	}


	public function CargarTipoVehiculo()
	{

		$q = $this->Garaje_Model->getTipoVehiculoTable();

			echo json_encode($q);
	}


	public function CargarTipoEstadoVehiculo()
	{

	$q = $this->Garaje_Model->getTipoEstadoVehiculo();

			echo json_encode($q);
	}

	public function CargarEstadoSolVehiculo()
	{

	$q = $this->Garaje_Model->getEstadoSolVehiculo();

			echo json_encode($q);
	}

	public function CargarTipoRevision()
	{

	$q = $this->Garaje_Model->getTipoRevision();

			echo json_encode($q);
	}

	public function CargarTipoChofer()
	{

	$q = $this->Garaje_Model->getTipoChofer();

			echo json_encode($q);
	}

	public function CargarDisponibilidadChofer()
	{

	$q = $this->Garaje_Model->getDisponibilidadChofer();

			echo json_encode($q);
	}

	public function CargarPoliza()
	{

	$q = $this->Garaje_Model->getPoliza();

			echo json_encode($q);
	}


	public function CargarModelo()
	{
		$data = $this->Garaje_Model->getModelo();
			echo json_encode($data);
	}

	public function CargarTipoAccidente()
	{
		$data = $this->Garaje_Model->getAccidente();
			echo json_encode($data);
	}

	public function CargarTipoReparacion()
	{

		$data = $this->Garaje_Model->getTipoReparacion();

			echo json_encode($data);

	}

// Insert Color
	public function InsertColores()
{
	$this->form_validation->set_rules('dt_color', 'Color', 'trim|required');
	$this->session->set_flashdata('tab', 'color');
	if ($this->form_validation->run())
	{
		$data = array('Desc_Colores' => $this->input->post('dt_color'));
		if ($this->Garaje_Model->InsertColores($data))
		{
			$this->session->set_flashdata('message', 'Color agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar El color');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

//Insert Tipo Vehiculo

public function InsertTipoVehiculo()
{
	$this->form_validation->set_rules('dt_tipovehiculo', 'Tipo de Vehiculo', 'trim|required');
	$this->session->set_flashdata('tab', 'tipovehiculo');
	if ($this->form_validation->run())
	{
		$data = array('Desc_TipoVehiculo' => $this->input->post('dt_tipovehiculo'));
		if ($this->Garaje_Model->InsertTipoVehiculo($data))
		{
			$this->session->set_flashdata('message', 'Tipo de vehiculo agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar tipo de vehiculo');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

// Insert Modelo

public function InsertModelo()
{
	$this->form_validation->set_rules('dt_modelo', 'Modelo', 'trim|required');
	$this->session->set_flashdata('tab', 'modelo');
	if ($this->form_validation->run())
	{
		$data = array('Desc_Modelo' => $this->input->post('dt_modelo'));
		if ($this->Garaje_Model->InsertModelo($data))
		{
			$this->session->set_flashdata('message', 'Modelo agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el modelo');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}


//Insert Tipo Reparacion


public function InsertTipoReparacion()
{
	$this->form_validation->set_rules('dt_Reparacion', 'Tipo de Reparacion', 'trim|required');
	$this->session->set_flashdata('tab', 'reparacion');
	if ($this->form_validation->run())
	{
		$data = array('Desc_TipoReparacion' => $this->input->post('dt_Reparacion'));
		if ($this->Garaje_Model->InsertTipoReparacion($data))
		{
			$this->session->set_flashdata('message', 'Tipo de Reparacion agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el tipo de reparacion');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

//Insert Vehiculo

public function InsertVehiculo()
{


	$this->form_validation->set_rules('dt_PlacaVehiculo',lang('vehicle_plate'), 'required|trim');
	$this->form_validation->set_rules('dt_VIN', lang('VIN'), 'required|trim');
	$this->form_validation->set_rules('dt_anno', lang('year'), 'required|trim');
		$this->form_validation->set_rules('dt_disponibilidad', lang('availability'), 'required|trim');
		$this->form_validation->set_rules('dt_kilometraje', lang('km'), 'required|trim');
		$this->form_validation->set_rules('Modelo', lang('model'), 'required|trim');
		$this->form_validation->set_rules('Color', lang('color'), 'required|trim');
		$this->form_validation->set_rules('TipoVehiculo', lang('vehicle_type'), 'required|trim');
		$this->form_validation->set_rules('Poliza', lang('policy'), 'required|trim');
		$this->session->set_flashdata('tab', 'vehiculo');
		if ($this->form_validation->run())
		{
			$data = array(

				'PlacaVehiculo' => $this->input->post('dt_PlacaVehiculo'),
				'NumeroVIN' => $this->input->post('dt_VIN'),
				'Anno' => $this->input->post('dt_anno'),
				'Disponibilidad' => $this->input->post('dt_disponibilidad'),
				'Kilometraje' => $this->input->post('dt_kilometraje'),
				'Modelo_Id' => $this->input->post('Modelo'),
				'Color_Id' => $this->input->post('Color'),
				'TipoVehiculo_id' => $this->input->post('TipoVehiculo'),
				'Polizas_idPolizas' => $this->input->post('Poliza')

			);
			if($this->Garaje_Model->InsertVehiculo($data))
			{
				$this->session->set_flashdata('message', 'Vehículo agregado correctamente');
			} else {
					$this->session->set_flashdata('error', 'Error al agregar el vehículo');
			}

		} else {
			$this->session->set_flashdata('error', validation_errors());
		}




	redirect('AdminCatalogosGaraje');
}

//Insert Accidente

	public function InsertAccidente()
{
	$this->form_validation->set_rules('dt_Accidente', 'Accidente', 'trim|required');
	$this->session->set_flashdata('tab', 'accidente');
	if ($this->form_validation->run())
	{
		$data = array('Descripcion_Accidente' => $this->input->post('dt_Accidente'));
		if ($this->Garaje_Model->InsertAccidente($data))
		{
			$this->session->set_flashdata('message', 'Accidente agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el accidente');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

//Insert Estado Accidente


public function InsertTipoEstadoVehiculo()
{
	$this->form_validation->set_rules('dt_EstadoReparacion', 'Estado de Reparación', 'trim|required');

	$this->session->set_flashdata('tab', 'estado');
	if ($this->form_validation->run())
	{
		$data = array('Descripcion' => $this->input->post('dt_EstadoReparacion'));
		if ($this->Garaje_Model->InsertTipoEstadoVehiculo($data))
		{
			$this->session->set_flashdata('message', 'Estado de Reparación agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el estado de reparación');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

//Insert Estado Solicitud


	public function InsertTipoEstadoSolVehiculo()
{
	$this->form_validation->set_rules('dt_Estado', 'Estado', 'trim|required');

	$this->session->set_flashdata('tab', 'estadosolvehiculo');
	if ($this->form_validation->run())
	{
		$data = array('Desc_Estado' => $this->input->post('dt_Estado'));
		if ($this->Garaje_Model->InsertTipoEstadoSolVehiculo($data))
		{
			$this->session->set_flashdata('message', 'Estado de Solicitud agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el estado de solicitud');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje');
}

//Insert Tipo de Revision

	public function InsertTipoRevision()
	{
		$this->form_validation->set_rules('dt_Revision', 'Tipo de Revisión', 'trim|required');

		$this->session->set_flashdata('tab', 'tiporevision');
		if ($this->form_validation->run())
		{
			$data = array('Desc_TipoRevision' => $this->input->post('dt_Revision'));
			if ($this->Garaje_Model->InsertTipoRevision($data))
			{
				$this->session->set_flashdata('message', 'Tipo de Revisión agregado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar el Tipo de Revisión');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());

		}
		redirect('AdminCatalogosGaraje');
	}

//Insert Tipo de Chofer

	public function InsertTipoChofer()
	{
		$this->form_validation->set_rules('dt_TipoChofer', 'Tipo de Chofer', 'trim|required');

		$this->session->set_flashdata('tab', 'tipochofer');
		if ($this->form_validation->run())
		{
			$data = array('Desc_Chofer' => $this->input->post('dt_TipoChofer'));
			if ($this->Garaje_Model->InsertTipoChofer($data))
			{
				$this->session->set_flashdata('message', 'Tipo de Chofer agregado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar el Tipo de Chofer');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());

		}
		redirect('AdminCatalogosGaraje');
	}

//Insert Disponibilidad de Chofer

	public function InsertDisponibilidadChofer()
	{
		$this->form_validation->set_rules('dt_DisponibilidadChofer', 'Disponibilidad de Chofer', 'trim|required');

		$this->session->set_flashdata('tab', 'disponibilidadchofer');
		if ($this->form_validation->run())
		{
			$data = array('Desc_Disponibilidad' => $this->input->post('dt_DisponibilidadChofer'));
			if ($this->Garaje_Model->InsertDisponibilidadChofer($data))
			{
				$this->session->set_flashdata('message', 'Disponibilidad de Chofer agregado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar la Disponibilidad de Chofer');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());

		}
		redirect('AdminCatalogosGaraje');
	}

//Insert Poliza

	public function InsertPoliza()
	{
		$this->form_validation->set_rules('dt_Poliza', 'Póliza', 'trim|required');

		$this->session->set_flashdata('tab', 'poliza');
		if ($this->form_validation->run())
		{
			$data = array('DescPoliza' => $this->input->post('dt_Poliza'));
			if ($this->Garaje_Model->InsertPoliza($data))
			{
				$this->session->set_flashdata('message', 'Poliza agregada correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar la Poliza');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());

		}
		redirect('AdminCatalogosGaraje');
	}

//Insert Solicitud de Vehiculo


public function InsertSolicitudVehiculo()
{
	$this->form_validation->set_rules('dt_anno', 'Año', 'required|trim');
	$this->form_validation->set_rules('dt_Razon', 'Razon', 'required|trim');
	if ($this->form_validation->run())
	{
		$data = array(

			'vehiculos_id' => $this->input->post('VehiculoSeleccionado'),
			'users_id' => $this->input->post('FuncionarioSeleccionado'),
			'FechaSalida' => $this->input->post('dt_anno'),
			'Razon' => $this->input->post('dt_Razon'),
			'EstadoSolicitudVehiculo_id' => 1,

		);
		if($this->Garaje_Model->InsertSolicitudVehiculo($data))
		{
			$this->session->set_flashdata('message', 'Solicitud de Vehículo agregado correctamente');
		} else {
				$this->session->set_flashdata('error', 'Error al agregar la Solicitud de Vehículo');
		}

	} else {
		$this->session->set_flashdata('error', validation_errors());
	}

redirect('AdminCatalogosGaraje/SolicitarVehiculo');
}

//Insert Viaje

public function InsertRegistroViaje()
{
	$this->form_validation->set_rules('dt_presupuesto', 'Presupuesto', 'trim|required');


	if ($this->form_validation->run())
	{
		$data = array(
			'solicitudvehiculos_id' => $this->input->post('SolicitudVehiculoSeleccionada'),
			'Choferes_idChoferes' => $this->input->post('ChoferAsignado'),
			'PresupuestoAsignado' => $this->input->post('dt_presupuesto'),
		);

		if ($this->Garaje_Model->InsertRegistroViaje($data))
		{
			$this->session->set_flashdata('message', 'Viaje agregada correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Viaje');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/ListadoViajes');
}

//Insert Facturas

public function InsertFacturasViaje()
{
	$this->form_validation->set_rules('dt_facturas', 'Facturas', 'trim|required');
	$this->form_validation->set_rules('dt_total', 'Monto Total', 'trim|required');

	if ($this->form_validation->run())
	{
		$data = array(
			'Viaje_idViaje' => $this->input->post('ListadoViajes'),
			'Facturas' => $this->input->post('dt_facturas'),
			'TotalFactura' => $this->input->post('dt_total'),
		);

		if ($this->Garaje_Model->InsertFacturasViaje($data))
		{
			$this->session->set_flashdata('message', 'Facturas agregadas correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar las Facturas');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/ReportarFacturas');
}

//Insert Accidente

public function InsertAccidenteDesc()
{
	$this->form_validation->set_rules('dt_fecha', 'Fecha', 'trim|required');
	$this->form_validation->set_rules('dt_DescAccidente', 'Descripción del Accidente', 'trim|required');

	if ($this->form_validation->run())
	{
		$data = array(
			'fecha' => $this->input->post('dt_fecha'),
			'desc_Accidente' => $this->input->post('dt_DescAccidente'),
			'Id_Vehiculo' => $this->input->post('VehiculoSeleccionado'),
			'id_TipoAccidente' => $this->input->post('TipoAccidente'),
		);

		if ($this->Garaje_Model->InsertAccidenteDesc($data))
		{
			$this->session->set_flashdata('message', 'Accidente registrado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Accidente');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/RegistrarAccidente');
}

//Insert Registro Reparacion


public function InsertCostoReparacion()
{
	$this->form_validation->set_rules('dt_DescReparacion', 'Descripción de la Reparación', 'trim|required');
	$this->form_validation->set_rules('dt_fecha', 'Fecha', 'trim|required');
	$this->form_validation->set_rules('dt_CostoReparacion', 'Costo de la Reparación', 'trim|required');
	$this->form_validation->set_rules('dt_Factura', 'Factura de Reparacion', 'trim|required');

	if ($this->form_validation->run())
	{
		$data = array(
			'Desc_Reparacion' => $this->input->post('dt_DescReparacion'),
			'Fecha' => $this->input->post('dt_fecha'),
			'Costo' => $this->input->post('dt_CostoReparacion'),
			'Id_Vehiculo' => $this->input->post('VehiculoSeleccionado'),
			'Media' => $this->input->post('dt_Factura'),
			'accidentesreparados_idAccidentesReparados' => $this->input->post('AccidenteReparado2'),
		);

		if ($this->Garaje_Model->InsertCostoReparacion($data))
		{
			$this->session->set_flashdata('message', 'Reparación registrada correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar la Reparación');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/RegistrarReparaccion');
}

//Insert Mantenimiento

public function InsertRegistroMantenimiento()
{
	$this->form_validation->set_rules('dt_fecha', 'Fecha', 'trim|required');

	if ($this->form_validation->run())
	{
		$data = array(
			'fecha' => $this->input->post('dt_fecha'),
			'vehiculos_id' => $this->input->post('VehiculoSeleccionado'),
			'TipoRevision_idTipoRevision' => $this->input->post('TipoRevision2'),
		);

		if ($this->Garaje_Model->InsertRegistroMantenimiento($data))
		{
			$this->session->set_flashdata('message', 'Mantenimiento registrado correctamente');
			$this->util->send_email('fran.95.v@gmail.com', 'Revisiones Mecanicas', 'Se ha registrado una revisión para el  '.$data['fecha']);
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Mantenimiento');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/RegistrarMantenimiento');
}

//Insert Choferes


public function InsertChoferes()
{
	$this->form_validation->set_rules('FuncionarioSeleccionado', 'Fecha', 'trim|required');

	if ($this->form_validation->run())
	{
		$data = array(
			'users_id' => $this->input->post('FuncionarioSeleccionado'),
			'TipoChofer_idTipoChofer' => $this->input->post('TipoChofer'),
			'DisponibilidadChofer_idDisponibilidadChofer' => $this->input->post('DisponibilidadChoferes2'),
		);

		if ($this->Garaje_Model->InsertListadoChoferes($data))
		{
			$this->session->set_flashdata('message', 'Chofer registrado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Chofer');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosGaraje/AgregarChoferes');
}

//--

	public function CargarVehiculo()
	{
		$this->db->select('vehiculos.id, vehiculos.PlacaVehiculo, vehiculos.NumeroVIN, vehiculos.Anno, vehiculos.Disponibilidad, vehiculos.Kilometraje, modelo.Desc_Modelo, colores.Desc_Colores, tipovehiculo.Desc_TipoVehiculo, polizas.DescPoliza' );
		$this->db->from('vehiculos');
		$this->db->join('modelo', 'vehiculos.Modelo_Id = modelo.id');
		$this->db->join('polizas', 'vehiculos.Polizas_idPolizas = polizas.idPolizas');
		$this->db->join('colores', 'vehiculos.Color_Id = colores.id');
		$this->db->join('tipovehiculo', 'vehiculos.TipoVehiculo_id = tipovehiculo.id');

		$q = $this->db->get();
		$data=array();

		if ($q->num_rows())
		{
			foreach ($q->result() as $row)
				{
					$data[] = $row;
				}


		}

		$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
		"recordsFiltered" => $q->num_rows() );
		
		
					echo json_encode($retData);
	}


	public function CargarChoferes()
	{

		$this->db->select('choferes.idChoferes, users.Cedula, tipochofer.Desc_Chofer, disponibilidadchofer.Desc_Disponibilidad' );
		$this->db->from('choferes');
		$this->db->join('users', 'choferes.users_id = users.id');
		$this->db->join('tipochofer', 'choferes.TipoChofer_idTipoChofer = tipochofer.idTipoChofer');
		$this->db->join('disponibilidadchofer', 'choferes.DisponibilidadChofer_idDisponibilidadChofer = disponibilidadchofer.idDisponibilidadChofer');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}



	public function CargarAccidente()
	{

		$this->db->select('listadoaccidentes.id, listadoaccidentes.fecha, listadoaccidentes.desc_Accidente, vehiculos.PlacaVehiculo, tipoaccidente.Descripcion_Accidente' );
		$this->db->from('listadoaccidentes');
		$this->db->join('vehiculos', 'listadoaccidentes.Id_Vehiculo = vehiculos.id');
		$this->db->join('tipoaccidente', 'listadoaccidentes.id_TipoAccidente = tipoaccidente.id');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}


	public function CargarRevisiones()
	{

		$this->db->select('revisiones.idRevisiones, vehiculos.PlacaVehiculo, tiporevision.Desc_TipoRevision, revisiones.Fecha' );
		$this->db->from('revisiones');
		$this->db->join('vehiculos', 'revisiones.vehiculos_id = vehiculos.id');
		$this->db->join('tiporevision', 'revisiones.TipoRevision_idTipoRevision = tiporevision.idTipoRevision');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}

	public function CargarListadoViajes()
	{

		$this->db->select('viaje.idViaje, solicitudvehiculos.Razon, users.Cedula, viaje.PresupuestoAsignado' );
		$this->db->from('viaje');
		$this->db->join('solicitudvehiculos', 'viaje.solicitudvehiculos_id = solicitudvehiculos.id');
		$this->db->join('choferes', 'viaje.Choferes_idChoferes = choferes.idChoferes');
		$this->db->join('users', 'choferes.users_id = users.id');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}

	public function CargarFacturasViajes()
	{

		$this->db->select('controlviajes.idControlViajes, solicitudvehiculos.Razon, controlviajes.Facturas, controlviajes.TotalFactura' );
		$this->db->from('controlviajes');
		$this->db->join('viaje', 'controlviajes.Viaje_idViaje = viaje.idViaje');
		$this->db->join('solicitudvehiculos', 'viaje.solicitudvehiculos_id = solicitudvehiculos.id');

		$q = $this->db->get();
		$data = array();
		
		if ($q->num_rows())
		{
			foreach ($q->result() as $row)
				{
					$data[] = $row;
				}


		}

		$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
		"recordsFiltered" => $q->num_rows() );
		
		echo json_encode($retData);

	}



//Solicitud de Vehiculo

public function CargarSolicitudes()
{

	$this->db->select('solicitudvehiculos.id, vehiculos.PlacaVehiculo, users.Cedula, solicitudvehiculos.FechaSalida, solicitudvehiculos.Razon, estadosolicitudvehiculo.Desc_Estado' );
	$this->db->from('solicitudvehiculos');
	$this->db->join('vehiculos', 'solicitudvehiculos.vehiculos_id = vehiculos.id');
	$this->db->join('users', 'solicitudvehiculos.users_id = users.id');
	$this->db->join('estadosolicitudvehiculo', 'solicitudvehiculos.EstadoSolicitudVehiculo_id = estadosolicitudvehiculo.id');
$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}


///Reparacciones

public function CargarListadoReparacion()
{

	$this->db->select('accidentesreparados.idAccidentesReparados, accidentesreparados.fecha, listadoaccidentes.desc_Accidente, estadoreparacion.Descripcion, tiporeparacion.Desc_TipoReparacion' );
	$this->db->from('accidentesreparados');
	$this->db->join('listadoaccidentes', 'accidentesreparados.listadoaccidentes_id = listadoaccidentes.id');
	$this->db->join('estadoreparacion', 'accidentesreparados.EstadoReparacion_idEstadoReparacion = estadoreparacion.idEstadoReparacion');
	$this->db->join('tiporeparacion', 'accidentesreparados.tiporeparacion_idTipoReparacion = tiporeparacion.idTipoReparacion');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}

//End Reparaciones

public function CargarReparacion()
{
$data=array();

	$this->db->select('reparaciones.id, reparaciones.Desc_Reparacion, reparaciones.Fecha, reparaciones.Costo, vehiculos.PlacaVehiculo, reparaciones.Media, listadoaccidentes.desc_Accidente' );
	$this->db->from('reparaciones');
	$this->db->join('vehiculos', 'reparaciones.Id_Vehiculo = vehiculos.id');
	$this->db->join('accidentesreparados', 'reparaciones.accidentesreparados_idAccidentesReparados = accidentesreparados.idAccidentesReparados');
	$this->db->join('listadoaccidentes', 'accidentesreparados.listadoaccidentes_id = listadoaccidentes.id');

$q = $this->db->get();
$data=array();

if ($q->num_rows())
{
	foreach ($q->result() as $row)
		{
			$data[] = $row;
		}


}

$retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


			echo json_encode($retData);
}

//End Listado Reparaciones


	public function CargarModels()
	{

		$this->data['module_name']='Garaje/Vehiculo';
		$this->data["Modelo"] =$this->Garaje_Model->getModelo() ;
		$this->page_construct('Garaje/AgregarVehiculo', $this->data);

	}

	public function CargarColors()
	{

		$this->data['module_name']='Garaje/Vehiculo';
		$this->data["Color"] =$this->Garaje_Model->getColor() ;
		$this->page_construct('Garaje/AgregarVehiculo', $this->data);

	}

	public function CargarTipoVehiculos()
	{

		$this->data['module_name']='Garaje/Vehiculo';
		$this->data["TipoVehiculo"] =$this->Garaje_Model->getTipoVehiculo() ;
		$this->page_construct('Garaje/AgregarVehiculo', $this->data);

	}
/*********************************funciones de borrado******************************************/
public function BorrarColor($id = null)
{
	if ($this->input->get("id")) { $id = $this->input->get("id"); }

		if ($this->Garaje_Model->BorrarColor($id))
		{
			$this->session->set_flashdata('message', 'Color borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar el color');
		}

		redirect("AdminCatalogosGaraje");

}

public function BorrarTipoVehiculo($id = null)
{
	if ($this->input->get("id")) {	$id = $this->input->get("id"); }

	if ($this->Garaje_Model->BorrarTipoVehiculo($id))
	{
		$this->session->set_flashdata('message', 'Tipo de vehículo borrado correctamente');
	} else {
		$this->session->set_flashdata('error', 'No se pudo borrar el tipo de vehículo');
	}
		redirect("AdminCatalogosGaraje");
}

public function BorrarModelo($id = null)
{
	if ($this->input->get("id")) { $id = $this->input->get("id");	}
		if ($this->Garaje_Model->BorrarModelo($id))
		{
			$this->session->set_flashdata('message', 'Modelo borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar modelo');
		}
		redirect("AdminCatalogosGaraje");

}

public function BorrarReparacion($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}
		if ($this->Garaje_Model->BorrarReparacion($id))
		{
			$this->session->set_flashdata('message', 'Tipo de reparación borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar el tipo de reparación');
		}
		redirect("AdminCatalogosGaraje");

}

public function BorrarVehiculo($id = null)
{
	if ($this->input->get("id")) {	$id = $this->input->get("id");}
		if ($this->Garaje_Model->BorrarVehiculo($id))
		{
			$this->session->set_flashdata('message', 'Vehículo borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar el vehículo');
		}
		redirect("AdminCatalogosGaraje");

}

public function BorrarAccidente($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}

			if ($this->Garaje_Model->BorrarAccidente($id))
			{
				$this->session->set_flashdata('message', 'Accidente borrado correctamente');
			} else {
				$this->session->set_flashdata('error', 'No se pudo borrar el accidente');
			}
		redirect("AdminCatalogosGaraje");

}

public function BorrarEstadoVehiculo($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}
	if ($this->Garaje_Model->BorrarEstadoVehiculo($id))
	{
		$this->session->set_flashdata('message', 'Estado de vehículo borrado correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar el estado de vehículo');
	}
	redirect("AdminCatalogosGaraje");
}

public function BorrarEstadoSolVehiculo($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}
	if ($this->Garaje_Model->BorrarEstadoSolVehiculo($id))
	{
		$this->session->set_flashdata('message', 'Estado solicitud de vehículo borrado correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar el estado solicitud de vehículo');
	}

	redirect("AdminCatalogosGaraje");
}

public function BorrarTipoRevision($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}

	if ($this->Garaje_Model->BorrarTipoRevision($id))
	{
		$this->session->set_flashdata('message', 'Tipo de revisión borrado correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar el tipo de revisión');
	}

	redirect("AdminCatalogosGaraje");
}

public function BorrarTipoChofer($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}

	if ($this->Garaje_Model->BorrarTipoChofer($id))
	{
		$this->session->set_flashdata('message', 'Tipo de chofer borrado correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar el tipo de chofer');
	}

	redirect("AdminCatalogosGaraje");
}

public function BorrarDisponibilidadChofer($id = null)
{
	if ($this->input->get("id")) {$id = $this->input->get("id");}
	if ($this->Garaje_Model->BorrarDisponibilidadChofer($id))
	{
		$this->session->set_flashdata('message', 'Disponibilidad de chofer borrado correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar la disponibilidad del chofer');
	}

	redirect("AdminCatalogosGaraje");
}

public function BorrarPoliza($id = null)
{
	if ($this->input->get("id")) {	$id = $this->input->get("id"); }
	if ($this->Garaje_Model->BorrarPoliza($id))
	{
		$this->session->set_flashdata('message', 'Póliza borrada correctamente');
	}
	else
	{
		$this->session->set_flashdata('error', 'No se pudo borrar la póliza');
	}
	redirect("AdminCatalogosGaraje");
}
/********************************* //funciones de borrado******************************************/

///Grafico Vehiculo SuM

public function GetGastosVehiculo()
{
	//se puede hacer por categoria o nombre
	//$id = $this->input->get("categoria");
	//$name = $this->input->get("producto");

	$alldata = $this->Garaje_Model->getSumCostos();
	//$datapermonth = array(0,0,0,0,0,0,0,0,0,0,0,0);
	$labels = array();
	$data = array();
foreach ($alldata["data"] as $row)
{
array_push($labels, $row->PlacaVehiculo);
array_push($data, $row->Costo);
}



	echo json_encode(array("labels" => $labels, "data" => $data));



}

public function GetGastosViajes()
{


	$alldata = $this->Garaje_Model->getSumViajes();

	$labels = array();
	$data = array();
foreach ($alldata["data"] as $row)
{
array_push($labels, $row->Razon);
array_push($data, $row->TotalFactura);
}



	echo json_encode(array("labels" => $labels, "data" => $data));



}

//uPDATE

//Listado


public function EditListado($id = NULL)
{
	$this->form_validation->set_rules('edit_Estado', 'estado', 'trim|required');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'EstadoSolicitudVehiculo_id' => $this->input->post('edit_Estado'),



		);

		if ($this->Garaje_Model->EditListado($dataUpdate, $id))
		{
			if ($dataUpdate["EstadoSolicitudVehiculo_id"]==2){


				if($garajeemail= $this->Garaje_Model->getListadobyId($id))
				{
					$this->util->send_email('fran.95.v@gmail.com', 'Solicitud Aprobada', 'La solicitud de vehículo con id '.$garajeemail->id.' con fecha de salida '.$garajeemail->FechaSalida. ' ha sido aprobada');

				}
			}

			$this->session->set_flashdata('message', 'Listado editado correctamente');
			redirect('AdminCatalogosGaraje/ListadoSolicitudes');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Partida');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminCatalogosGaraje/ListadoSolicitudes');
	}else{
		$this->data["EstadoSolicitud"] = $this->Garaje_Model->getEstadoSolicitudLista($id);
		$this->data["estado"] = $this->Garaje_Model->getListadobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/EditarListado', $this->data);
		}
	}
}

//Color

public function EditColor($id = NULL)
{
	$this->form_validation->set_rules('dt_color', 'Color', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Colores' => $this->input->post('dt_color'),
		);

		if ($this->Garaje_Model->EditColor($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Color editado correctamente');
			$this->session->set_flashdata('tab', 'color');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Color');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'color');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["color"] = $this->Garaje_Model->getColorbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editColor', $this->data);
		}
	}
}

//Tipo Vehiculo

public function EditTipoVehiculo($id = NULL)
{
	$this->form_validation->set_rules('dt_tipovehiculo', 'TipoVehiculo', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_TipoVehiculo' => $this->input->post('dt_tipovehiculo'),
		);

		if ($this->Garaje_Model->EditTipoVehiculo($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo Vehiculo editado correctamente');
			$this->session->set_flashdata('tab', 'tipovehiculo');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Tipo Vehiculo');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'tipovehiculo');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["tipovehiculo"] = $this->Garaje_Model->getTipoVehiculobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editTipoVehiculo', $this->data);
		}
	}
}

//Modelo

public function EditModelo($id = NULL)
{
	$this->form_validation->set_rules('dt_modelo', 'Modelo', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Modelo' => $this->input->post('dt_modelo'),
		);

		if ($this->Garaje_Model->editModelo($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Modelo editado correctamente');
			$this->session->set_flashdata('tab', 'modelo');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Modelo');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'modelo');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["modelo"] = $this->Garaje_Model->getModelobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editModelo', $this->data);
		}
	}
}

//Tipo Reparacion

public function EditTipoReparacion($id = NULL)
{
	$this->form_validation->set_rules('dt_Reparacion', 'TipoReparacion', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_TipoReparacion' => $this->input->post('dt_Reparacion'),
		);

		if ($this->Garaje_Model->editTipoReparacion($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo Reparacion editado correctamente');
			$this->session->set_flashdata('tab', 'reparacion');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Tipo de Reparacion');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'reparacion');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["tiporeparacion"] = $this->Garaje_Model->getTipoReparacionbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editTipoReparacion', $this->data);
		}
	}
}

//Accidente

public function EditAccidente($id = NULL)
{
	$this->form_validation->set_rules('dt_Accidente', 'Accidente', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Descripcion_Accidente' => $this->input->post('dt_Accidente'),
		);

		if ($this->Garaje_Model->editAccidente($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Accidente editado correctamente');
			$this->session->set_flashdata('tab', 'accidente');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Estado');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'accidente');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["accidente"] = $this->Garaje_Model->getAccidentebyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editAccidente', $this->data);
		}
	}
}

//Estado Accidente

public function EditEstado($id = NULL)
{
	$this->form_validation->set_rules('dt_EstadoReparacion', 'Estado', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Descripcion' => $this->input->post('dt_EstadoReparacion'),
		);

		if ($this->Garaje_Model->editEstado($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Estado editado correctamente');
			$this->session->set_flashdata('tab', 'estado');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Estado');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'estado');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["estado"] = $this->Garaje_Model->getEstadobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editEstado', $this->data);
		}
	}
}

//Estado

public function EditEstadoSol($id = NULL)
{
	$this->form_validation->set_rules('dt_Estado', 'Estado', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Estado' => $this->input->post('dt_Estado'),
		);

		if ($this->Garaje_Model->editEstadoSol($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Estado editado correctamente');
			$this->session->set_flashdata('tab', 'estadosolvehiculo');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Estado');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'estadosolvehiculo');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["estadosol"] = $this->Garaje_Model->getEstadoSolbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editEstadoSol', $this->data);
		}
	}
}


//TipoRevision

public function editTipoRevision($id = NULL)
{
	$this->form_validation->set_rules('dt_Revision', 'TipoRevision', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_TipoRevision' => $this->input->post('dt_Revision'),
		);

		if ($this->Garaje_Model->editTipoRevision($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de Revision editado correctamente');
			$this->session->set_flashdata('tab', 'tiporevision');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Tipo de Revision');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'tiporevision');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["tiporevision"] = $this->Garaje_Model->getTipoRevisionbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editTipoRevision', $this->data);
		}
	}
}

//TipoChofer
public function EditTipoChofer($id = NULL)
{
	$this->form_validation->set_rules('dt_TipoChofer', 'TipoChofer', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Chofer' => $this->input->post('dt_TipoChofer'),
		);

		if ($this->Garaje_Model->EditTipoChofer($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de Chofer editado correctamente');
			$this->session->set_flashdata('tab', 'tipochofer');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Tipo de Chofer');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'tipochofer');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["tipochofer"] = $this->Garaje_Model->getTipoChoferbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/EditTipoChofer', $this->data);
		}
	}
}

//Disponibilidad
public function EditDisponibilidad($id = NULL)
{
	$this->form_validation->set_rules('dt_DisponibilidadChofer', 'Disponibilidad', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Disponibilidad' => $this->input->post('dt_DisponibilidadChofer'),
		);

		if ($this->Garaje_Model->EditDisponibilidad($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Disponibilidad editado correctamente');
			$this->session->set_flashdata('tab', 'disponibilidadchofer');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la disponibilidad');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'disponibilidadchofer');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["disponibilidad"] = $this->Garaje_Model->getDisponibilidadbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/EditDisponibilidad', $this->data);
		}
	}
}

//Poliza

public function EditPoliza($id = NULL)
{
	$this->form_validation->set_rules('dt_Poliza', 'Poliza', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'DescPoliza' => $this->input->post('dt_Poliza'),
		);

		if ($this->Garaje_Model->EditPoliza($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Poliza editado correctamente');
			$this->session->set_flashdata('tab', 'poliza');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Poliza');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'poliza');
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["poliza"] = $this->Garaje_Model->getPolizabyId($id);
		$this->data["id"] = $id;
			$this->load->view('Garaje/EditGaraje/editPoliza', $this->data);
		}
	}
}


//Vehiculo

public function EditVehiculo($id = NULL)
{
	$this->form_validation->set_rules('dt_PlacaVehiculo', 'Placa de Vehiculo', 'trim|required');
	$this->form_validation->set_rules('dt_disponibilidad', 'Disponibilidad', 'trim|required');
	$this->form_validation->set_rules('dt_kilometraje', 'Kilometraje', 'trim|required');
	$this->form_validation->set_rules('Color', 'Color', 'trim|required');
	$this->form_validation->set_rules('Poliza', 'Poliza', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'PlacaVehiculo' => $this->input->post('dt_PlacaVehiculo'),
			'Disponibilidad' => $this->input->post('dt_disponibilidad'),
			'Kilometraje' => $this->input->post('dt_kilometraje'),
			'Color_Id' => $this->input->post('Color'),
			'Polizas_idPolizas' => $this->input->post('Poliza'),

		);

		if ($this->Garaje_Model->editVehiculo($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Vehiculo editado correctamente');
			$this->session->set_flashdata('tab', 'vehiculo');
			redirect('AdminCatalogosGaraje');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Vehiculo');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminCatalogosGaraje');
	}else{

		$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["Poliza"] =$this->Garaje_Model->getListadoPoliza();
		$this->data["vehiculo"] = $this->Garaje_Model->getVehiculobyId($id);

		$this->data["id"] = $id;
		$this->load->view('Garaje/EditGaraje/editVehiculos', $this->data);
		}
	}
}

//Edit Chofer
public function EditChofer($id = NULL)
{
	$this->form_validation->set_rules('DisponibilidadChoferes2', 'Disponibilidad', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(

			'DisponibilidadChofer_idDisponibilidadChofer' => $this->input->post('DisponibilidadChoferes2'),


		);

		if ($this->Garaje_Model->EditChofer($dataUpdate, $id))
		{
			
			$this->session->set_flashdata('message', 'Chofer editado correctamente');
			redirect('AdminCatalogosGaraje/AgregarChoferes');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el Chofer');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminCatalogosGaraje/AgregarChoferes');
	}else{

		//$this->data["Color"] =$this->Garaje_Model->getColor();
		$this->data["DisponibilidadChoferes2"] =$this->Garaje_Model->getDisponibilidadChoferes2();
		$this->data["ChoferDisponible"] = $this->Garaje_Model->getChoferbyId($id);

		$this->data["id"] = $id;
		$this->load->view('Garaje/EditGaraje/EditChofer', $this->data);
		}
	}
}

//Edit Accidente 
public function changeStatusAccidente($id=NULL)
{

		$this->form_validation->set_rules('dt_status', lang('status'), 'trim|required');
		if($this->input->get("id")){ $id = $this->input->get('id');}

		if ($this->form_validation->run())
		{

			$dataUpdate = array(
				'EstadoReparacion_idEstadoReparacion' => $this->input->post('dt_status'),
			);

			if ($this->Garaje_Model->editAccidenteStatus($dataUpdate, $id))
			{
				$this->session->set_flashdata('message', 'Accidente editado correctamente');

				redirect('AdminCatalogosGaraje/RegistrarReparaccion');
			}else {
				$this->session->set_flashdata('error', 'Error al editar el Estado');
			}
		} else{


		if (validation_errors())
		{
			$this->session->set_flashdata('error', validation_errors());

			redirect('AdminCatalogosGaraje/RegistrarReparaccion');
		}else{

			$this->data["accidente"] = $this->Garaje_Model->getAccidenteReparadobyId($id);
			$this->data["status"] = $this->Garaje_Model->getTipoEstadoVehiculo();
			$this->data["id"] = $id;
				$this->load->view('Garaje/EditGaraje/editAccidenteStatus', $this->data);
			}
		}
}


}
