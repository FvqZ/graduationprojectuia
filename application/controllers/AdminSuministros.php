<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSuministros extends MY_Controller {

	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
	  $this->load->library('Util');


		$this->load->library('form_validation');
	  $this->load->model("Suministros_Model");  /* This used to load model */
		$this->load->library('Ion_auth'); //invocar libreria de auth
		if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
    {
      redirect('auth/login', 'refresh');
    }

	}

	public function index()
	{
		$this->data['module_name']='Administracion de Módulos en Suministros';
		$this->data["Preferencial"] =$this->Suministros_Model->getPreferencial();
		$this->data["Categoria"] =$this->Suministros_Model->getCategorias();
		$this->data["Zona"] =$this->Suministros_Model->getZona();
		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$this->page_construct('Suministros/MantenimientoSuministros', $this->data);
	}

public function proveedores()
{
	$this->data['module_name']='Proveedores';
	$this->data["TipoProveedor"] =$this->Suministros_Model->getTipoProveedor();
	$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
	$this->page_construct('Suministros/Proveedores', $this->data);
}

public function compras()
{
	$this->data['module_name']='Compras';
	$this->data["usuarios"] =$this->Suministros_Model->getUsers();
	$this->data["tipo_compra"] =$this->Suministros_Model->getTipoCompras();
	$this->data["proveedores"] =$this->Suministros_Model->getProveedores();
	$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
	$this->page_construct('Suministros/Compras', $this->data);
}

public function ListadoPedidos()
{
	$this->data['module_name']='Listado';
	$this->data["usuarios"] =$this->Suministros_Model->getUsers();

	$this->data["tipo_compra"] =$this->Suministros_Model->getTipoCompras();
	$this->data["proveedores"] =$this->Suministros_Model->getProveedores();
	$this->data["EstadoSolicitud"] =$this->Suministros_Model->getEstadoSolicitud();
	$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
	$this->page_construct('Suministros/ListadoPedidos', $this->data);
}
	////Email

	public function send_mail() {
        //Load email library
        $this->util->send_email("fran.95.v@gmail.com", 'Prueba Correo', 'Testing the email class.');
    }


	//CategoriasSuministros

	public function AgregarCategoriaSuministros()
	{
		$this->data['module_name']='Suministros/Agregar Categoria';

		redirect('AdminSuministros');

	}


	public function CargarCategoriaSuministros()
	{

	$q = $this->Suministros_Model->getCategorias();

			echo json_encode($q);
	}


	public function InsertCategorias()
	{

		$this->form_validation->set_rules('dt_Categoria', 'Descripción de Categoría', 'trim|required');
		if ($this->form_validation->run())
		{
			$data = array(
				'Desc_Categorias' => $this->input->post('dt_Categoria')
			);
			if ($this->Suministros_Model->InsertCategorias($data))
			{
				$this->session->set_flashdata('tab', 'Categoria');
				$this->session->set_flashdata('message', 'Categoría agregada correctamente');

			} else {
				$this->session->set_flashdata('tab', 'Categoria');
				$this->session->set_flashdata('error', 'Error al agregar la categoría');
			}
			redirect("AdminSuministros");
		}

		$this->session->set_flashdata('error', validation_errors());
		redirect("AdminSuministros");

	}



	public function InsertTipoProveedor()
	{
		$this->form_validation->set_rules('dt_TipoProveedor', 'Tipo de proveedor', 'trim|required');
		$this->session->set_flashdata('tab', 'TipoProveedor');
		if ($this->form_validation->run())
		{
			$data = array('Desc_TipoProveedor' => $this->input->post('dt_TipoProveedor'));
			if ($this->Suministros_Model->InsertTipoProveedor($data))
			{
				$this->session->set_flashdata('message', 'Tipo de proveedor agregaado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar el tipo de proveedor');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors());
		}
		redirect('AdminSuministros');

	}

	public function InsertZona()
	{

		$this->form_validation->set_rules('dt_Zona', 'Zona', 'required');
		$this->session->set_flashdata('tab', 'Zonas');

		if ($this->form_validation->run())
		{
			$data = array(
				'Desc_Zona' => $this->input->post('dt_Zona')
			);
			if ($this->Suministros_Model->InsertZona($data))
			{
				$this->session->set_flashdata('message', 'Zona agregada correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar zona');
			}


		} else {
			$this->session->set_flashdata('error', validation_errors());
		}

		redirect('AdminSuministros');
	}


	public function InsertEdificio()
	{

		$this->form_validation->set_rules('dt_Edificio', 'Edificio', 'required');
		$this->session->set_flashdata('tab', 'Edificio');
		if ($this->form_validation->run())
		{
			$data = array(
				'Desc_Region' => $this->input->post('dt_Edificio')
			);
			if ($this->Suministros_Model->InsertEdificio($data))
			{
				$this->session->set_flashdata('message', 'Zona agregada correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar zona');
			}


		} else {
			$this->session->set_flashdata('error', validation_errors());
		}

		redirect('AdminSuministros');
	}

	// End Categorias Suministros

	// Inventarios

	public function AgregarInventario()
	{
		$this->data['module_name']='Suministros/Agregar Categoria';
		redirect('AdminSuministros');

	}


	public function CargarInventarios()
	{

		$q =$this->Suministros_Model->getInventario();
		echo json_encode($q);
	}


	public function CargarSolicitudes()
	{

		$q =$this->Suministros_Model->getSolicitudes();
		echo json_encode($q);
	}


	public function InsertInv()
	{

		$this->form_validation->set_rules('Categoria', lang('category'), 'trim|required');
		$this->form_validation->set_rules('dt_item', lang('item'), 'trim|required');
		$this->form_validation->set_rules('dt_unitario', 'Costo', 'trim|required');
		$this->form_validation->set_rules('dt_fecha', 'Fecha de Ingreso', 'trim|required');
		$this->form_validation->set_rules('Preferencial', 'Preferencial', 'trim|required');
		$this->form_validation->set_rules('Zona', 'Zona', 'trim|required');
		$this->form_validation->set_rules('dt_minimo', 'Cantidad Mínima', 'trim|required');
		$this->form_validation->set_rules('Edificio', 'Edificio', 'trim|required');

		if ($this->form_validation->run())
		{
			$data = array(
				'Id_RefCategoria' => $this->input->post('Categoria'),
				'DescripcionInv' => $this->input->post('dt_item'),
				'Cantidad' => $this->input->post('dt_cantidad'),
				'costo' => $this->input->post('dt_unitario'),
				'FechaIngreso' => $this->input->post('dt_fecha'),
				'Preferencial_id' => $this->input->post('Preferencial'),
				'Zona_idZona' => $this->input->post('Zona'),
				'cantidadminima' => $this->input->post('dt_minimo'),
				'region_idRegion' => $this->input->post('Edificio')

			);
			if($this->Suministros_Model->InsertInventarios($data))
			{
					$this->session->set_flashdata('message', 'Item agregado correctamente');
			}
			else
			{
				$this->session->set_flashdata('error', 'Error al agregar el item');
			}
		}
		else
		{
				$this->session->set_flashdata('error', validation_errors());
		}


		redirect('AdminSuministros');
	}


	// End Inventarios


	//Estado

	public function AgregarEstadoSuministros()
	{
		$this->data['module_name']='Suministros/Agregar Categoria';
		$this->session->set_flashdata('tab', 'Estado');
	redirect('AdminSuministros');

	}


	public function CargarEstadoSuministros()
	{

	$q = $this->Suministros_Model->getEstadoSolicitud();

			echo json_encode($q);
	}


	public function InsertEstadoSuministro()
	{
		$this->form_validation->set_rules('dt_EstadoSolicitud', 'Estado de Solicitud', 'trim|required');
		if ($this->form_validation->run())
		{
			$data = array(
				'Descripcion' => $this->input->post('dt_EstadoSolicitud')
			);
			if ($this->Suministros_Model->InsertEstadoSuministro($data))
			{
				$this->session->set_flashdata('message', 'Estado agregado correctamente');
				$this->session->set_flashdata('tab', 'Estado');

			} else {
				$this->session->set_flashdata('error', 'Error al agregar el estado');
				$this->session->set_flashdata('tab', 'Estado');
			}
			redirect('AdminSuministros');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			$this->session->set_flashdata('tab', 'Estado');
			redirect('AdminSuministros');
		}
	}


	//End Estado

	//Preferencia

	public function AgregarPreferencia()
	{
		$this->data['module_name']='Suministros/Agregar Categoria';
		$this->session->set_flashdata('tab', 'Preferencial');
		redirect('AdminSuministros');

	}


	public function CargarPreferencial()
	{

	$q = $this->Suministros_Model->getPreferencial();

			echo json_encode($q);
	}


	public function InsertPreferencial()
	{
		$this->form_validation->set_rules('dt_Preferencial', 'Descripción preferencial', 'trim|required');
		$this->session->set_flashdata('tab', 'Preferencial');
		if ($this->form_validation->run())
		{
			$data = array(
				'Desc_Preferencial' => $this->input->post('dt_Preferencial')
			);
			if ($this->Suministros_Model->InsertPreferencial($data))
			{
				$this->session->set_flashdata('message', 'Preferencial agregado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Erro al agregar el preferencial');
			}
		}else {
			$this->session->set_flashdata('error', validation_errors());
		}
		redirect('AdminSuministros');
	}

	//Borrado

	public function BorrarCategorias($id = null)
	{
		$id = $this->input->get("id");
		$result = $this->Suministros_Model->BorrarCategorias($id);

		if ($result)
		{
			$this->session->set_flashdata('message', 'Categoría eliminada correctamente');
			redirect("AdminSuministros");
		}
			$this->session->set_flashdata('error', 'No se pudo eliminar la categoría');
			redirect("AdminSuministros");

	}

public function BorrarInventario($id = null)
	{
		$id = $this->input->get("id");
		$result = $this->Suministros_Model->BorrarInventario($id);
		$this->session->set_flashdata('tab', 'Inventario');
		if ($result)
		{
			$this->session->set_flashdata('message', 'Producto eliminada correctamente');
			redirect("AdminSuministros");
		}
			$this->session->set_flashdata('error', 'No se pudo eliminar el producto');
			redirect("AdminSuministros");

	}

public function BorrarEstadoSolicitud($id = null)
	{
		$id = $this->input->get("id");
		$result = $this->Suministros_Model->BorrarEstadoSolicitud($id);
		$result = $this->Suministros_Model->BorrarInventario($id);
		$this->session->set_flashdata('tab', 'Estado');
		if ($result)
		{
			$this->session->set_flashdata('message', 'Estado Solicitud eliminada correctamente');
			redirect("AdminSuministros");
		}
			$this->session->set_flashdata('error', 'No se pudo eliminar el estado solicitud');
			redirect("AdminSuministros");

	}

	public function BorrarPreferencial($id = null)
	{
		$id = $this->input->get("id");
		$result = $this->Suministros_Model->BorrarPreferencial($id);
		$result = $this->Suministros_Model->BorrarInventario($id);
			$this->session->set_flashdata('tab', 'Preferencial');
		if ($result)
		{
			$this->session->set_flashdata('message', 'Preferencia eliminada correctamente');
			redirect("AdminSuministros");
		}
			$this->session->set_flashdata('error', 'No se pudo eliminar la Preferencia');
			redirect("AdminSuministros");

	}
	//EndBorrado




	////////////////Listado de Inventario-Grafico///////////////////////


	public function ListadoInventario()
	{
		$this->data['module_name']='Listado de Inventarios';
		$this->data["Preferencial"] =$this->Suministros_Model->getPreferencial();
		$this->data["Categoria"] =$this->Suministros_Model->getCategorias();
		$this->data["Zona"] =$this->Suministros_Model->getZona();
		$this->data["Edificio"] =$this->Suministros_Model->getEdificio();

		$this->page_construct('Suministros/Listado', $this->data);
	}



public function addSolicitud()
{


	$this->form_validation->set_rules('TipoProveduria', 'Tipo de Proveeduria', 'trim|required');
	$this->form_validation->set_rules('justificacion', 'Justificación', 'trim|required');
	$this->form_validation->set_rules('dt_cantidad', 'Cantidad', 'trim|required');
	$this->form_validation->set_rules('Categoria', 'Categoría', 'trim|required');


	if ($this->form_validation->run())
	{
		$data = array(
			'users_id' => $this->session->userdata('user_id'),
			'id_RefTipoSolicitud' => $this->input->post('TipoProveduria'),
			'Justificacion' => $this->input->post('justificacion'),
			'FechaSolicitud' => date('Y-m-d'),
			'EstadoSolicitud_idEstadoSolicitud' => 1,
			'CantidadPedido' => $this->input->post('dt_cantidad'),
			'inventarios_id' => $this->input->post('Categoria'),

		);

		if ($this->Suministros_Model->validatePreferential($data))
		{
			$this->session->set_flashdata('error', 'No se pudo agregar la solicitud ya que el item preferencial no cuenta con cantidad suficiente en el inventario');
			redirect('AdminSuministros/CargarSolicitud');
			return;
		}


		$InsertInventarios=$this->Suministros_Model->InsertSolicitudProveeduria($data);
		if ($InsertInventarios)
		{
			if ($InsertInventarios->EstadoSolicitud_idEstadoSolicitud==2){
				;
				if($inventariominimo = $this->Suministros_Model->ValidacionInv($InsertInventarios->id))
				{
					$this->util->send_email('fran.95.v@gmail.com', 'Alerta de cantidad Mínima', 'El producto '.$inventariominimo->DescripcionInv.' a llegado a su cantidad mínima, con una cantidad actual de '.$inventariominimo->Cantidad.'.' );
				}
			}
			$this->session->set_flashdata('message', 'Solicitud agregada correctamente');
	}


	} else {

		$this->session->set_flashdata('error', validation_errors());
	}
	redirect('AdminSuministros/CargarSolicitud');
}


public function CargarSolicitud()
{

	$this->data['module_name']='Solicitud de Suministros';
	$this->data["Item"] =$this->Suministros_Model->getItems();
	$this->data["Estados"] = $this->Suministros_Model->getEstadoSolicitud();
	$this->data["Preferencial"] = $this->Suministros_Model->getPreferencial();
	$this->data["tipoProveeduria"] = $this->Suministros_Model->getSolicitudProveeduria();

	$this->page_construct('Suministros/SolicitudSuministros', $this->data);

}

	public function GetInventarioPorMes()
	{
		//se puede hacer por categoria o nombre
		//$id = $this->input->get("categoria");
		//$name = $this->input->get("producto");

		$alldata = $this->Suministros_Model->getInventario();
		//$datapermonth = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$labels = array();
		$data = array();
foreach ($alldata["data"] as $row)
{
	array_push($labels, $row->DescripcionInv);
	array_push($data, $row->Cantidad);
}



		 echo json_encode(array("labels" => $labels, "data" => $data));



	}


public function getTipoCompras()
{
	$q = $this->Suministros_Model->getTipoCompras();
	echo json_encode($q);
}

public function getTipoProveedor()
{
	$q = $this->Suministros_Model->getTipoProveedor();
	echo json_encode($q);
}

public function getZonas()
{
	$q = $this->Suministros_Model->getZonas();
	echo json_encode($q);
}


public function getEdificio()
{
	$q = $this->Suministros_Model->getEdificio();
	echo json_encode($q);
}


public function InsertTipoCompra()
{
	$this->form_validation->set_rules('dt_TipoCompra', 'Tipo de Compra', 'required');
	$this->session->set_flashdata('tab', 'TipoCompra');
	if ($this->form_validation->run())
	{
		$data = array('DescTipoCompra' => $this->input->post('dt_TipoCompra'));
		if ($this->Suministros_Model->InsertTipoCompra($data))
		{
			$this->session->set_flashdata('message', 'Tipo de compra agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar tipo de compra');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminSuministros');
}


	public function deleteTipoCompra($id = NULL)
	{
		if($this->input->get('id')) { $id = $this->input->get('id'); }
		$this->session->set_flashdata('tab', 'TipoCompra');
		if ($this->Suministros_Model->deleteTipoCompra($id))
		{
			$this->session->set_flashdata('message', 'Tipo de compra eliminado correctamente');
		} else {
			$this->session->set_flashdata('error', 'Error al eliminar tipo de compra');
		}
			redirect('AdminSuministros');
	}

	public function deleteTipoProveedor($id = NULL)
	{
		if($this->input->get('id')) { $id = $this->input->get('id'); }
		$this->session->set_flashdata('tab', 'TipoProveedor');

		if ($this->Suministros_Model->deleteTipoProveedor($id))
		{
			$this->session->set_flashdata('message', 'Tipo de Proveedor eliminado correctamente');
		} else {
			$this->session->set_flashdata('error', 'Error al eliminar tipo de proveedor');
		}
			redirect('AdminSuministros');
	}

	public function deleteZonas($id = NULL)
	{
		if($this->input->get('id')) { $id = $this->input->get('id'); }
		$this->session->set_flashdata('tab', 'Zonas');

		if ($this->Suministros_Model->deleteZonas($id))
		{
			$this->session->set_flashdata('message', 'Zona eliminada correctamente');
		} else {
			$this->session->set_flashdata('error', 'Error al eliminar Zona');
		}
			redirect('AdminSuministros');
	}

	public function deleteEdificio($id = NULL)
	{
		if($this->input->get('id')) { $id = $this->input->get('id'); }
		$this->session->set_flashdata('tab', '	Edificio');

		if ($this->Suministros_Model->deleteEdificio($id))
		{
			$this->session->set_flashdata('message', 'Edificio eliminada correctamente');
		} else {
			$this->session->set_flashdata('error', 'Edificio al eliminar Zona');
		}
			redirect('AdminSuministros');
	}

public function getProveedores()
{
	$q = $this->Suministros_Model->getProveedores();
	echo json_encode($q);
}



public function deleteProveedor($id=NULL)
{
	if($this->input->get('id')) { $id = $this->input->get('id'); }


	if ($this->Suministros_Model->deleteProveedor($id))
	{
		$this->session->set_flashdata('message', 'Proveedor eliminado correctamente');
	} else {
		$this->session->set_flashdata('error', 'Error al eliminar el proveedor');
	}
		redirect('AdminSuministros/proveedores');
}


public function InsertProveedor()
{
	$this->form_validation->set_rules('TipoProveedor', 'Tipo de Proveedor', 'trim|required');
	$this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
	$this->form_validation->set_rules('contactos', 'Contactos', 'trim|required');
	$this->form_validation->set_rules('email', 'Correo Electrónico', 'trim|required|valid_email');

	if ($this->form_validation->run())
	{

		$data = array(
			'idTipoProveedor' => $this->input->post('TipoProveedor'),
			'NombreProveedor' => $this->input->post("nombre"),
			"Contactos" => $this->input->post("contactos"),
			"Email" => $this->input->post("email")
		);
		if ($this->Suministros_Model->InsertProveedor($data))
		{
			$this->session->set_flashdata('message', 'Proveedor agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar tipo de compra');
		}
	} else{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminSuministros/proveedores');
}




public function EditProveedor($id = NULL)
{
	$this->form_validation->set_rules('edit_TipoProveedor', 'Tipo de Proveedor', 'trim|required');
	$this->form_validation->set_rules('edit_nombre', 'Nombre', 'trim|required');
	$this->form_validation->set_rules('edit_contactos', 'Contactos', 'trim|required');
	$this->form_validation->set_rules('edit_email', 'Correo Electrónico', 'trim|required|valid_email');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'idTipoProveedor' => $this->input->post('edit_TipoProveedor'),
			'NombreProveedor' => $this->input->post("edit_nombre"),
			"Contactos" => $this->input->post("edit_contactos"),
			"Email" => $this->input->post("edit_email")
		);
		if ($this->Suministros_Model->editProveedor($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Proveedor editado correctamente');
			redirect('AdminSuministros/proveedores');
		}else {
			$this->session->set_flashdata('error', 'Error al editar tipo de compra');
		}
	} else{

		if (validation_errors())
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('AdminSuministros/proveedores');
		} else
		{
			$this->data["TipoProveedor"] =$this->Suministros_Model->getTipoProveedor();
			$this->session->set_flashdata('error', validation_errors());
			$this->data["proveedor"] = $this->Suministros_Model->getProveedorById($id);
			$this->data["id"] = $id;
			$this->load->view('Suministros/editProveedor', $this->data);
		}


	}

}


/****************** Compras *******************/

public function getCompras()
{
	$q = $this->Suministros_Model->getCompras();
	echo json_encode($q);
}


public function InsertCompra()
{
	$this->form_validation->set_rules('user', 'Usuario', 'trim|required');
	$this->form_validation->set_rules('TipoCompra', 'Tipo de Compra', 'trim|required');
	$this->form_validation->set_rules('Proveedor', 'Proveedor', 'trim|required');
	$this->form_validation->set_rules('numero_pedido', 'Número de pedido', 'trim|required');
	$this->form_validation->set_rules('total', 'Total', 'trim|required');

	if ($this->form_validation->run())
	{

		$data = array(
			'users_id' => $this->input->post('user'),
			'idTipoCompra' => $this->input->post("TipoCompra"),
			"Proveedores_idProveedores" => $this->input->post("Proveedor"),
			"Region_idRegion" => 1,
			"NumeroPedido" => $this->input->post("numero_pedido"),
			"Total" => $this->input->post("total"),

		);
		if ($this->Suministros_Model->InsertCompra($data))
		{
			$this->session->set_flashdata('message', 'Compras agregado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Compras al agregar tipo de compra');
		}
	} else{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminSuministros/compras');
}


public function deleteCompra($id=NULL)
{
	if($this->input->get('id')) { $id = $this->input->get('id'); }


	if ($this->Suministros_Model->deleteCompra($id))
	{
		$this->session->set_flashdata('message', 'Compra eliminada correctamente');
	} else {
		$this->session->set_flashdata('error', 'Error al eliminar la compra');
	}
		redirect('AdminSuministros/compras');
}


public function EditCompra($id = NULL)
{
	$this->form_validation->set_rules('edit_user', 'Usuario', 'trim|required');
	$this->form_validation->set_rules('edit_TipoCompra', 'Tipo de Compra', 'trim|required');
	$this->form_validation->set_rules('edit_Proveedor', 'Proveedor', 'trim|required');
	$this->form_validation->set_rules('edit_numero_pedido', 'Número de pedido', 'trim|required');
	$this->form_validation->set_rules('edit_total', 'Total', 'trim|required');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'users_id' => $this->input->post('edit_user'),
			'idTipoCompra' => $this->input->post("edit_TipoCompra"),
			"Proveedores_idProveedores" => $this->input->post("edit_Proveedor"),
			"Region_idRegion" => 1,
			"NumeroPedido" => $this->input->post("edit_numero_pedido"),
			"Total" => $this->input->post("edit_total")

		);

		if ($this->Suministros_Model->editCompra($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Compra editada correctamente');
			redirect('AdminSuministros/compras');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la compra');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminSuministros/compras');
	}else{
		$this->data["usuarios"] =$this->Suministros_Model->getUsers();
		$this->data["tipo_compra"] =$this->Suministros_Model->getTipoCompras();
		$this->data["proveedores"] =$this->Suministros_Model->getProveedores();
		$this->data["compra"] = $this->Suministros_Model->getCompraById($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/editCompras', $this->data);
	}



	}

}

/*********************** Edit Catalogos ***********************/



public function EditCategoria($id = NULL)
{
	$this->form_validation->set_rules('dt_Categoria', 'Nombre de Categoría', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Categorias' => $this->input->post('dt_Categoria'),
		);

		if ($this->Suministros_Model->editCategoria($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Categoría editada correctamente');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la categoría');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminSuministros');
	}else{

		$this->data["categoria"] = $this->Suministros_Model->getCategorybyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editCategoria', $this->data);
		}
	}
}

public function EditInventario($id = NULL)
{
	$this->form_validation->set_rules('Categoria', 'Nombre de Categoría', 'trim|required');
	$this->form_validation->set_rules('dt_item', 'Item', 'trim|required');
	$this->form_validation->set_rules('dt_cantidad', 'Cantidad', 'trim|required');
	$this->form_validation->set_rules('dt_unitario', 'Costo', 'trim|required');
	$this->form_validation->set_rules('dt_fecha', 'Fecha', 'trim|required');
	$this->form_validation->set_rules('Preferencial', 'Preferencial', 'trim|required');
	$this->form_validation->set_rules('Zona', 'Zona', 'trim|required');
	$this->form_validation->set_rules('dt_minimo', 'Minimo', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Id_RefCategoria' => $this->input->post('Categoria'),
			'DescripcionInv' => $this->input->post('dt_item'),
			'Cantidad' => $this->input->post('dt_cantidad'),
			'costo' => $this->input->post('dt_unitario'),
			'FechaIngreso' => $this->input->post('dt_fecha'),
			'Preferencial_id' => $this->input->post('Preferencial'),
			'Zona_idZona' => $this->input->post('Zona'),
			'cantidadminima' => $this->input->post('dt_minimo'),
			'region_idRegion' => $this->input->post('Edificio')

		);

		if ($this->Suministros_Model->editInventario($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Inventario editado correctamente');
			$this->session->set_flashdata('tab', 'Inventario');
			redirect('AdminSuministros/ListadoInventario');
		}else {
			$this->session->set_flashdata('error', 'Error al editar inventario');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminSuministros/ListadoInventario');
	}else{

		$this->data["Preferencial"] =$this->Suministros_Model->getPreferencial();
		$this->data["Categoria"] =$this->Suministros_Model->getCategorias();
		$this->data["Zona"] =$this->Suministros_Model->getZona();
		$this->data["inventario"] = $this->Suministros_Model->getInventariobyId($id);
		$this->data["Edificio"] = $this->Suministros_Model->getEdificio($id);

		$this->data["id"] = $id;
		$this->load->view('Suministros/EditCatalogos/editInventario', $this->data);
		}
	}
}


public function sumInventario($id = NULL)
{
	$this->form_validation->set_rules('dt_cantidad', 'Cantidad', 'trim|required');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Cantidad' => $this->input->post('dt_cantidad'),
		);

		if ($this->Suministros_Model->sumInventario($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Cantidad agregada correctamente');
			redirect('AdminSuministros/ListadoInventario');
		}else {
			$this->session->set_flashdata('error', 'Error al editar inventario');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'Inventario');
		redirect('AdminSuministros/ListadoInventario');
	}else{

		$this->data["Preferencial"] =$this->Suministros_Model->getPreferencial();
		$this->data["Categoria"] =$this->Suministros_Model->getCategorias();
		$this->data["Zona"] =$this->Suministros_Model->getZona();
		$this->data["inventario"] = $this->Suministros_Model->getInventariobyId($id);

		$this->data["id"] = $id;
		$this->load->view('Suministros/EditCatalogos/sumInventario', $this->data);
		}
	}
}


public function EditEstadoSuministro($id = NULL)
{
	$this->form_validation->set_rules('dt_EstadoSolicitud', 'Descripción Estado', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Descripcion' => $this->input->post('dt_EstadoSolicitud'),
		);

		if ($this->Suministros_Model->editEstadoSolicitud($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Estado Solicitud editado correctamente');
			$this->session->set_flashdata('tab', 'Estado');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el estado solicitud');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'Estado');
		redirect('AdminSuministros');
	}else{

		$this->data["estado"] = $this->Suministros_Model->getEstadoSolicitudbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editEstado', $this->data);
		}
	}
}


public function EditCatalogoPreferencial($id = NULL)
{
	$this->form_validation->set_rules('dt_Preferencial', 'Estado Preferencial', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Preferencial' => $this->input->post('dt_Preferencial'),
		);

		if ($this->Suministros_Model->editEstadoPreferencial($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Estado Preferencial editado correctamente');
			$this->session->set_flashdata('tab', 'Preferencial');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el estado preferencial');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'Preferencial');
		redirect('AdminSuministros');
	}else{

		$this->data["estado"] = $this->Suministros_Model->getEstadoPreferencialbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editCatalogoPreferencial', $this->data);
		}
	}
}







public function EditTipoCompra($id = NULL)
{
	$this->form_validation->set_rules('dt_TipoCompra', 'Tipo de Compra', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'DescTipoCompra' => $this->input->post('dt_TipoCompra'),
		);

		if ($this->Suministros_Model->editTipoCompra($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de compra editado correctamente');
			$this->session->set_flashdata('tab', 'TipoCompra');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el tipo de compra');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'TipoCompra');
		redirect('AdminSuministros');
	}else{

		$this->data["tipocompra"] = $this->Suministros_Model->getTipoComprabyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editTipoCompra', $this->data);
		}
	}
}



public function EditTipoProveedor($id = NULL)
{
	$this->form_validation->set_rules('dt_TipoProveedor', 'Tipo de Proveedor', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_TipoProveedor' => $this->input->post('dt_TipoProveedor'),
		);

		if ($this->Suministros_Model->EditTipoProveedor($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Tipo de proveedor editado correctamente');
			$this->session->set_flashdata('tab', 'TipoProveedor');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el tipo de proveedor');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'TipoProveedor');
		redirect('AdminSuministros');
	}else{

		$this->data["tipoproveedor"] = $this->Suministros_Model->getTipoProveedorbyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editTipoProveedor', $this->data);
		}
	}
}
//Zona

public function EditZona($id = NULL)
{
	$this->form_validation->set_rules('dt_Zona', 'Zona', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Zona' => $this->input->post('dt_Zona'),
		);

		if ($this->Suministros_Model->EditZona($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Zona editado correctamente');
			$this->session->set_flashdata('tab', 'Zonas');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Zona');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'Zonas');
		redirect('AdminSuministros');
	}else{

		$this->data["zona"] = $this->Suministros_Model->getZonabyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editZona', $this->data);
		}
	}
}



public function EditEdificio($id = NULL)
{
	$this->form_validation->set_rules('dt_Edificio', 'Edificio', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Desc_Region' => $this->input->post('dt_Edificio'),
		);

		if ($this->Suministros_Model->EditEdificio($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Edificio editado correctamente');
			$this->session->set_flashdata('tab', 'Edificio');
			redirect('AdminSuministros');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Edificio');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'Edificio');
		redirect('AdminSuministros');
	}else{

		$this->data["edificio"] = $this->Suministros_Model->getEdificiobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editEdificio', $this->data);
		}
	}
}


//uPDATE

//Listado


public function EditListado($id = NULL)
{
	$this->form_validation->set_rules('edit_Estado', 'estado', 'trim|required');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$overInv = $this->Suministros_Model->validateSuministros($id);
		if ($overInv && $this->input->post('edit_Estado') == 2)
		{
			$this->session->set_flashdata('error', 'No se puede aprobar ya que sobrepasa la cantidad disponible');
			redirect('AdminSuministros/ListadoPedidos');
		}

		$dataUpdate = array(
			'EstadoSolicitud_idEstadoSolicitud' => $this->input->post('edit_Estado')
		);

		if ($this->Suministros_Model->EditListado($dataUpdate, $id))
		{
			if ($dataUpdate["EstadoSolicitud_idEstadoSolicitud"]==2)
			{
				$inventario = $this->Suministros_Model->restaInventario($dataUpdate, $id);

				if($inventariominimo = $this->Suministros_Model->ValidacionInv($inventario))
				{
					$this->util->send_email('fran.95.v@gmail.com', 'Alerta de cantidad Mínima', 'El producto '.$inventariominimo->DescripcionInv.' ha llegado a su cantidad mínima, con una cantidad actual de '.$inventariominimo->Cantidad.'.' );
				}
			}
			$this->session->set_flashdata('message', 'Listado editado correctamente');
			redirect('AdminSuministros/ListadoPedidos');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Partida');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminSuministros/ListadoPedidos');
	}else{
		$this->data["EstadoSolicitud"] = $this->Suministros_Model->getEstadoSolicitudLista($id);
		$this->data["estado"] = $this->Suministros_Model->getListadobyId($id);
		$this->data["id"] = $id;
			$this->load->view('Suministros/EditCatalogos/editListado', $this->data);
		}
	}
}

//Grafico

public function GetGastosVehiculo()
{
	//se puede hacer por categoria o nombre
	//$id = $this->input->get("categoria");
	//$name = $this->input->get("producto");

	$alldata = $this->Suministros_Model->getSumCostosCompras();
	//$datapermonth = array(0,0,0,0,0,0,0,0,0,0,0,0);
	$labels = array();
	$data = array();
foreach ($alldata["data"] as $row)
{
array_push($labels, $row->proveedor);
array_push($data, $row->total);
}



	echo json_encode(array("labels" => $labels, "data" => $data));



}



}
