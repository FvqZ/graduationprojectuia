<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminCatalogosCajaChica extends MY_Controller {

    function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
	  $this->load->model("CajaChica_Model");  /* This used to load model */
    $this->load->library('Ion_auth'); //invocar libreria de auth
	$this->load->library('form_validation');
	$this->load->library('Util');

    if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
    {
      redirect('auth/login', 'refresh');
    }
  }

	public function index()
	{
		$this->data['module_name']='Administración de Módulos en Caja Chica';
		$this->data["Presupuesto"] =$this->CajaChica_Model->getPresupuesto();
		$this->data["Partidas"] =$this->CajaChica_Model->getPartidas();
		$this->data["Compras"] =$this->CajaChica_Model->getCompras();
        $this->data["Estado"] =$this->CajaChica_Model->getEstado();

		$this->data["PresupuestoAnual"] =$this->CajaChica_Model->getPresupuestoAnual();
		$this->data["CategoriaPartida"] =$this->CajaChica_Model->getCategoriaPartida();
		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

		$this->page_construct('CajaChica/CatalogosCajaChica', $this->data);

	}

	public function SolicitudCajaChica()
	{
		$this->data['module_name']='Solicitud por Caja Chica';
		$this->data["EstadoSolicitud"] =$this->CajaChica_Model->getEstadoSolicitud();
		$this->data["PartidaSeleccionada"] =$this->CajaChica_Model->getPartidaSeleccionada();
		$this->data["UsuarioSeleccionado"] =$this->CajaChica_Model->getUsuarioSeleccionado();


		$this->page_construct('CajaChica/SolicitudCajaChica', $this->data);

	}

	public function ListadoSolicitudesCajaChica()
	{
		$this->data['module_name']='Solicitud por Caja Chica';
		$this->data["EstadoSolicitud"] =$this->CajaChica_Model->getEstadoSolicitud();
		$this->load->library('email');

		$this->data["PartidaSeleccionada"] =$this->CajaChica_Model->getPartidaSeleccionada();
		$this->data["UsuarioSeleccionado"] =$this->CajaChica_Model->getUsuarioSeleccionado();


		$this->page_construct('CajaChica/ListadoSolicitudesCajaChica', $this->data);

	}


//Insert Presupuesto

	public function InsertPresupuesto()
{
	$this->form_validation->set_rules('dt_monto', 'Monto', 'trim|required');
	$this->form_validation->set_rules('dt_anno', 'Año', 'trim|required');
	
	$this->session->set_flashdata('tab', 'presupuesto');
	if ($this->form_validation->run())
	{
		$data = array(
			'Monto' => $this->input->post('dt_monto'),
            'AnnoPresupuesto' => $this->input->post('dt_anno')
		);

		if ($this->CajaChica_Model->InsertPresupuesto($data))
		{
			$this->session->set_flashdata('message', 'Presupuesto registrado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Presupuesto');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosCajaChica');
}

	//Insert Caja Chica

	public function InsertEstadoCajaChica()
{
	$this->form_validation->set_rules('dt_estado', 'Estado Solicitud', 'trim|required');
	
	$this->session->set_flashdata('tab', 'estadocajachica');
	if ($this->form_validation->run())
	{
		$data = array(
			'Descripcion' => $this->input->post('dt_estado')
		);

		if ($this->CajaChica_Model->InsertEstadoCajaChica($data))
		{
			$this->session->set_flashdata('message', 'Estado registrado correctamente');
		}else {
			$this->session->set_flashdata('error', 'Error al agregar el Estado');
		}
	}
	else
	{
		$this->session->set_flashdata('error', validation_errors());

	}
	redirect('AdminCatalogosCajaChica');
}

//Insert Partida

	public function InsertPartida()
	{
		$this->form_validation->set_rules('dt_Partida', 'Partida', 'trim|required');
		
		$this->session->set_flashdata('tab', 'partida');
		if ($this->form_validation->run())
		{
			$data = array(
				'Presupuestoporpartida' => $this->input->post('dt_Partida'),
				'Presupuesto_idPresupuesto' => $this->input->post('PresupuestoAnual'),
				'categoriasinventarios_id' => $this->input->post('CategoriaPartida'),
			);
	
			if ($this->CajaChica_Model->InsertPartida($data))
			{
				$this->session->set_flashdata('message', 'Partida registrado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar el Partida');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
	
		}
		redirect('AdminCatalogosCajaChica');
	}


	public function InsertSolicitudCajaChica()
	{
		$this->form_validation->set_rules('dt_factura', 'Facturas', 'trim|required');
		$this->form_validation->set_rules('dt_razon', 'Razon', 'trim|required');
		$this->form_validation->set_rules('dt_Total', 'Total', 'trim|required');
		

		if ($this->form_validation->run())
		{
			if($this->input->post('dt_Total') <= 75000){
				$data = array(
					'FechaCompra' => date("Y-m-d"),
					'Nota' => $this->input->post('dt_factura'),
					'Razon' => $this->input->post('dt_razon'),
					'Estado_idEstado' => 1,
					'Partidas_idPartidas' => $this->input->post('PartidaSeleccionada'),
					'users_id' => $this->input->post('UsuarioSeleccionado'),
					'TotalFactura' => $this->input->post('dt_Total')
		
				);
			}else{
				$data = array(
					'FechaCompra' => date("Y-m-d"),
					'Nota' => $this->input->post('dt_factura'),
					'Razon' => $this->input->post('dt_razon'),
					'Estado_idEstado' => 2,
					'Partidas_idPartidas' => $this->input->post('PartidaSeleccionada'),
					'users_id' => $this->input->post('UsuarioSeleccionado'),
					'TotalFactura' => $this->input->post('dt_Total')
				);
		
			}
	
			if ($this->CajaChica_Model->InsertSolicitudCajaChica($data))
			{
				$this->session->set_flashdata('message', 'Partida registrado correctamente');
			}else {
				$this->session->set_flashdata('error', 'Error al agregar el Partida');
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
	
		}
		redirect('AdminCatalogosCajaChica/SolicitudCajaChica');
	}

    public function BorrarPresupuesto($id = null)
{

		if ($this->input->get("id")) { $id = $this->input->get("id");	}
		if ($this->CajaChica_Model->BorrarPresupuesto($id))
		{
			$this->session->set_flashdata('message', 'Presupuesto borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar el presupuesto');
		}
		redirect("AdminCatalogosCajaChica");

}

public function BorrarPartida($id = null)
{

		if ($this->input->get("id")) { $id = $this->input->get("id");	}
		if ($this->CajaChica_Model->BorrarPartida($id))
		{
			$this->session->set_flashdata('message', 'Partida borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar la partida');
		}
		redirect("AdminCatalogosCajaChica");
		

}

public function BorrarEstado($id = null)
{

		if ($this->input->get("id")) { $id = $this->input->get("id");	}
		if ($this->CajaChica_Model->BorrarEstado($id))
		{
			$this->session->set_flashdata('message', 'Partida borrado correctamente');
		} else {
			$this->session->set_flashdata('error', 'No se pudo borrar la partida');
		}
		redirect("AdminCatalogosCajaChica");

}

public function CargarPartida()
{

    $data = $this->CajaChica_Model->getPartidas() ;
    echo json_encode($data);

}

public function CargarPresupuesto()
{

    $data = $this->CajaChica_Model->getPresupuesto() ;
    echo json_encode($data);

}

public function CargarEstadoCajaChica()
{

    $data = $this->CajaChica_Model->getEstadoCajaChica() ;
    echo json_encode($data);

}

public function CargarSolicitudCajaChica()
{
    $SolicitudCajaChica = $this->CajaChica_Model->getSolicitudCajaChica();

        echo json_encode($SolicitudCajaChica);
}

//Updates

//Presupuesto

public function EditPresupuesto($id = NULL)
{
	$this->form_validation->set_rules('dt_monto', 'presupuesto', 'trim|required');
	$this->form_validation->set_rules('dt_anno', 'presupuesto', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Monto' => $this->input->post('dt_monto'),
			'AnnoPresupuesto' => $this->input->post('dt_anno'),
		);

		if ($this->CajaChica_Model->EditPresupuesto($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Presupuesto editado correctamente');
			$this->session->set_flashdata('tab', 'presupuesto');
			redirect('AdminCatalogosCajaChica');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el presupuesto');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'presupuesto');
		redirect('AdminCatalogosCajaChica');
	}else{

		$this->data["presupuesto"] = $this->CajaChica_Model->getPresupuestobyId($id);
		$this->data["id"] = $id;
			$this->load->view('CajaChica/EditCajaChica/editPresupuesto', $this->data);
		}
	}
}

//Estado


public function EditEstado($id = NULL)
{
	$this->form_validation->set_rules('dt_estado', 'estado', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Descripcion' => $this->input->post('dt_estado'),

		);

		if ($this->CajaChica_Model->EditEstado($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Estado editado correctamente');
			$this->session->set_flashdata('tab', 'estadocajachica');
			redirect('AdminCatalogosCajaChica');
		}else {
			$this->session->set_flashdata('error', 'Error al editar el estado');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'estadocajachica');
		redirect('AdminCatalogosCajaChica');
	}else{

		$this->data["estado"] = $this->CajaChica_Model->getEstadobyId($id);
		$this->data["id"] = $id;
			$this->load->view('CajaChica/EditCajaChica/editEstado', $this->data);
		}
	}
}

public function EditPartidas($id = NULL)
{
	$this->form_validation->set_rules('dt_Partida', 'partidas', 'trim|required');

	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Presupuestoporpartida' => $this->input->post('dt_Partida'),
			'Presupuesto_idPresupuesto' => $this->input->post('edit_TipoProveedor'),
			'categoriasinventarios_id' => $this->input->post('edit_TipoPartida'),


		);

		if ($this->CajaChica_Model->EditPartidas($dataUpdate, $id))
		{
			$this->session->set_flashdata('message', 'Partida editado correctamente');
			$this->session->set_flashdata('tab', 'partida');
			redirect('AdminCatalogosCajaChica');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Partida');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		$this->session->set_flashdata('tab', 'partida');
		redirect('AdminCatalogosCajaChica');
	}else{
		$this->data["PresupuestoAnual"] = $this->CajaChica_Model->getPresupuestoAnual($id);
		$this->data["CategoriaPartida"] = $this->CajaChica_Model->getCategoriaPartida($id);
		$this->data["partidas"] = $this->CajaChica_Model->getPartidabyId($id);
		$this->data["id"] = $id;
			$this->load->view('CajaChica/EditCajaChica/editPartida', $this->data);
		}
	}
}


//Listado


public function EditListado($id = NULL)
{
	$this->form_validation->set_rules('edit_Estado', 'estado', 'trim|required');
	if($this->input->get("id")){ $id = $this->input->get('id');}

	if ($this->form_validation->run())
	{

		$dataUpdate = array(
			'Estado_idEstado' => $this->input->post('edit_Estado'),



		);

		if ($this->CajaChica_Model->EditListado($dataUpdate, $id))
		{

			if ($dataUpdate["Estado_idEstado"]==3)
      {


				if($cajachichaemail= $this->CajaChica_Model->getListadobyId($id))
        {
            $this->util->send_email('fran.95.v@gmail.com', 'Solicitud de Caja Chica Aprobada', 'La solicitud de caja chica con id '.$cajachichaemail->idComprasCajaChica.' con fecha de solicitud '.$cajachichaemail->FechaCompra. ' ha sido aprobada');
        }

			}

			$this->session->set_flashdata('message', 'Listado editado correctamente');
			redirect('AdminCatalogosCajaChica/ListadoSolicitudesCajaChica');
		}else {
			$this->session->set_flashdata('error', 'Error al editar la Partida');
		}
	} else{


	if (validation_errors())
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('AdminCatalogosCajaChica/ListadoSolicitudesCajaChica');
	}else{
		$this->data["EstadoSolicitud"] = $this->CajaChica_Model->getEstadoSolicitud($id);
		$this->data["estado"] = $this->CajaChica_Model->getListadobyId($id);
		$this->data["id"] = $id;
			$this->load->view('CajaChica/EditCajaChica/EditListado', $this->data);
		}
	}
}



}
