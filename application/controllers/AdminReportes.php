<?php
  defined('BASEPATH') OR exit('No direct script access allowed');


  /**
   *
   */
  class AdminReportes extends MY_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->helper('form');
     $this->load->model("Reporte_Model");  /* This used to load model */
     $this->load->library('form_validation');

     $this->load->library('Ion_auth'); //invocar libreria de auth
     $this->load->library('form_validation');
     if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
     {
       redirect('auth/login', 'refresh');
     }
    }






    public function index()
    {
      $this->data['module_name']='Reporte de Inventario';
      $this->data["producto_data"] = $this->Reporte_Model->getInventario();
      $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
      $this->page_construct('Reportes/index', $this->data);
    }


    public function getInventario()
    {
      if ($this->input->get('fecha_inicio')) { $fecha_inicio = $this->input->get('fecha_inicio');  } else{ $fecha_inicio = NULL; }
      if ($this->input->get('fecha_final')) { $fecha_final = $this->input->get('fecha_final');  } else{ $fecha_final = NULL; }
      if ($this->input->get('producto')) { $producto = $this->input->get('producto');  } else{ $producto = NULL; }

      $array_where = array(
        'producto' => $producto,
        'fecha_inicio' => $fecha_inicio,
        'fecha_final' => $fecha_final );

        $q = $this->Reporte_Model->getInventarioRows($array_where);

        echo json_encode($q);
    }





  }






 ?>
