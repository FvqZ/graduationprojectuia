<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainMenu extends MY_Controller {

	function __construct()
	{
	parent::__construct();
	$this->load->library('Ion_auth'); //invocar libreria de auth
	if (!$this->ion_auth->logged_in()) // redireccionar a login si no esta logeado
	{
		redirect('auth/login', 'refresh');
	}

}

	public function index()
	{
		$this->data['module_name']='Menu Principal';
		$this->page_construct('MenuPrincipal', $this->data);

	}
}
