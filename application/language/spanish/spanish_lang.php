<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*+++++++++++++++++HEADER+++++++++++++++*/
//Garaje
$lang["login"] = "Iniciar Sesión";
$lang["login_page"] = "Inicio de Sesión";
$lang["home"] = "Inicio";
$lang["garage"] = "Garaje";
$lang["supplies"] = "Suministros";
$lang["small_box"] = "Caja Chica";
$lang["users"] = "Usuarios";
$lang["reports"] = "Reportes";
$lang["request_vehicle"] = "Solicitar Vehículo";
$lang["request_list"] = "Listado de Solicitudes";
$lang["add_accident"] = "Registrar un Accidente";
$lang["add_repair"] = "Registrar una Reparación";
$lang["catalogo_support"] = "Mantenimiento de Catálogos";
$lang["driver_list"] = "Listado de Choferes";
$lang["add_support"] = "Registrar un Mantenimiento";
$lang["trips_list"] = "Listado de Viajes";
$lang["list_invoices"] = "Listado de Facturas";
$lang["add_invoices"] = "Reportar Facturas";

// suministros
$lang["request_supplies"] = "Solicitud de Suministros";
$lang["available_inventory"] = "Inventario Disponible";
$lang["provider"] = "Proveedor";
$lang["sales"] = "Compras";
$lang["order_list"] = "Lista de Pedidos";

// caja chica
$lang["petty_cash"] = "Caja Chica";
$lang["new_request"] = "Nueva Solicitud";

//ususarios
$lang["users"] = "Usuarios";
$lang["users_list"] = "Listado de Usuarios";

// Reportes
$lang["reports"] = "Reportes";
$lang["inventory_reports"] = "Reporte de Inventarios";
$lang["logout"] = "Cerrar Sesión";


/*++++++++++++++++++++GARAJE++++++++++++++*/
$lang["ID"] = "ID";
$lang["vehicle_plate"] = "Placa de Vehículo";
$lang["VIN"] = "Número VIN";
$lang["year"] = "Año";
$lang["km"] = "Kilometraje";
$lang["model"] = "Modelo";
$lang["color"] = "Color";
$lang["vehicle_type"] = "Tipo de Vehículo";

$lang["staff_request"] = "Funcionario que Solicita";
$lang["date_out"] = "Fecha de Salida";
$lang["reason"] = "Razón de Solicitud";
$lang["request_status"] = "Estado de Solicitud";
$lang["requested_vehicle"] = "Vehículo Solicitado";
$lang["actions"] = "Acciones";

$lang["vehicle"] = "Vehículo";
$lang["staff"] = "Funcionario";
$lang["generate_vehicle_request"] = "Generar una Solicitud de Vehículo";
$lang["close"] = "Cerrar";
$lang["date"] = "Fecha";
$lang["accident"] = "Accidente";
$lang["date"] = "Fecha";
$lang["accident_type"] = "Tipo de Accidente";
$lang["accident_description"] = "Descripción del Accidente";
$lang["accident_date"] = "Fecha del Accidente";
$lang["record_date"] = "Fecha de Registro";
$lang["repair_date"] = "Fecha de la Reparación";
$lang["repair_status"] = "Estado de Reparación";
$lang["repair_type"] = "Tipo de Reparación";
$lang["repair_description"] = "Descripción de la Reparación";
$lang["cost"] = "Costo";
$lang["media"] = "Media";
$lang["user"] = "Usuario";
$lang["driver_type"] = "Tipo de Chofer";
$lang["driver_availability"] = "Disponibilidad del Chofer";
$lang["add_driver"] = "Registrar un Chofer";

$lang["revision_type"] = "Tipo de Revisión";
$lang["revision"] = "Revisión";
$lang["maintenance_type"] = "Tipo de Mantenimiento";

$lang["trip_request"] = "Solicitud de Viaje";
$lang["assigned_driver"] = "Chofer Asignado";
$lang["budget_assigned"] = "Presupuesto Asignado";
$lang["create_new_trip"] = "Crear Nuevo Viaje";
$lang["vehicle_request"] = "Solicitud de Vehículo";
$lang["driver"] = "Chofer";
$lang["invoices"] = "Facturas";
$lang["total"] = "Total";
$lang["trip"] = "Viaje";
$lang["status"] = "Estado";
$lang["add_trip_invoice"] = "Agregar Facturas de Viaje";
$lang["status_request_vehicle"] = "Estado Solicitud de Vehículo";
$lang["policy_secures"] = "Pólizas/Seguros";
$lang["policy"] = "Póliza";
$lang["policy_type"] = "Tipo de Pólizas";
$lang["availability"] = "Disponibilidad";
$lang["availability_type"] = "Tipo de Disponibilidad";
$lang["add"] = "Agregar";



// suministros
$lang["category_type"] = "Tipo de Categoría";
$lang["preferential_catalog"] = "Catálogo Preferencial";
$lang["category"] = "Categoría";
$lang["item"] = "Item";
$lang["qty"] = "Cantidad";
$lang["unitary_price"] = "Precio Unitario";
$lang["last_date_ingress"] = "Última Fecha de Ingreso";
$lang["request_supplies"] = "Solicitar Suministro en Inventario";
$lang["requests"] = "Solicitudes";

$lang["qty_requested"] = "Cantidad Pedido";
$lang["request_type"] = "Tipo de Solicitud";
$lang["justification"] = "Justificación";
$lang["date_request"] = "Fecha Solicitud";

$lang["prov_request_type"] = "Tipo de Solicitud Proveeduría";
$lang["purchase_type"] = "Tipo de Compra";
$lang["provider_type"] = "Tipo de Proveedor";
$lang["zones"] = "Zonas";
$lang["warehouse"] = "Bodega";
$lang["description"] = "Descripción";
