<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Util {

  public function __construct() {  }

    public function send_email($to, $subject, $message, $from = NULL, $from_name = NULL, $attachment = NULL, $cc = NULL, $bcc = NULL)
    {
        $CI =& get_instance();
        $CI->load->library('email');
        $config = array(
    		'protocol' =>'smtp',
    		'smtp_host' =>'ssl://smtp.gmail.com',
    		'smtp_timeout' =>30,
    		'smtp_port' =>465,
    		'smtp_user' =>'notificacionesproyectouia@gmail.com',
    		'smtp_pass' =>'passwordproyectouia',
    		'charset' =>'utf-8',
    		'mailtype' =>'html',
    		'newline' =>"\r\n"

    	  );
        $CI->email->initialize($config);

        $CI->email->from('notificacionesproyectouia@gmail.com', 'Gestión');

        $CI->email->to($to);
        if ($cc) {
            $CI->email->cc($cc);
        }
        if ($bcc) {
            $CI->email->bcc($bcc);
        }
        $CI->email->subject($subject);
        $meessagehtml='
                                <html>
                        <head>
                            <style type="text/css">
                                body {background-color: #CCD9F9;
                                    font-family: Verdana, Geneva, sans-serif}

                                h3 {color:#4C628D}

                                p {font-weight:bold}
                            </style>
                        </head>
                        <body>

                            <h3>Estimado Usuario,</h3>
                            <h3>Se le informa que.</h3> 

                            <p>'.$message.'</p>

                        </body>
                        </html>
        ';
        $CI->email->set_mailtype("html");
        $CI->email->message($meessagehtml);
        if ($attachment) {
            if(is_array($attachment)) {
                $CI->email->attach($attachment['file'], '', $attachment['name'], $attachment['mine']);
            } else {
                $CI->email->attach($attachment);
            }
        }

        if ($CI->email->send()) {
            //echo $this->email->print_debugger(); die();
            return TRUE;
        } else {
            //echo $this->email->print_debugger(); die();
            return FALSE;
        }
    }

}
