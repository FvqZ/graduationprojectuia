<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Edit_loginattempts extends CI_Migration {
	private $tables;

	public function __construct() {
		parent::__construct();
		$this->load->dbforge();
	}

	public function up() {
		$this->dbforge->drop_column("login_attempts", "users_id");
	}

	public function down() {
		$fields = array(
		        'users_id' => array('type' => 'INT')
		);
		$this->dbforge->add_column('login_attempts', $fields);
	}
}
