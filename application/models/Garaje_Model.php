<?php

class Garaje_Model extends CI_Model{

    function __construct()
    {
       function __construct() {
        //parent::__construct();
        $this->load->database();
      }
    }

    //Inserts


    public function InsertColores($data){
        $result = $this->db->insert("colores",$data);
        return $result;
      }

      public function InsertAccidente($data){
        $result = $this->db->insert("tipoaccidente",$data);
        return $result;
      }
    public function InsertTipoVehiculo($data){
       $result = $this->db->insert("tipovehiculo",$data);
       return $result;
     }

     public function InsertModelo($data){
      $result = $this->db->insert("modelo",$data);
      return $result;
    }

    public function InsertTipoReparacion($data){
      $result = $this->db->insert("tiporeparacion",$data);
      return $result;
    }

    public function InsertVehiculo($data){
      $result = $this->db->insert("vehiculos",$data);
      return $result;
    }

    public function InsertRegistroViaje($data){
      $result = $this->db->insert("viaje",$data);
      return $result;
    }

    public function InsertFacturasViaje($data){
      $result = $this->db->insert("controlviajes",$data);
      return $result;
    }

    public function InsertRegistroMantenimiento($data){
      $result = $this->db->insert("revisiones",$data);
      return $result;
    }


    public function InsertSolicitudVehiculo($data){
      $result = $this->db->insert("solicitudvehiculos",$data);
      return $result;
    }

    public function InsertListadoChoferes($data){
      $result = $this->db->insert("choferes",$data);
      return $result;
    }

    public function InsertAccidenteDesc($data){
      $result = $this->db->insert("listadoaccidentes",$data);

      ////Trigger

      if($result)
      {

        $insertId = $this->db->insert_id();
        $data2 = array(

          'fecha' => date("Y/m/d"),
          'listadoaccidentes_id' => $insertId,
          'EstadoReparacion_idEstadoReparacion' => 1,
          'tiporeparacion_idTipoReparacion' => 2,

        );

        $result = $this->db->insert("accidentesreparados",$data2);

      }



      return $result;
    }

    public function InsertCostoReparacion($data){
      $result = $this->db->insert("reparaciones",$data);
      return $result;
    }




    public function InsertTipoEstadoVehiculo($data){
      $result = $this->db->insert("estadoreparacion",$data);
      return $result;
    }

    public function InsertTipoEstadoSolVehiculo($data){
      $result = $this->db->insert("estadosolicitudvehiculo",$data);
      return $result;
    }

    public function InsertTipoRevision($data){
      $result = $this->db->insert("tiporevision",$data);
      return $result;
    }

    public function InsertTipoChofer($data){
      $result = $this->db->insert("tipochofer",$data);
      return $result;
    }

    public function InsertDisponibilidadChofer($data){
      $result = $this->db->insert("disponibilidadchofer",$data);
      return $result;
    }

    public function InsertPoliza($data){
      $result = $this->db->insert("polizas",$data);
      return $result;
    }

  //Select


    public function getModelo()
    {
      $q = $this->db->get('modelo');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
        $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
        "recordsFiltered" => $q->num_rows() );
        return $retData;
      }

      return false;

    }

    public function getAccidente()
    {
      $q = $this->db->get('tipoaccidente');
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
      return $retData;

    }

    public function getColor()
    {
      $q = $this->db->get('colores');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }

        return $data;
      }

      return false;

    }

    public function getDisponibilidadChoferes2()
    {

      $q = $this->db->get('disponibilidadchofer');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }

    public function getTipoVehiculo()
    {
         $this->db->select('vehiculos.id, vehiculos.PlacaVehiculo, vehiculos.NumeroVIN, vehiculos.Anno, modelo.Desc_Modelo, colores.Desc_Colores, tipovehiculo.Desc_TipoVehiculo' );
         $this->db->from('vehiculos');
         $this->db->join('modelo', 'vehiculos.Modelo_Id = modelo.id');
         $this->db->join('colores', 'vehiculos.Color_Id = colores.id');
         $this->db->join('tipovehiculo', 'vehiculos.TipoVehiculo_id = tipovehiculo.id');

      $q = $this->db->get();
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );

      return $retData;

    }


    public function getFuncionarioSeleccionado()
    {
          $this->db->select('users.id, vehiculos.PlacaVehiculo, users.Cedula, solicitudvehiculos.FechaSalida, solicitudvehiculos.Razon, estadosolicitudvehiculo.Desc_Estado' );
          $this->db->from('solicitudvehiculos');
          $this->db->join('vehiculos', 'solicitudvehiculos.vehiculos_id = vehiculos.id');
          $this->db->join('users', 'solicitudvehiculos.users_id = users.id');
          $this->db->join('estadosolicitudvehiculo', 'solicitudvehiculos.EstadoSolicitudVehiculo_id = estadosolicitudvehiculo.id');
      $q = $this->db->get();
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }
      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );

      return $retData;

    }

    public function getFuncionarioSeleccionado2()
    {

      $q = $this->db->get('users');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }

    public function getTipoChofer2()
    {

      $q = $this->db->get('tipochofer');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }



    public function getPoliza()
    {

      $q = $this->db->get("polizas");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }
      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }


  ////////////////////
    public function getCostoReparacion()
  {

	$this->db->select('reparaciones.id, reparaciones.Desc_Reparacion, reparaciones.Fecha, reparaciones.Costo, vehiculos.PlacaVehiculo, reparaciones.Media, listadoaccidentes.desc_Accidente' );
	$this->db->from('reparaciones');
	$this->db->join('vehiculos', 'reparaciones.Id_Vehiculo = vehiculos.id');
	$this->db->join('accidentesreparados', 'reparaciones.accidentesreparados_idAccidentesReparados = accidentesreparados.idAccidentesReparados');
	$this->db->join('listadoaccidentes', 'accidentesreparados.listadoaccidentes_id = listadoaccidentes.id');

  $q = $this->db->get();
  $data = array();
	if ($q->num_rows())
	{
		foreach ($q->result() as $row)
			{
				$data[] = $row;
			}
	}

  $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
"recordsFiltered" => $q->num_rows() );


return $retData;
  }

  public function getCostoReparacion2()
  {
    $this->db->select('accidentesreparados.idAccidentesReparados, listadoaccidentes.desc_Accidente' );
	$this->db->from('accidentesreparados');
	$this->db->join('listadoaccidentes', 'accidentesreparados.listadoaccidentes_id = listadoaccidentes.id');
  $this->db->where('accidentesreparados.EstadoReparacion_idEstadoReparacion !=', 3 )
  ->or_where('accidentesreparados.EstadoReparacion_idEstadoReparacion != ', 4 );

    $q = $this->db->get();
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }


      return $data;
    }

    return false;

  }


    public function getTipoVehiculo2()
    {


      $this->db->select('vehiculos.id, vehiculos.PlacaVehiculo, vehiculos.NumeroVIN, vehiculos.Anno, modelo.Desc_Modelo, colores.Desc_Colores, tipovehiculo.Desc_TipoVehiculo' );
      $this->db->from('vehiculos');
      $this->db->join('modelo', 'vehiculos.Modelo_Id = modelo.id');
      $this->db->join('colores', 'vehiculos.Color_Id = colores.id');
      $this->db->join('tipovehiculo', 'vehiculos.TipoVehiculo_id = tipovehiculo.id');

      $q = $this->db->get();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }

    public function getTipoVehiculoTable2()
    {

      $q = $this->db->get("tipovehiculo");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getListadoPoliza()
    {

      $q = $this->db->get("polizas");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getTipoRevision2()
    {

      $q = $this->db->get("tiporevision");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getSolicitudVehiculoSeleccionada()
    {

      $q = $this->db->get("solicitudvehiculos");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getChoferAsignado()
    {

      $this->db->select('choferes.idChoferes, users.Cedula' );
      $this->db->from('choferes');
      $this->db->join('users', 'choferes.users_id = users.id');


      $q = $this->db->get();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getListadoViajes()
    {

      $this->db->select('viaje.idViaje, solicitudvehiculos.Razon' );
      $this->db->from('viaje');
      $this->db->join('solicitudvehiculos', 'Viaje.solicitudvehiculos_id = solicitudvehiculos.id');


      $q = $this->db->get();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }


   public function getTipoChoferes()
    {

      $q = $this->db->get("tipochofer");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }

    public function getDisponibilidadChoferes()
    {

      $q = $this->db->get("disponibilidadchofer");
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


  return $data;
      }

      return false;

    }


    public function getTipoVehiculoTable()
    {

      $q = $this->db->get("tipovehiculo");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }

    public function getTipoEstadoVehiculo()
    {

      $q = $this->db->get("estadoreparacion");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }

    public function getEstadoSolVehiculo()
    {

      $q = $this->db->get("estadosolicitudvehiculo");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }

    public function getTipoRevision()
    {

      $q = $this->db->get("tiporevision");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }

      }
      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;
    }

    public function getTipoChofer()
    {

      $q = $this->db->get("tipochofer");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }

    public function getDisponibilidadChofer()
    {

      $q = $this->db->get("disponibilidadchofer");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }



    public function getTipoReparacion()
    {

      $q = $this->db->get("tiporeparacion");
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;

    }

    public function getColores()
    {
      $q = $this->db->get('colores');
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
      }
      $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );
return $retData;
    }

  /********************Funciones de borrado**********************/

    public function BorrarColor($id = null)
    {
      $q = $this->db->delete("colores",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarTipoVehiculo($id = null)
    {
      $q = $this->db->delete("tipovehiculo",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarModelo($id = null)
    {
      $q = $this->db->delete("modelo",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarAccidente($id = null)
    {
      $q = $this->db->delete("tipoaccidente",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarReparacion($id = null)
    {
      $q = $this->db->delete("tiporeparacion",array("idTipoReparacion" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarVehiculo($id = null)
    {
      $q = $this->db->delete("vehiculos",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarEstadoVehiculo($id = null)
    {
      $q = $this->db->delete("estadoreparacion",array("idEstadoReparacion" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarEstadoSolVehiculo($id = null)
    {
      $q = $this->db->delete("estadosolicitudvehiculo",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarTipoRevision($id = null)
    {
      $q = $this->db->delete("tiporevision",array("idTipoRevision" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarTipoChofer($id = null)
    {
      $q = $this->db->delete("tipochofer",array("idTipoChofer" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarDisponibilidadChofer($id = null)
    {
      $q = $this->db->delete("disponibilidadchofer",array("idDisponibilidadChofer" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarPoliza($id = null)
    {
      $q = $this->db->delete("polizas",array("idPolizas" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    //Listado Dropdown

    public function getEstadoSolicitudLista()
    {
      $q = $this->db->get('estadosolicitudvehiculo');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }


    /******************** //Funciones de borrado**********************/

////////////Sum

public function getSumCostos()
{


	$this->db->select('reparaciones.id, reparaciones.Desc_Reparacion, reparaciones.Fecha, sum(reparaciones.Costo) as Costo, vehiculos.PlacaVehiculo, reparaciones.Media, listadoaccidentes.desc_Accidente' );
	$this->db->from('reparaciones');
	$this->db->join('vehiculos', 'reparaciones.Id_Vehiculo = vehiculos.id');
	$this->db->join('accidentesreparados', 'reparaciones.accidentesreparados_idAccidentesReparados = accidentesreparados.idAccidentesReparados');
	$this->db->join('listadoaccidentes', 'accidentesreparados.listadoaccidentes_id = listadoaccidentes.id');
  $this->db->group_by('vehiculos.PlacaVehiculo');


  $q = $this->db->get();

  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
    $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
    "recordsFiltered" => $q->num_rows() );
    return $retData;
  }
  return false;


}


public function getSumViajes()
{

  $this->db->select('controlviajes.idControlViajes, solicitudvehiculos.Razon, controlviajes.Facturas, sum(controlviajes.TotalFactura) as TotalFactura' );
  $this->db->from('controlviajes');
  $this->db->join('viaje', 'controlviajes.Viaje_idViaje = viaje.idViaje');
  $this->db->join('solicitudvehiculos', 'viaje.solicitudvehiculos_id = solicitudvehiculos.id');
  $this->db->group_by('solicitudvehiculos.Razon');


  $q = $this->db->get();

  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
    $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
    "recordsFiltered" => $q->num_rows() );
    return $retData;
  }
  return false;


}


//Updates

public function EditListado($data=array(), $id = NULL)
{
  return $this->db->update('solicitudvehiculos', $data, array('id' => $id));
}

public function EditColor($data=array(), $id = NULL)
{
  return $this->db->update('colores', $data, array('id' => $id));
}

public function EditTipoVehiculo($data=array(), $id = NULL)
{
  return $this->db->update('tipovehiculo', $data, array('id' => $id));
}

public function EditModelo($data=array(), $id = NULL)
{
  return $this->db->update('modelo', $data, array('id' => $id));
}

public function EditTipoReparacion($data=array(), $id = NULL)
{
  return $this->db->update('tiporeparacion', $data, array('idTipoReparacion' => $id));
}

public function EditAccidente($data=array(), $id = NULL)
{
  return $this->db->update('tipoaccidente', $data, array('id' => $id));
}

public function editAccidenteStatus($data=array(), $id = NULL)
{
  return $this->db->update('accidentesreparados', $data, array('idAccidentesReparados' => $id));
}

public function EditEstado($data=array(), $id = NULL)
{
  return $this->db->update('estadoreparacion', $data, array('idEstadoReparacion' => $id));
}

public function EditEstadoSol($data=array(), $id = NULL)
{
  return $this->db->update('estadosolicitudvehiculo', $data, array('id' => $id));
}

public function EditTipoRevision($data=array(), $id = NULL)
{
  return $this->db->update('tiporevision', $data, array('idTipoRevision' => $id));
}

public function EditTipoChofer($data=array(), $id = NULL)
{
  return $this->db->update('tipochofer', $data, array('idTipoChofer' => $id));
}

public function EditDisponibilidad($data=array(), $id = NULL)
{
  return $this->db->update('disponibilidadchofer', $data, array('idDisponibilidadChofer' => $id));
}

public function EditPoliza($data=array(), $id = NULL)
{
  return $this->db->update('polizas', $data, array('idPolizas' => $id));
}

public function EditVehiculo($data=array(), $id = NULL)
{
  return $this->db->update('vehiculos', $data, array('id' => $id));
}

public function EditChofer($data=array(), $id = NULL)
{
  return $this->db->update('choferes', $data, array('idChoferes' => $id));
}


//GetbyId

public function getListadobyId($id = NULL)
{
  $q = $this->db->get_where('solicitudvehiculos', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoReparacionbyId($id = NULL)
{
  $q = $this->db->get_where('tiporeparacion', array('idTipoReparacion' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getColorbyId($id = NULL)
{
  $q = $this->db->get_where('colores', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoVehiculobyId($id = NULL)
{
  $q = $this->db->get_where('tipovehiculo', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getModelobyId($id = NULL)
{
  $q = $this->db->get_where('modelo', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getAccidentebyId($id = NULL)
{
  $q = $this->db->get_where('tipoaccidente', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getAccidenteReparadobyId($id = NULL)
{
  $q = $this->db->get_where('accidentesreparados', array('idAccidentesReparados' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getEstadobyId($id = NULL)
{
  $q = $this->db->get_where('estadoreparacion', array('idEstadoReparacion' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getEstadoSolbyId($id = NULL)
{
  $q = $this->db->get_where('estadosolicitudvehiculo', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoRevisionbyId($id = NULL)
{
  $q = $this->db->get_where('tiporevision', array('idTipoRevision' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoChoferbyId($id = NULL)
{
  $q = $this->db->get_where('tipochofer', array('idTipoChofer' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getDisponibilidadbyId($id = NULL)
{
  $q = $this->db->get_where('disponibilidadchofer', array('idDisponibilidadChofer' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getPolizabyId($id = NULL)
{
  $q = $this->db->get_where('polizas', array('idPolizas' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getVehiculobyId($id = NULL)
{
  $q = $this->db->get_where('vehiculos', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getChoferbyId($id = NULL)
{
  $q = $this->db->get_where('choferes', array('idChoferes' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}



}


?>
