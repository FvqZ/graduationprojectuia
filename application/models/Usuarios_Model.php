<?php

class Usuarios_Model extends CI_Model{

    function __construct()
    {
       function __construct() {
        //parent::__construct();
        $this->load->database();
      }
    }

    //Inserts

    public function InsertTipoCedula($data){
        $result = $this->db->insert("tipocedula",$data);
        return $result;
      }

      public function InsertTipoUsuario($data){
        $result = $this->db->insert("groups",$data);
        return $result;
      }

      public function InsertDepartamento($data){
        $result = $this->db->insert("departamento",$data);
        return $result;
      }

      public function InsertUsuarios($data){
        if ($this->db->insert("users",$data))
        {
          $id = $this->db->insert_id();
          return $this->db->insert('users_groups', array('user_id' => $id, 'group_id' => $data['group_id'])); // 3 = user normal

        }
        return false;
      }





    // Borrados

    public function BorrarTipoCedula($id = null)
    {
      $q = $this->db->delete("tipocedula",array("idTipoCedula" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarTipoUsuarios($id = null)
    {
      $q = $this->db->delete("groups",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function BorrarDepartamento($id = null)
    {
      $q = $this->db->delete("departamento",array("id" => $id));
      if ($q)
      {
      return true;
      }

      return false;
    }

    public function borrarUsuario($id=null)
    {
      return $this->db->delete('users', array('id' =>$id));
    }


    //Selects

    public function getTipoCedula()
    {
      $q = $this->db->get('tipocedula');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;

      }
      return false;
    }

    public function getTipoUsuarios()
    {
      $q = $this->db->get('groups');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;

      }
      return false;
    }

    public function getDepartamento()
    {
      $q = $this->db->get('departamento');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;

      }
      return false;
    }

    public function getUsuarios()
    {
      $this->db->select("users.id,tipocedula.Description, users.Cedula, users.email, users.active, CONCAT(users.first_name, ' ', users.last_name) as user, users.phone, groups.name, departamento.Desc_Departamento" );
      $this->db->from('users');
      $this->db->join('tipocedula', 'users.tipocedula_idTipoCedula1 = tipocedula.idTipoCedula');
      $this->db->join('groups', 'users.groups_id = groups.id');
      $this->db->join('departamento', 'users.departamento_id = departamento.id');
      $q = $this->db->get();
       if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;

      }
      return false;
    }


    //Select DropDowns

    public function getTipoCedulaSelect()
    {
      $q = $this->db->get('tipocedula');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }

    public function getTipoUsuarioSelect()
    {
      $q = $this->db->get('groups');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }


          return $data;
        }

      return false;
    }

    public function getDepartamentoSelect()
    {
      $q = $this->db->get('departamento');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
          {
            $data[] = $row;
          }


          return $data;
        }

      return false;
    }



    /*************/

    public function getRoles()
    {
      $q = $this->db->get('groups');
      $data = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      return  array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
                "recordsFiltered" => $q->num_rows() );

    }

    public function getRolebyId($id = NULL)
    {
      $q = $this->db->get_where('groups', array('id' => $id));

      if ($q->num_rows())
      {
        return $q->row();

      }

      return false;

    }

    public function EditRole($data= array(), $id = NULL)
    {
      return $this->db->update('groups', $data, array('id' => $id));
    }

    public function deleteRole($id=NULL)
    {
      return $this->db->delete('groups', array('id' => $id));
    }

    public function getusuarioRoles()
    {
      $this->db->select("users_groups.id as id, users.username as username, groups.name as group")
      ->join('users', "users.id = users_groups.user_id")
      ->join('groups', "groups.id = users_groups.group_id");
      $data = array();
      $q = $this->db->get('users_groups');

      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
      }

      return  array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
                "recordsFiltered" => $q->num_rows() );
    }

    public function editUser($data=array(), $id = NULL)
    {
      return $this->db->update('users', $data, array('id' => $id));
    }

    public function getGroupById($id=NULL)
    {
      $q = $this->db->get_where('users_groups', array('id' => $id));

      if ($q->num_rows())
      {
        return $q->row();
      }
      return false;
    }

    public function getUsuarioById($id=NULL)
    {
      $q = $this->db->get_where('users', array('id' => $id));
      if ($q->num_rows())
      {
        return $q->row();
      }
      return false;

    }

    //Get by id

    public function getTipoCedulabyId($id = NULL)
{
  $q = $this->db->get_where('tipocedula', array('idTipoCedula' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoUsuariobyId($id = NULL)
{
  $q = $this->db->get_where('groups', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getDepartamentobyId($id = NULL)
{
  $q = $this->db->get_where('departamento', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function EditTipoCedula($data=array(), $id = NULL)
{
  return $this->db->update('tipocedula', $data, array('idTipoCedula' => $id));
}

public function EditTipoUsuario($data=array(), $id = NULL)
{
  return $this->db->update('groups', $data, array('id' => $id));
}

public function EditDepartamento($data=array(), $id = NULL)
{
  return $this->db->update('departamento', $data, array('id' => $id));
}


}





?>
