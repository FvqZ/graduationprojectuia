<?php

class Suministros_Model extends CI_Model{

    function __construct()
    {
       function __construct() {
        //parent::__construct();
        $this->load->database();
      }
    }

    //Inserts

    public function InsertCategorias($data){
        $result = $this->db->insert("categoriasinventarios",$data);
        return $result;
      }

    public function InsertInventarios($data){
       $result = $this->db->insert("inventarios",$data);
       return $result;
     }

     public function validatePreferential($data = array())
     {
          $q = $this->getInventariobyId($data["inventarios_id"]);
          if ($q->Preferencial_id == 1)
          {
            if ($data["CantidadPedido"] > $q->Cantidad)
            {
              return true; // si es preferencial y la cantidad del pedido es mayor a la actual retornar true para validar
            }
          }
          return false;
     }

     public function InsertSolicitudProveeduria($data)
     {
       $ret = $this->db->insert("solicitudproveeduria",$data);
       $insertId = $this->db->insert_id();
       $q = $this->getInventariobyId($data["inventarios_id"]);
       if($q->Preferencial_id==1)
       {
         $this->db->update('solicitudproveeduria',array('EstadoSolicitud_idEstadoSolicitud'=>2),array('id'=>$insertId));
         $this->restaInventario([],$insertId);
         $q = $this->db->get_where('solicitudproveeduria',array('id'=>$insertId));
         return $q->row();
        }else{
          $q = $this->db->get_where('solicitudproveeduria',array('id'=>$insertId));
          return $q->row();
        }


      return false;

      // return $ret;




      }

      /*public function restaInventario($data=array(), $id=NULL)
{

  $q = $this->db->get_where('solicitudproveeduria', array('id' => $id));
 // $t = $this->db->get_where('inventarios', array('inventarios_id' => $id));

  if ($q->num_rows())
  {
    $rowSolicitud = $q->row();
    $t = $this->db->get_where('inventarios', array('id' => $rowSolicitud->inventarios_id));
    $rowInventario = $t->row();
    $qty = (int)$rowInventario->Cantidad - (int)$rowSolicitud->CantidadPedido;
    if($this->db->update('inventarios', array('Cantidad' => $qty), array('id' => $rowSolicitud->inventarios_id)))
    {
     return $rowSolicitud->inventarios_id;
    }

  }

  return false;

}

public function ValidacionInv($id){

  $q = $this->db->get_where('inventarios', array('id' => $id));
  if($q->num_rows())
  {
    if($q->row()->Cantidad <= $q->row()->cantidadminima)
    {
      return $q->row();
    }

  }
  return false;

}*/



     public function InsertEstadoSuministro($data){
      $result = $this->db->insert("estadoSolicitud",$data);
      return $result;
    }

    public function InsertPreferencial($data){
      $result = $this->db->insert("preferencial",$data);
      return $result;
    }

    public function InsertTipoCompra($data){
      $result = $this->db->insert("tipo_de_compra",$data);
      return $result;
    }


    public function InsertTipoProveedor($data){
      $result = $this->db->insert("tipo_de_proveedor",$data);
      return $result;
    }

    public function InsertZona($data){
      $result = $this->db->insert("zona",$data);
      return $result;
    }


    public function InsertEdificio($data){
      $result = $this->db->insert("region",$data);
      return $result;
    }
    //Selects


     public function getCategorias()
     {
       $q = $this->db->get('categoriasinventarios');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }
       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getZona()
     {
       $q = $this->db->get('zona');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }

       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }


     public function getEdificio()
     {
       $q = $this->db->get('region');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }
       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getItems()
     {
        $this->db->select('inventarios.id, categoriasinventarios.Desc_Categorias, inventarios.DescripcionInv, inventarios.Cantidad, inventarios.costo, inventarios.FechaIngreso');
        $this->db->from('inventarios');
        $this->db->join('categoriasinventarios', 'inventarios.id_RefCategoria = categoriasinventarios.id');


       $q = $this->db->get('');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }

       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getInventario()
     {


      $this->db->select('inventarios.id, categoriasinventarios.Desc_Categorias, inventarios.DescripcionInv, inventarios.Cantidad, inventarios.costo, inventarios.FechaIngreso, preferencial.Desc_Preferencial, zona.Desc_Zona, inventarios.cantidadminima, region.Desc_Region' );
      $this->db->from('inventarios');
      $this->db->join('categoriasinventarios', 'inventarios.Id_RefCategoria = categoriasinventarios.id');
      $this->db->join('preferencial', 'inventarios.Preferencial_id = preferencial.id');
      $this->db->join('zona', 'inventarios.Zona_idZona = zona.idZona');
      $this->db->join('region', 'inventarios.region_idRegion = region.idRegion');

      $data = array();

       $q = $this->db->get();

       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }
       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;


     }

     public function getSolicitudes()
     {


      $this->db->select('solicitudproveeduria.id, solicitudproveeduria.FechaSolicitud, solicitudproveeduria.Justificacion, inventarios.DescripcionInv, tiposolicitudproveeduria.Desc_TipoSolicitud, solicitudproveeduria.CantidadPedido, users.Cedula, estadosolicitud.Descripcion' );
      $this->db->from('solicitudproveeduria');
      $this->db->join('tiposolicitudproveeduria', 'solicitudproveeduria.id_RefTipoSolicitud = tiposolicitudproveeduria.id');
      $this->db->join('users', 'solicitudproveeduria.users_id = users.id');
      $this->db->join('inventarios', 'solicitudproveeduria.inventarios_id = inventarios.id');
      $this->db->join('estadosolicitud', 'solicitudproveeduria.EstadoSolicitud_idEstadoSolicitud = estadosolicitud.idEstadoSolicitud');
      $data = array();

       $q = $this->db->get();

       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

       }
       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;


     }

     public function getEstadoSolicitud()
     {
       $q = $this->db->get('estadosolicitud');
       $data =array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }
       }

       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getSolicitudProveeduria()
     {
       $q = $this->db->get('tiposolicitudproveeduria');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }
       }

       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getPreferencial()
     {
       $q = $this->db->get('preferencial');
       $data = array();
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }
       }

       $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
       "recordsFiltered" => $q->num_rows() );
       return $retData;

     }

     public function getPreferencial2()
     {

       $q = $this->db->get("preferencial");
       if ($q->num_rows())
       {
         foreach ($q->result() as $row)
         {
           $data[] = $row;
         }

         return $data;
      }

      return false;

    }



      public function getCategoria2()
         {

           $q = $this->db->get("categoriasinventarios");
           if ($q->num_rows())
           {
             foreach ($q->result() as $row)
             {
               $data[] = $row;
             }

             return $data;
      }

      return false;

    }



     //borrado

     public function BorrarCategorias($id = null)
     {
       $q = $this->db->delete("categoriasinventarios",array("id" => $id));
       if ($q)
       {
       return true;
       }

       return false;
     }


     public function BorrarInventario($id = null)
     {
       $q = $this->db->delete("inventarios",array("id" => $id));
       if ($q)
       {
       return true;
       }

       return false;
     }

     public function BorrarEstadoSolicitud($id = null)
     {
       $q = $this->db->delete("estadosolicitud",array("idEstadoSolicitud" => $id));
       if ($q)
       {
       return true;
       }

       return false;
     }

     public function BorrarPreferencial($id = null)
     {
       $q = $this->db->delete("preferencial",array("id" => $id));
       if ($q)
       {
       return true;
       }

       return false;
     }

/********************************/


public function getTipoCompras($data=array())
{
  $q = $this->db->get('tipo_de_compra');
  $data = array();
  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
  }
  return array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
  "recordsFiltered" => $q->num_rows() );
}

public function getTipoProveedor($data=array())
{
  $q = $this->db->get("tipo_de_proveedor");
  $data = array();
  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
  }
  return array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
  "recordsFiltered" => $q->num_rows() );
}

public function getZonas($data=array())
{
  $q = $this->db->get('zona');
  $data = array();
  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
  }
  return array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
  "recordsFiltered" => $q->num_rows() );
}


  public function deleteTipoCompra($id = NULL)
  {
    return $this->db->delete('tipo_de_compra', array('id' => $id));
  }

  public function deleteTipoProveedor($id = NULL)
  {
    return $this->db->delete('tipo_de_proveedor', array('id' => $id));
  }

  public function deleteZonas($id = NULL)
  {
    return $this->db->delete('zona', array('idZona' => $id));
  }


  public function deleteEdificio($id = NULL)
  {
    return $this->db->delete('region', array('idRegion' => $id));
  }

  public function getProveedores()
  {
    $data = array();
    $this->db->select('proveedores.idProveedores as id, tipo_de_proveedor.Desc_TipoProveedor as tipo, proveedores.NombreProveedor as nombre, proveedores.Contactos as contacto, proveedores.Email as email')
    ->join('tipo_de_proveedor', 'tipo_de_proveedor.id = proveedores.idTipoProveedor')
    ->from('proveedores');

    $q = $this->db->get();

    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }
    }

    return array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
    "recordsFiltered" => $q->num_rows() );

  }

  public function InsertProveedor($data=array())
  {
    return $this->db->insert('proveedores', $data);
  }

  public function deleteProveedor($id = NULL)
  {
    return $this->db->delete('proveedores', array('idProveedores' => $id));
  }
  public function getProveedorById($id = NULL)
  {
    $q = $this->db->get('proveedores', array('idProveedores' => $id));
    if ($q->num_rows())
    {
      return $q->row();
    }
    return false;
  }

public function editProveedor($data=array(), $id = NULL)
{
  return $this->db->update('proveedores', $data, array("idProveedores" => $id));
}


public function getCompras()
{
  $data = array();
  $this->db->select("compras.idCompras as id, CONCAT(users.first_name, '', users.last_name) as username, tipo_de_compra.DescTipoCompra as tipocompra, proveedores.NombreProveedor as proveedor, NumeroPedido as numeropedido, Total as total")
  ->join('users', 'users.id = compras.users_id')
  ->join('tipo_de_compra', 'tipo_de_compra.id = compras.idTipoCompra')
  ->join('proveedores', 'proveedores.idProveedores=compras.Proveedores_idProveedores')
  ->from('compras');

  $q = $this->db->get();

  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
  }

  return array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
  "recordsFiltered" => $q->num_rows() );

}


public function getUsers()
{
  $q = $this->db->get('users');
  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }
    return $data;
  }

  return array();

}

public function InsertCompra($data = array())
{
  return $this->db->insert('compras', $data);
}

public function deleteCompra($id= NULL)
{
  return $this->db->delete('compras', array('idCompras' => $id));
}

public function editCompra($data=array(), $id = NULL)
{
  return $this->db->update('compras', $data, array("idCompras" => $id));
}

public function getCompraById($id = NULL)
{
  $q = $this->db->get_where('compras', array('idCompras' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

//Grafico

public function getSumCostosCompras()
{

  $this->db->select("compras.idCompras as id, CONCAT(users.first_name, '', users.last_name) as username, tipo_de_compra.DescTipoCompra as tipocompra, proveedores.NombreProveedor as proveedor, NumeroPedido as numeropedido, sum(Total) as total")
  ->join('users', 'users.id = compras.users_id')
  ->join('tipo_de_compra', 'tipo_de_compra.id = compras.idTipoCompra')
  ->join('proveedores', 'proveedores.idProveedores=compras.Proveedores_idProveedores')
  ->group_by('proveedores.NombreProveedor')
  ->from('compras');


  $q = $this->db->get();
  $data = array();
  if ($q->num_rows())
  {
    foreach ($q->result() as $row)
    {
      $data[] = $row;
    }

  }
  $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
  "recordsFiltered" => $q->num_rows() );
  return $retData;


}


/****************** catalogos suministros *****************/
//get

public function getCategorybyId($id = NULL)
{
  $q = $this->db->get_where('categoriasinventarios', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}
public function getInventariobyId($id = NULL)
{
  $q = $this->db->get_where('inventarios', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getEstadoSolicitudbyId($id = NULL)
{
  $q = $this->db->get_where('estadosolicitud', array('idEstadoSolicitud' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getEstadoPreferencialbyId($id = NULL)
{
  $q = $this->db->get_where('preferencial', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoComprabyId($id = NULL)
{
  $q = $this->db->get_where('tipo_de_compra', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getTipoProveedorbyId($id = NULL)
{
  $q = $this->db->get_where('tipo_de_proveedor', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}

public function getZonabyId($id = NULL)
{
  $q = $this->db->get_where('zona', array('idZona' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}


public function getEdificiobyId($id = NULL)
{
  $q = $this->db->get_where('region', array('idRegion' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}
//update
public function editCategoria($data=array(), $id = NULL)
{
  return $this->db->update('categoriasinventarios', $data, array("id" => $id));
}

public function editInventario($data=array(), $id=NULL)
{
  return $this->db->update('inventarios', $data, array('id' => $id));
}
public function sumInventario($data=array(), $id=NULL)
{

  $q = $this->db->get_where('inventarios', array('id' => $id));
  if ($q->num_rows())
  {
    $row = $q->row();
    $qty = (int)$data["Cantidad"] + (int)$row->Cantidad;
    return $this->db->update('inventarios', array('Cantidad' => $qty), array('id' => $id));
  }

  return false;

}

public function restaInventario($data=array(), $id=NULL)
{

  $q = $this->db->get_where('solicitudproveeduria', array('id' => $id));
 // $t = $this->db->get_where('inventarios', array('inventarios_id' => $id));

  if ($q->num_rows())
  {
    $rowSolicitud = $q->row();
    $t = $this->db->get_where('inventarios', array('id' => $rowSolicitud->inventarios_id));
    $rowInventario = $t->row();
    $qty = (int)$rowInventario->Cantidad - (int)$rowSolicitud->CantidadPedido;
    if($this->db->update('inventarios', array('Cantidad' => $qty), array('id' => $rowSolicitud->inventarios_id)))
    {
     return $rowSolicitud->inventarios_id;
    }

  }

  return false;

}

public function ValidacionInv($id){

  $q = $this->db->get_where('inventarios', array('id' => $id));
  if($q->num_rows())
  {
    if($q->row()->Cantidad <= $q->row()->cantidadminima)
    {
      return $q->row();
    }

  }
  return false;

}


//estadosolicitud
public function editEstadoSolicitud($data=array(), $id = NULL)
{
  return $this->db->update('estadosolicitud', $data, array('idEstadoSolicitud' => $id));
}

public function editEstadoPreferencial($data=array(), $id = NULL)
{
  return $this->db->update('preferencial', $data, array('id' => $id));
}
public function editTipoCompra($data=array(), $id = NULL)
{
  return $this->db->update('tipo_de_compra', $data, array('id' => $id));
}

public function editTipoProveedor($data=array(), $id = NULL)
{
  return $this->db->update('tipo_de_proveedor', $data, array('id' => $id));
}

public function editZona($data=array(), $id = NULL)
{
  return $this->db->update('zona', $data, array('idZona' => $id));
}


public function editEdificio($data=array(), $id = NULL)
{
  return $this->db->update('region', $data, array('idRegion' => $id));
}

// Dropdown

    // Dropdown

    //Listado Dropdown

    public function getEstadoSolicitudLista()
    {
      $q = $this->db->get('estadosolicitud');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }


        return $data;
      }

      return false;

    }


    //Updates

public function EditListado($data=array(), $id = NULL)
{
  return $this->db->update('solicitudproveeduria', $data, array('id' => $id));
}


//GetbyId

public function getListadobyId($id = NULL)
{
  $q = $this->db->get_where('solicitudproveeduria', array('id' => $id));
  if ($q->num_rows())
  {
    return $q->row();
  }
  return false;
}



public function validateSuministros($id=NULL)
{
  $q = $this->db->get_where('solicitudproveeduria', array('id' => $id), 1);

  if ($q->num_rows())
  {
    $r = $this->db->get_where('inventarios', array('id' => $q->row()->inventarios_id), 1);
    if ($r->num_rows())
    {
      $cantidad_solicitada =(int)$q->row()->CantidadPedido;
      $cantidad_disponible = (int)$r->row()->Cantidad;
      var_dump($cantidad_solicitada);
      var_dump($cantidad_disponible);
      if ($cantidad_solicitada > $cantidad_disponible)
      {
        return true;
      }
    }
  }
  return false;
}



}




?>
