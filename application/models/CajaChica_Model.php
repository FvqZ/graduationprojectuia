<?php

class CajaChica_Model extends CI_Model{

    function __construct()
    {
       function __construct() {
        //parent::__construct();
        $this->load->database();
      }
    }

    public function InsertPresupuesto($data){
        $result = $this->db->insert("presupuesto",$data);
        return $result;
      }

      public function InsertEstadoCajaChica($data){
        $result = $this->db->insert("estado",$data);
        return $result;
      }
      
      public function InsertPartida($data){
        $result = $this->db->insert("partidas",$data);
        return $result;
      }

      public function InsertSolicitudCajaChica($data){
        $result = $this->db->insert("comprascajachica",$data);
        return $result;
      }

      public function getPresupuesto()
      {
        $q = $this->db->get('presupuesto');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;
        }
  
        return false;
  
      }

      public function getEstadoCajaChica()
      {
        $q = $this->db->get('estado');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
  return $retData;
        }
  
        return false;
  
      }

      public function getPartidas()
      {

        $this->db->select('partidas.idPartidas, partidas.Presupuestoporpartida, presupuesto.Monto, categoriasinventarios.Desc_Categorias' );
		$this->db->from('partidas');
		$this->db->join('presupuesto', 'partidas.Presupuesto_idPresupuesto = presupuesto.idPresupuesto');
		$this->db->join('categoriasinventarios', 'partidas.categoriasinventarios_id = categoriasinventarios.id');
        $q = $this->db->get();
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
        return $retData;
        }
  
        return false;
  
      }

      public function getSolicitudCajaChica()
      {

        $this->db->select('comprascajachica.idComprasCajaChica, comprascajachica.FechaCompra, comprascajachica.Nota, comprascajachica.Razon, estado.Descripcion, users.Cedula, categoriasinventarios.Desc_Categorias, presupuesto.AnnoPresupuesto, comprascajachica.TotalFactura' );
		$this->db->from('comprascajachica');
		$this->db->join('estado', 'comprascajachica.Estado_idEstado = estado.idEstado');
		$this->db->join('users', 'comprascajachica.users_id = users.id');
    $this->db->join('partidas', 'comprascajachica.Partidas_idPartidas = partidas.idPartidas');
    $this->db->join('categoriasinventarios', 'partidas.categoriasinventarios_id = categoriasinventarios.id');
    $this->db->join('presupuesto', 'partidas.Presupuesto_idPresupuesto = presupuesto.idPresupuesto');


        $q = $this->db->get('');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          $retData = array('draw' => $q->num_rows(), 'data' => $data, "recordsTotal"=> $q->num_rows(),
          "recordsFiltered" => $q->num_rows() );
        return $retData;
        }
  
        return false;
  
      }


      public function getCompras()
      {
        $q = $this->db->get('compras');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          return $data;
        }
  
        return false;
  
      }

      public function getEstado()
      {
        $q = $this->db->get('estado');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
          return $data;
        }
  
        return false;
  
      }
      
      public function BorrarPresupuesto($id = null)
      {
        $q = $this->db->delete("presupuesto",array("idPresupuesto" => $id));
        if ($q)
        {
        return true;
        }
  
        return false;
      }

      public function BorrarPartida($id = null)
      {
        $q = $this->db->delete("partidas",array("idPartidas" => $id));
        if ($q)
        {
        return true;
        }
  
        return false;
      }

      public function BorrarEstado($id = null)
      {
        $q = $this->db->delete("estado",array("idEstado" => $id));
        if ($q)
        {
        return true;
        }
  
        return false;
      }

      //Select Dropdown
      

      public function getPresupuestoAnual()
      {
        $q = $this->db->get('presupuesto');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
  
          return $data;
        }
  
        return false;
  
      }

      public function getCategoriaPartida()
      {
        $q = $this->db->get('categoriasinventarios');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
  
          return $data;
        }
  
        return false;
  
      }

      public function getEstadoSolicitud()
      {
        $q = $this->db->get('estado');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
  
          return $data;
        }
  
        return false;
  
      }

      public function getUsuarioSeleccionado()
      {
        $q = $this->db->get('users');
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
  
          return $data;
        }
  
        return false;
  
      }


      public function getPartidaSeleccionada()
      {

        $this->db->select('partidas.idPartidas as id, partidas.Presupuestoporpartida, presupuesto.Monto, categoriasinventarios.Desc_Categorias, presupuesto.AnnoPresupuesto' );
        $this->db->from('partidas');
        $this->db->join('presupuesto', 'partidas.Presupuesto_idPresupuesto = presupuesto.idPresupuesto');
        $this->db->join('categoriasinventarios', 'partidas.categoriasinventarios_id = categoriasinventarios.id');

        $q = $this->db->get();
        if ($q->num_rows())
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
  
  
          return $data;
        }
  
        return false;
  
      }


      //GetbyId

      public function getPresupuestobyId($id = NULL)
        {
          $q = $this->db->get_where('presupuesto', array('idPresupuesto' => $id));
          if ($q->num_rows())
          {
            return $q->row();
          }
          return false;
        }

        public function getEstadobyId($id = NULL)
        {
          $q = $this->db->get_where('estado', array('idEstado' => $id));
          if ($q->num_rows())
          {
            return $q->row();
          }
          return false;
        }

        public function getPartidabyId($id = NULL)
        {
          $q = $this->db->get_where('partidas', array('idPartidas' => $id));
          if ($q->num_rows())
          {
            return $q->row();
          }
          return false;
        }

        public function getListadobyId($id = NULL)
        {
          $q = $this->db->get_where('comprascajachica', array('idComprasCajaChica' => $id));
          if ($q->num_rows())
          {
            return $q->row();
          }
          return false;
        }




      //Updates

      public function editPresupuesto($data=array(), $id = NULL)
      {
        return $this->db->update('presupuesto', $data, array('idPresupuesto' => $id));
      }

      public function editEstado($data=array(), $id = NULL)
      {
        return $this->db->update('estado', $data, array('idEstado' => $id));
      }

      public function EditPartidas($data=array(), $id = NULL)
      {
        return $this->db->update('partidas', $data, array('idPartidas' => $id));
      }
 
      public function EditListado($data=array(), $id = NULL)
      {
        return $this->db->update('comprascajachica', $data, array('idComprasCajaChica' => $id));
      }

  
  
  



}


?>
