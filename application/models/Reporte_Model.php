<?php

class Reporte_Model extends CI_Model{

    function __construct()
    {
       function __construct() {
        //parent::__construct();
        $this->load->database();
      }
    }


    public function getInventario()
    {
      $q = $this->db->get('inventarios');
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data[] = $row;
        }
        return $data;
      }
      return array();
    }

    public function getInventarioRows($data)
    {

      $this->db->select('inventarios.id, categoriasinventarios.Desc_Categorias, DescripcionInv, Cantidad, costo, FechaIngreso, preferencial.Desc_Preferencial, zona.Desc_Zona, cantidadminima')
      ->join('categoriasinventarios', 'categoriasinventarios.id = inventarios.Id_RefCategoria')
      ->join('preferencial', 'preferencial.id=inventarios.Preferencial_id')
      ->join('zona', 'zona.idZona=inventarios.Zona_idZona')
      ->from('inventarios');
      if (!is_null($data["fecha_inicio"]))
      {
        $this->db->where('FechaIngreso >=', $data["fecha_inicio"]);
      }
      if (!is_null($data["fecha_final"]))
      {
        $this->db->where('FechaIngreso <=', $data["fecha_final"]);
      }

      if (!is_null($data["producto"]))
      {
        $this->db->where('inventarios.id', $data["producto"]);
      }


      $q = $this->db->get();
      $data_ret = array();
      if ($q->num_rows())
      {
        foreach ($q->result() as $row)
        {
          $data_ret[] = $row;
        }
      }
      return array('draw' => $q->num_rows(), 'data' => $data_ret, "recordsTotal"=> $q->num_rows(),
      "recordsFiltered" => $q->num_rows() );;
    }






  }


 ?>
